/* Custom javascript file
======================================================================== 


/* Fit videos
======================================================================== */

function fitVideos(e) {
	jQuery('.object iframe, .object object, .object embed, .object video, .object .media-container, .object .media-container img').each(function(){
		var obj = jQuery(this),
			width = obj.parents('.object').width(),
			ratio = obj.width() / obj.height();

		if (obj.parents('.slide-content').length) {
			width = obj.parents('.grid_6').width();
		}

		obj.width(width);
		obj.height(width / ratio);
	});
}

jQuery(document).ready(function($) {

	/* Window resize events
	======================================================================== */
	
	var $main_nav   = $("#main-nav"),
		$nav_toggle = $("#nav-toggle"),
		$container = $('#content.blog-grid .content-wrapper'),
		masonry_loaded = false;

	// Masonry setup
    $('#content').imagesLoaded(function(){

    	if ($container.length > 0)
    	{
	        $container.masonry({
	            itemSelector: '.hentry',
	            singleMode: true,
	            isResizable: true
	            // isFitWidth: true,
	            // containerStyle: {position: 'relative'},
	            // cornerStampSelector: 'aside'
	            // isAnimated: true
	        });

	        // Trigger window resize
	    	masonry_loaded = true;
	    }

	 //    $('#content.blog-fluid-grid .content-wrapper .row').freetile({
		//     selector: '.hentry'
		// });

	    // $("#content.blog-fluid-grid .content-wrapper .row").vgrid({
	    //     easing: "easeOutQuint",
	    //     time: 500,
	    //     delay: 20,
	    //     fadeIn: {
	    //         time: 300,
	    //         delay: 50
	    //     }
	    // });

		setTimeout(function(){
			$(window).resize();
			fitVideos(null);
		}, 100);
    });

    // var vg = $('#content.blog-fluid-grid').vgrid();

    $.refreshMasonry = function() {
		if (masonry_loaded)	{
     		$container.masonry('reload');
     	}
     	fitVideos(null);
	}

	$('.object iframe, .object embed, .object object').load(function(){
		fitVideos(null);
	});

	$(window).on('resize', function () {
		$.refreshMasonry();
     	fitVideos(null);
	});

	// $(window).smartresize(function(){  
	// });

	/* Disable CSS Transitions by default (it prevents errors with 3rd party libraries) */
	$.toggleDisabledByDefault();


	/* Navigation
	======================================================================== */
	$nav_toggle.click(function (e) {
		e.preventDefault();
		// $main_nav.toggle();
	}); 

	$('.dropdown').tinyNav({header:false, active:'current-menu-item'});


	/* Photo viewer
	======================================================================== */
	$('.zoom').touchTouch();


    /* Hover feature
	======================================================================== */
	$(document).on('mouseover', '.object', function(){
		$('.overlay', $(this)).stop().fadeToggle();
	});
	$(document).on('mouseout', '.object', function(){
		$('.overlay', $(this)).stop().fadeToggle();
	});


	/* Sidebar follow
	======================================================================== */

	if (!Modernizr.touch) {
		var	$sidebar   = $('aside.sidebar'),
			$content   = $('#content');

		if ($sidebar.length > 0 && $content.length > 0)
		{
		    var $window    = $(window),
		   		threshold  = 15,
		        offset     = $sidebar.offset(),
		        offset_top = offset.top + threshold,
		        timer;

		    $window.scroll(function() {
		        clearTimeout(timer);
		        timer = setTimeout(function() {
		        	if ($('#content').width() > 600) {
			            if ($content.height() > $sidebar.height()) {
			                var new_margin = $window.scrollTop() - offset_top;
			                if ($window.scrollTop() > offset_top && ($sidebar.height()+new_margin) <= $content.height()) {
			                    // Following the scroll...
		                    	// $sidebar.css('margin-top', new_margin);
			                    $sidebar.stop().animate({ marginTop: new_margin });
			                } else if (($sidebar.height()+new_margin) > $content.height()) {
			                    // Reached the bottom...
		                    	// $sidebar.css('margin-top', $content.height()-$sidebar.height());
			                    $sidebar.stop().animate({ marginTop: $content.height()-$sidebar.height() });
			                } else if ($window.scrollTop() <= offset_top) {
			                    // Initial position...
		                    	// $sidebar.css('margin-top', 0);
			                    $sidebar.stop().animate({ marginTop: 0 });
			                }
			            }
					} else {
			          	$sidebar.css({ marginTop: 0 });
			        }
		        }, 100);
		    });
		}
	}


	/* Accordion
	======================================================================== */

	var $accordion_trigger = $('.accordion-trigger'),
		$accordion_content = $('.accordion-content');

	$accordion_trigger.click(function() {
		$accordion_trigger.removeClass('active');
		if($(this).next().is(':hidden') == true) {
	 		$accordion_content.each(function(index) {
		       	$(this).slideUp(300); });
			$(this).addClass('active');
			$(this).next().slideDown(300);
		} else{
			$(this).next().slideUp(300);
		}

		setTimeout(fitVideos, 500);
		return false;
	});
	
	$accordion_content.hide();
	$('.default-content').trigger('click');


	/* Tabs
	======================================================================== */

	function switch_tabs(obj) {
		$('.tabs a').removeClass("active");
		var $current_tab = $(obj.attr('href'));
		$current_tab.siblings().hide();
		$current_tab.show();
	    obj.addClass("active");
	}

	$('.tabs a').click(function(){
        switch_tabs($(this));
        return false;
    });
 
    switch_tabs($('.default-tab'));


	/* Filterable content
	======================================================================== */

	/* Allows to filter the content by tags in the portfolio pages */
	$('.filter a').click(function() {
		$('.filter .active').removeClass('active');
		$(this).parent().addClass('active');
		
		var filterVal = $(this).attr('href').replace(' ',' .').replace('#',' .');

		$('.filterable > div').stop().css('opacity', 0).addClass('hidden');

		if(filterVal == " .all") {
			$('.filterable .hidden').animate({opacity:1, avoidCSSTransitions:false, leaveTransforms:false}, 500).removeClass('hidden');
		} else {
			$('.filterable > div').each(function() {
				if ($(this).is(filterVal)) {
					$(this).animate({opacity:1, avoidCSSTransitions:false, leaveTransforms:false}, 500).removeClass('hidden');
				}
			});
		}

		// $(window).trigger('resize').trigger('resize');
		
		// return false;
	});


	/* Ajax contact form
	======================================================================== */

	$("#contact-form").submit(function(e) {
		// alert('aaaa');
		e.preventDefault();

		var str = $("#contact-form").serialize(),
			$button = $('button', $(this));
		
	   	$.ajax({
		   	type: "POST",
		   	dataType: 'json',
		   	url: contact_data.ajaxurl,
		   	data: 'action=contact_form&'+str,
		   	beforeSend: function() {
	   			$('.icon-preload', $button).remove();
	   			$('<i class="icon-preload" style="margin:-3px 0 -1px 14px;padding:0;"></i>').appendTo($button);
			},
		   	success: function(response) {
	   			$('.icon-preload', $button).remove();
				$("#info").ajaxComplete(function(event, request, settings) {
					if(response['success'] == true) {
						result = '<div class="success">'+contact_data.thank_you+'</div>';
						$("#fields").animate({opacity:0, avoidCSSTransitions:false, leaveTransforms:false}, 500).css('display', 'none');
					} else {
						result = '<div class="error"><ul>';
						$.each(response['errors'], function(key, val) {
							result += '<li>' + val + '</li>';
							// $('#'+key).addClass('error-field');
						});
						result += '</ul></div>';
					}

					$(this).hide();
					$(this).html(result).css({'visibility':'visible', 'display':'block'}).animate({opacity:1}, 500);
					$button.css('background-image', 'none');
				});
			}
		});

		// return false;
	});


	/* Fit texts
	======================================================================== */
	$(".page-template-template-custom-php h1").not('#logo h1').fitText(0.8, {minFontSize:'40px', maxFontSize:'75px'});
	// $(".fullwidth-slides .entry-title, .slide-content .entry-title a").fitText(0.8, {minFontSize:'40px', maxFontSize:'75px'});



	/* Infinite Scroll
	======================================================================== */	

	function isFunction(functionToCheck) {
		var getType = {};
		return functionToCheck && getType.toString.call(functionToCheck) == '[object Function]';
	}

	var $content_wrapper = $('.content-wrapper .row'),
		$content_wrapper_slides = $('.content-wrapper.fullwidth-slides'),
		$load_more = $('#load-more'),
		next_page_url = $('#pages-nav .next').attr('href'),
		last_page_url = '',
		pages_counter = 1,
		page_to_reach = 1;

    $load_more.click(infiniteScroll);

	// Restore the pages
	if (window.location.hash) {
		page_to_reach = window.location.hash.replace('#page', '');
		page_to_reach = parseInt(page_to_reach);
		restorePages(true);
	}

	function scrollShowContent() {
		$('.fullwidth-slide').off('inview');
		$('.fullwidth-slide').on('inview', function (event, visible) {
			if (visible == true) {
				$('.slide-content', $(this)).stop().show().addClass('fadeInRight');
				$('.slides-overlay', $(this)).fadeIn(500);
			// }
			} else {
				$('.slide-content', $(this)).removeClass('fadeInRight').fadeOut(500);
				$('.slides-overlay', $(this)).stop().fadeOut(500);
				// setTimeout(function(){
				// 	$('.fullwidth-slide .slide-content').hide();
				// 	$('.fullwidth-slide .slides-overlay').hide();
				// }, 501);
			}
		});
	}

	function restorePages(skip) {
    	if (pages_counter < page_to_reach) {
			page_to_reach++;
			setTimeout(function(){
		    	$('.slide-next', $('.fullwidth-slides .fullwidth-slide:last-child')).trigger('click');
			}, 100);
	    }
	    if (!skip) {
		    pages_counter++;
	    	window.location.hash = '#page'+pages_counter;
	    }
    	scrollShowContent();
	    return false;
	}

	function infiniteScroll(callback) {

		if ($load_more.length > 0) {
    		$('.icon-preload', $load_more).remove();
   			$('<i class="icon-preload" style="margin:-3px 0 -1px 14px;padding:0;"></i>').appendTo($load_more);
   		}

    	if (last_page_url == next_page_url || $(location).attr('href') == next_page_url) {
   			callback(false);
   			return false;
   		}

	   	$.get(next_page_url, function(responseHtml) {

	   		var next_page = false;

    		var dom = $(responseHtml),
    			$newElements = $('.content-wrapper .box', dom),
    			next_page = $('#pages-nav .next', dom).attr('href');

    		if ($content_wrapper_slides.length > 0) {
    			$newElements = $('article.fullwidth-slide', dom);
    			next_page = $('a.next.page-numbers', dom).attr('href');
    		}
							
    		if (!next_page) {
    			$load_more.hide();
    		}

    		$newElements.css({ opacity: 0 });

    		if ($content_wrapper_slides.length > 0) {
	       		$content_wrapper_slides.append($newElements);
	       	} else if ($content_wrapper.length > 0) {
	       		$content_wrapper.append($newElements);
	       	}

    		dom.filter('script').each(function(a,b){
    			if ($(this).attr('data-needed')) {
		            $.globalEval(this.text || this.textContent || this.innerHTML || '');
    			}
	        });

	       	$newElements.imagesLoaded(function(){
		    	fitVideos(null);
		    	$container.masonry('reload');
		    	$newElements.animate({ opacity: 1 });
		    	$('.icon-preload', $load_more).remove();
		    	$('.zoom').touchTouch();

		    	// Execute the callback function
			    if (isFunction(callback)) {
					callback(next_page);
				}
		    });
	   	});

		return false;
    }


	/* Content Slider - FlexSlider
	======================================================================== */

	var content_slider = $('.slider-shortcode .flexslider').flexslider({
		animation: "slide",
		slideshow: true,
		slideshowSpeed: 4000,
		animationDuration: 300,
		directionNav: true,
		controlNav: true,
		animationLoop: true,
		pauseOnAction: true,
		pauseOnHover: true,
		controlNav: false,
		directionNav: true,
	});


	/* Slides template
	======================================================================== */

	$('body').off('click', '.slide-next');
	$('body').on('click', '.slide-next', function(){
		var $this = $(this),
			$next_slide = $this.parents('article').next('article');

		if ($next_slide.length > 0) {
			$.scrollTo($next_slide, 500, {easing:'easeInOutExpo'});
		} else {

			$('.icon-preload', $this).remove();
   			$('<i class="icon-preload" style="width:58px;height:43px;background-position:center center;background-color:rgba(0,0,0,0.8);padding:0;"></i>').appendTo($this);

			infiniteScroll(function(next_page){
				$('.icon-preload', $this).remove();
				$next_slide = $this.parents('article').next('article');
				if ($next_slide.length > 0) {
					$.scrollTo($next_slide, 500, {easing:'easeInOutExpo'});
				}
				var $last_slide = $('.fullwidth-slides .fullwidth-slide:last-child');
				if (!next_page) {
					$('.slide-next', $last_slide).remove();
				} else {
    				last_page_url = next_page_url;
    				next_page_url = next_page;
				}
 				restorePages();
			});
		}
	});

	if (!Modernizr.touch) {
		var $fullwidth_slides = $('.content-wrapper.fullwidth-slides');

		if ($fullwidth_slides.length > 0) {
			$('.fullwidth-slide .slide-content').hide();
			$('.fullwidth-slide .slides-overlay').hide();

			scrollShowContent();

			// $('body').on('inview', '.fullwidth-slide', function (event, visible) {
			// 	if (visible == true) {
			// 		$('.slide-content', $(this)).stop().show().addClass('fadeInRight');
			// 		$('.slides-overlay', $(this)).fadeIn(500);
			// 	// }
			// 	} else {
			// 		$('.slide-content', $(this)).removeClass('fadeInRight').fadeOut(500);
			// 		$('.slides-overlay', $(this)).stop().fadeOut(500);
			// 		setTimeout(function(){
			// 			$('.fullwidth-slide .slide-content').hide();
			// 			$('.fullwidth-slide .slides-overlay').hide();
			// 		}, 501);
			// 	}
			// });

			$(window).trigger('scroll');
		}
	}

	/* Load TinyMCE plugins
	======================================================================== */
	// tinyMCEPreInit.load_ext("http://plugin/url/dir", "en");
	// tinymce.PluginManager.load("plugin_name", "http://plugin/url/dir/file.js");

});