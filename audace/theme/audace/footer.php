<div id="background-overlay"></div>


<?php $footer_widgets = get_field('yo_footer'); if ($footer_widgets == NULL || $footer_widgets == true) : ?>
<?php if (!is_page_template('template-clean.php') && !is_page_template('template-custom.php')) : ?>
	<?php if (function_exists('dynamic_sidebar') && is_active_sidebar('footer-widgets')) : ?>
	<!-- Footer -->
	<footer id="main-footer" class="row clearfix">
		<div class="grid_12">
			<div class="row horizontal-widgets">
				<?php dynamic_sidebar('footer-widgets'); ?>
			</div>
		</div>
	</footer>
	<?php endif; ?>
<?php endif; endif; ?>

<?php wp_footer(); ?>
</body></html>