<?php

/*
 *------------------------------------------------------------------------------------------------
 * Errors reporting
 *------------------------------------------------------------------------------------------------
 *
 */

// error_reporting(E_ALL);
// ini_set('display_errors', '1');


/*
 *------------------------------------------------------------------------------------------------
 * Theme Setup
 *------------------------------------------------------------------------------------------------
 *
 * Add languages support and WP features.
 *
 */

function yo_theme_setup()
{  
    //----------------------------------------------------------------------------------------
    // Languages
    //----------------------------------------------------------------------------------------
    load_theme_textdomain('theme_admin', get_template_directory() . '/languages');
    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";
    if (is_readable($locale_file))
        require_once($locale_file);


    //----------------------------------------------------------------------------------------
    // Menus
    //----------------------------------------------------------------------------------------
    register_nav_menus(array(
        'primary' => 'Primary Location',
    ));


    //----------------------------------------------------------------------------------------
    // Post Formats (WP3.1+)
    //----------------------------------------------------------------------------------------
    $formats = array('aside', 'audio', 'gallery', 'image', 'link', 'quote', 'video', 'status');
    add_theme_support('post-formats', $formats); 


    //----------------------------------------------------------------------------------------
    // Thumbnails (Custom sizes)
    //----------------------------------------------------------------------------------------
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(150, 150, true); // Normal post thumbnails
    add_image_size('full-hd', 1920, false, true); // 1080p (1920x1080)
    add_image_size('hd', 1280, false, true); // 720p (1280x720)
    add_image_size('standard', 853, false, true); // 480p (853x480)
    add_image_size('standard-fixed', 853, 480, true); // 480p (853x480)
    add_image_size('low', 640, false, true); // 360p (640x360)
    add_image_size('very-low', 480, false, true); // 320p (480x320)

    //----------------------------------------------------------------------------------------
    // RSS support (WP3.0+)
    //----------------------------------------------------------------------------------------
    add_theme_support('automatic-feed-links');

    //----------------------------------------------------------------------------------------
    // bbPress support
    //----------------------------------------------------------------------------------------
    // add_theme_support('bbpress');

    //----------------------------------------------------------------------------------------
    // TinyMCE Style
    //----------------------------------------------------------------------------------------
    // Customize the visual editor style with editor-style.css to match the theme style.
    add_editor_style('assets/css/editor-styles.css');

    //----------------------------------------------------------------------------------------
    // Set default theme options
    //----------------------------------------------------------------------------------------
    if (isset($_GET['activated']) && $_GET['activated'] == true)
    {
        $options = get_option('yo_theme_options');
        if (empty($options))
        {
            // $default_options = 'logo_enabled=on&logo=58&logo2x=57&logo_margin=0px&tagline_margin=0px&main_nav_top=27px&favicon=&favicon2x=54&contact_email=&tracking_code=&search_layout=1sidebar&yo_background_type=slideshow&yo_background_color=%23000000&yo_background_pattern=05&yo_background_standard%5Bimage%5D=&yo_background_standard%5Bposition%5D=center&yo_background_standard%5Brepeat%5D=no-repeat&yo_background_slideshow_settings%5Btransition_duration%5D=700&yo_background_slideshow_settings%5Bdelay%5D=7000&yo_background_slideshow%5B0%5D%5Bmedia%5D=93&yo_background_video%5Bvideo_type%5D=embed&yo_background_video%5Bvideo_url%5D=&yo_background_video%5Bvideo_mp4%5D=&yo_background_video%5Bvideo_ogv%5D=&yo_font=Noticia%2BText%3A700&yo_body_font=Open%2BSans%3Aregular&branding%5Blogo_size%5D=48px&branding%5Blogo_color%5D=%23e80000&branding%5Btagline_size%5D=14px&branding%5Btagline_color%5D=%23ffffff&branding%5Bshadow%5D=%23000000&branding%5Bbackground%5D=rgba(0%2C+0%2C+0%2C+0.75)&navigation%5Bsize%5D=14px&navigation%5Bmain_color%5D=%23ffffff&navigation%5Bmain_background%5D=&navigation%5Bmain_shadow%5D=%23000000&navigation%5Bmain_hover_color%5D=&navigation%5Bmain_hover_background%5D=rgba(255%2C+255%2C+255%2C+0.25)&navigation%5Bmain_hover_shadow%5D=&navigation%5Bsubmenu_color%5D=%23ffffff&navigation%5Bsubmenu_background%5D=rgba(0%2C+0%2C+0%2C+0.9)&navigation%5Bsubmenu_shadow%5D=&navigation%5Bcurrent_color%5D=%23ffffff&navigation%5Bcurrent_background%5D=rgba(237%2C+0%2C+0%2C+0.82)&navigation%5Bcurrent_shadow%5D=%23000000&body_text%5Bsize%5D=14px&body_text%5Bcolor%5D=%23363636&body_text%5Bshadow%5D=&top_headline%5Bsize%5D=36px&top_headline%5Bcolor%5D=%23ffffff&top_headline%5Bshadow%5D=%23000000&top_headline%5Bbackground%5D=rgba(0%2C+0%2C+0%2C+0.5)&headlines%5Bsize%5D=24px&headlines%5Bcolor%5D=%23363636&headlines%5Bshadow%5D=&headlines%5Bhover%5D=%23cf0000&boxes%5Bcaption%5D=rgba(255%2C+255%2C+255%2C+0.95)&boxes%5Bmeta%5D=rgba(196%2C+196%2C+196%2C+0.91)&boxes%5Bseparator%5D=%23ffffff&links%5Bcolor%5D=%23db0000&links%5Bhover%5D=%23ff0000&sidebars%5Bheadlines_color%5D=&sidebars%5Bheadlines_shadow%5D=&sidebars%5Btext_color%5D=&sidebars%5Btext_shadow%5D=&sidebars%5Blinks_color%5D=%233d3d3d&sidebars%5Blinks_hover%5D=%23ff0000&sidebars%5Blinks_shadow%5D=&sidebars%5Bbackground%5D=rgba(255%2C+255%2C+255%2C+0.96)&sidebars%5Bseparator%5D=%23e8e8e8&buttons%5Bbg_color%5D=%23333333&buttons%5Btext_color%5D=%23ffffff&buttons%5Btext_shadow%5D=%23000000&buttons%5Bbg_hover%5D=%23ff0000&slider%5Bbg_color%5D=rgba(0%2C+0%2C+0%2C+0.55)&slider%5Btext_color%5D=%23ffffff&slider%5Btext_shadow%5D=%23000000&css=&js=';
            $default_options = 'logo_margin=0px&tagline_margin=10px&main_nav_top=27px&contact_email=&tracking_code=&search_layout=1sidebar&yo_background_type=slideshow&yo_background_color=%23000000&yo_background_pattern=05&yo_background_standard%5Bimage%5D=&yo_background_standard%5Bposition%5D=center&yo_background_standard%5Brepeat%5D=no-repeat&yo_background_slideshow_settings%5Btransition_duration%5D=700&yo_background_slideshow_settings%5Bdelay%5D=7000&yo_background_slideshow%5B0%5D%5Bmedia%5D=93&yo_background_video%5Bvideo_type%5D=embed&yo_background_video%5Bvideo_url%5D=&yo_background_video%5Bvideo_mp4%5D=&yo_background_video%5Bvideo_ogv%5D=&yo_font=Noticia%2BText%3A700&yo_body_font=Open%2BSans%3Aregular&branding%5Blogo_size%5D=48px&branding%5Blogo_color%5D=%23e80000&branding%5Btagline_size%5D=14px&branding%5Btagline_color%5D=%23ffffff&branding%5Bshadow%5D=%23000000&branding%5Bbackground%5D=rgba(0%2C+0%2C+0%2C+0.75)&navigation%5Bsize%5D=14px&navigation%5Bmain_color%5D=%23ffffff&navigation%5Bmain_background%5D=&navigation%5Bmain_shadow%5D=%23000000&navigation%5Bmain_hover_color%5D=&navigation%5Bmain_hover_background%5D=rgba(255%2C+255%2C+255%2C+0.25)&navigation%5Bmain_hover_shadow%5D=&navigation%5Bsubmenu_color%5D=%23ffffff&navigation%5Bsubmenu_background%5D=rgba(0%2C+0%2C+0%2C+0.9)&navigation%5Bsubmenu_shadow%5D=&navigation%5Bcurrent_color%5D=%23ffffff&navigation%5Bcurrent_background%5D=rgba(237%2C+0%2C+0%2C+0.82)&navigation%5Bcurrent_shadow%5D=%23000000&body_text%5Bsize%5D=14px&body_text%5Bcolor%5D=%23363636&body_text%5Bshadow%5D=&top_headline%5Bsize%5D=36px&top_headline%5Bcolor%5D=%23ffffff&top_headline%5Bshadow%5D=%23000000&top_headline%5Bbackground%5D=rgba(0%2C+0%2C+0%2C+0.5)&headlines%5Bsize%5D=24px&headlines%5Bcolor%5D=%23363636&headlines%5Bshadow%5D=&headlines%5Bhover%5D=%23cf0000&boxes%5Bcaption%5D=rgba(255%2C+255%2C+255%2C+0.95)&boxes%5Bmeta%5D=rgba(196%2C+196%2C+196%2C+0.91)&boxes%5Bseparator%5D=%23ffffff&links%5Bcolor%5D=%23db0000&links%5Bhover%5D=%23ff0000&sidebars%5Bheadlines_color%5D=&sidebars%5Bheadlines_shadow%5D=&sidebars%5Btext_color%5D=&sidebars%5Btext_shadow%5D=&sidebars%5Blinks_color%5D=%233d3d3d&sidebars%5Blinks_hover%5D=%23ff0000&sidebars%5Blinks_shadow%5D=&sidebars%5Bbackground%5D=rgba(255%2C+255%2C+255%2C+0.96)&sidebars%5Bseparator%5D=%23e8e8e8&buttons%5Bbg_color%5D=%23333333&buttons%5Btext_color%5D=%23ffffff&buttons%5Btext_shadow%5D=%23000000&buttons%5Bbg_hover%5D=%23ff0000&slider%5Bbg_color%5D=rgba(0%2C+0%2C+0%2C+0.55)&slider%5Btext_color%5D=%23ffffff&slider%5Btext_shadow%5D=%23000000&css=&js=';
            wp_parse_str(stripslashes($default_options), $default_options);
            update_option('yo_theme_options', $default_options);
        }
    }
}

add_action('after_setup_theme', 'yo_theme_setup');


/*
 *------------------------------------------------------------------------------------------------
 * Admin bar Menu Items
 *------------------------------------------------------------------------------------------------
 *
 * Add some items to the admin menu, like the theme-options link to quickly customize the theme.
 *
 */

function admin_menu_items($admin_bar)
{
    $args = array(
            'id'    => 'theme-options',
            'title' => __('Theme Options', 'theme_admin'),
            'href'  => site_url() .'/wp-admin/themes.php?page=yo-theme-options',
            'meta'  => array('title' => __('Click to edit the theme options', 'theme_admin'))
            );
 
    $admin_bar->add_menu($args);
}

add_action('admin_bar_menu', 'admin_menu_items',  100);


/*
 *------------------------------------------------------------------------------------------------
 * Register Widgets (Sidebars)
 *------------------------------------------------------------------------------------------------
 *
 * These are the widget areas that can be customized in the Admin section (Appearance > Widgets).
 *
 */

function yo_widgets_init()
{
    // Better Recent Posts Widget
    include_once('library/widget-recent-posts.php');
    register_widget('YO_Better_Recent_Posts');

    // Flickr Widget
    include_once('library/widget-flickr.php');
    register_widget('YO_Hey_Its_Flickr');

    // Widget areas
    register_sidebar(array(
        'name' => 'Blog Sidebar 1',
        'id' => 'sidebar1',
        // 'class' => 'caption meta',
        'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'Blog Sidebar 2',
        'id' => 'sidebar2',
        // 'class' => 'caption meta',
        'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'Before Header',
        'id' => 'before-header',
        'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'Before Loop',
        'id' => 'before-loop',
        'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'After Loop',
        'id' => 'after-loop',
        'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));

    // register_sidebar(array(
    //     'name' => 'Before Single Post',
    //     'id' => 'before-post',
    //     // 'class' => 'caption meta',
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h3 class="widget-title">',
    //     'after_title' => '</h3>'));

    // register_sidebar(array(
    //     'name' => 'After Single Post',
    //     'id' => 'after-post',
    //     // 'class' => 'caption meta',
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h3 class="widget-title">',
    //     'after_title' => '</h3>'));

    register_sidebar(array(
        'name' => 'Footer',
        'id' => 'footer-widgets',
        // 'class' => 'caption meta',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'));
}

add_action('widgets_init', 'yo_widgets_init');




/*
 *------------------------------------------------------------------------------------------------
 * Enqueue CSS & Javascript in the frontend
 *------------------------------------------------------------------------------------------------
 *
 */

// Enqueue scripts
function yo_enqueue_scripts()
{
    // CSS
    // wp_enqueue_style('yo-googlefont', 'http://fonts.googleapis.com/css?family=Noticia+Text:700');
    wp_enqueue_style('yo-plugins', get_template_directory_uri().'/assets/css/plugins.css', false, '1.0');
    wp_enqueue_style('yo-layout', get_template_directory_uri().'/assets/css/layout.css', false, '1.0');

    // Javascript
    wp_enqueue_script('yo-modernizr', get_template_directory_uri().'/assets/js/vendor/modernizr-2.6.2.min.js', false, YO_VER);
    wp_register_script('yo-easing', get_template_directory_uri().'/assets/js/jquery.easing.min.js', array('jquery'), YO_VER, true);
    wp_register_script('yo-plugins', get_template_directory_uri().'/assets/js/plugins.js', array('jquery', 'yo-easing'), YO_VER, true);    
    if (is_singular() && comments_open() && get_option('thread_comments')) wp_enqueue_script('comment-reply');   

    // Register the main JS
    wp_register_script('yo-main', get_template_directory_uri().'/assets/js/main.js', array('jquery', 'yo-plugins'), YO_VER, true);
    wp_localize_script('yo-main', 'contact_data', array('ajaxurl' => admin_url('admin-ajax.php'), 'thank_you' => __('Thank you for contacting us!', 'theme_admin')));

    // Load the main JS
    wp_enqueue_script('yo-main');
}

add_action('wp_enqueue_scripts', 'yo_enqueue_scripts');




/*
 *------------------------------------------------------------------------------------------------
 * Custom stuff
 *------------------------------------------------------------------------------------------------
 *
 *
 */

include('yow/init.php');
include('library/metaboxes.php');
include('library/theme-options.php');
include('library/shortcodes.php');
include('library/theme-functions.php');
include('library/custom.php');



/*
 *------------------------------------------------------------------------------------------------
 * Switch Theme
 *------------------------------------------------------------------------------------------------
 *
 * Executed once after theme installation.
 *
 */
// function yo_switch_theme()
// {
// }

// add_action('switch_theme', 'yo_switch_theme');



/*
 *------------------------------------------------------------------------------------------------
 * Highlight Current Menu Items
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_current_menu_item_class($classes, $item)
{
    if (is_singular())
    {
        global $post;
        $post_cat = (array) wp_get_post_categories($post->ID);
        $page_cat = (array) get_field('yo_blog_categories', $item->object_id);

        // If the current post belongs to the page
        $result = array_intersect($post_cat, $page_cat);
        if (!empty($result))
        {
            $classes[] = 'current-menu-parent';
            $classes[] = 'current-menu-item';
        }

        if (isset($item->hasChildren) && !empty($item->hasChildren))
        {
            foreach ($item->hasChildren as $children)
            {
                $page_cat = (array) get_field('yo_blog_categories', $children);
                $result = array_intersect($post_cat, $page_cat);

                if (!empty($result))
                {
                    $classes[] = 'current-menu-parent';
                    $classes[] = 'current-menu-item';
                }
            }
        }
    }

    return $classes;
}

add_filter('nav_menu_css_class', 'yo_current_menu_item_class', 1, 2);


class YO_Walker extends Walker_Nav_Menu
{
    function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        // check, whether there are children for the given ID and append it to the element

        if (isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]))
        {
            $children = $children_elements[$element->ID];
            if (!isset($element->hasChildren)) $element->hasChildren = array();

            foreach ($children as &$child)
                $element->hasChildren[] = $child->object_id;
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}




// Required by Wordpress
if ( ! isset( $content_width ) )
    $content_width = 1200; /* pixels */