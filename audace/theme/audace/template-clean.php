<?php
/*
Template Name: Landing Page (No Header/Footer)
*/

get_header(); ?>


<section id="content" role="main" class="clearfix row top fullwidth-template">

	<div class="hentry grid_12">
        
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
        <?php endif; ?>

	</div>

</section>


<?php get_footer(); ?>