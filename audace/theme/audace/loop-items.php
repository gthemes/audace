<?php $original_entry_class = $entry_class; ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
    <?php 

        // Add a some classes for the fluid layout
        if ($page_layout == 'fluid' && (get_post_format() == 'image'||
                                        get_post_format() == 'video' ||
                                        get_post_format() == 'gallery'))
        {
            $entry_class = ' big ';
            // $entry_class = 'box grid_6 ';
        }
        else
        {
            $entry_class = $original_entry_class;
        }

    ?>

    <!-- Post -->
    <?php
        if (get_post_format())
        {
            @include(locate_template('library/format-'.get_post_format().'.php'));
        }
        else
        {
            include(locate_template('library/format-standard.php'));
        }
    ?> <!-- / Post -->
<?php endwhile; // Loop ?>

<?php else: ?>
    <article <?php post_class($entry_class); ?>>
        <div class="caption">
            <div class="entry-content">
                <p><?php _e('Nothing found!', 'theme_admin'); ?></p>
            </div>
        </div>
    </article>
<?php endif; ?>