<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title(' | ', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php $before_header = get_field('yo_before_header'); if ($before_header == NULL || $before_header == true) : ?>
<?php if (!is_page_template('template-clean.php') && function_exists('dynamic_sidebar') && is_active_sidebar('before-header')) : ?>
<div id="before-header" class="row clearfix">
    <div class="grid_12 horizontal-widgets">
        <div class="row">
            <?php dynamic_sidebar('before-header'); ?>
        </div>
    </div>
</div>
<?php endif; endif; ?>

<div class="header-wrapper">
	
	<?php if (!is_page_template('template-clean.php')) : ?>
	<header id="main-header" class="row">
		<div class="grid_12">

		    <div id="logo">
		        <a href="<?php echo site_url(); ?>">
		        	<?php if (get_field('logo_enabled', 'options')) : ?>
		        		<?php if (get_field('logo2x', 'options')) : $logo_attributes = wp_get_attachment_image_src(get_field('logo2x', 'options'), 'full'); ?>
			  				<img src="<?php echo $logo_attributes[0]; ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo round($logo_attributes[1]/2); ?>px;height:<?php echo round($logo_attributes[2]/2); ?>px;">
			  			<?php else : ?>
			  				<img src="<?php the_image(get_field('logo', 'options')); ?>" alt="<?php bloginfo('name'); ?>">
			  			<?php endif; ?>
			  		<?php else : ?>
			  			<h1><?php bloginfo('name'); ?></h1>
			  		<?php endif; ?>
		        </a>
		        <span id="tagline"><?php bloginfo('description'); ?></span>
		    </div>

		    <nav id="main-nav">
		        <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'nav-container', 'menu_class' => 'dropdown', 'fallback_cb' => false, 'walker' => new YO_Walker())); ?>
		    </nav>

		</div>
	</header>
	<?php endif; ?>

</div>