<?php if (isset($page_content)) : ?>
<div class="row">
    <div id="page-content" class="grid_12">
        <?php echo $page_content; ?>
    </div>
</div>
<?php endif; ?>

<div class="content-wrapper fullwidth-slides">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
            <div class="slide-content row">

                <div class="grid_6">
                    <div class="fullheight-wrapper">
                        <div class="fullheight-content">
                            <div class="header-alignleft"><h1 class="entry-title black-text-shadow"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1></div>
                            <?php $caption = get_field('yo_show_caption'); if ($caption == true OR $caption == NULL): ?>
                            <?php if (yo_has_excerpt()): ?><div class="header-alignleft black-text-shadow slide-excerpt"><?php the_excerpt(); ?></div><?php endif; ?>
                            <div class="header-alignleft"><p><?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a> '; ?><a class="mini button more-link" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php _e('More &rarr;', 'theme_admin'); ?></a></p></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="grid_6">
                    <div class="fullheight-wrapper">
                        <div class="fullheight-content">
                            <?php

                                if (get_post_format())
                                {
                                    switch (get_post_format())
                                    {
                                        case 'video':
                                            echo '<div class="video object">';
                                            yo_video();
                                            echo '</div>';
                                            break;

                                        case 'audio':
                                            echo '<div class="audio object">';
                                            yo_audio();
                                            echo '</div>';
                                            break;

                                        case 'gallery':
                                            $gallery = yo_gallery('compact-nav');
                                            if (!empty($gallery))
                                                echo '<div class="object">'.$gallery.'</div>';
                                            break;

                                        case 'quote':
                                            $quote_author = get_field('yo_quote_author');
                                            $quote_source = get_field('yo_quote_source');
                                            echo '<div class="object">';
                                            echo '<blockquote>';
                                            the_content();
                                            if ($quote_author) echo ($quote_source) ?
                                                '<cite><a href="'.$quote_source.'" target="_blank">'.$quote_author.'</a></cite>' :
                                                '<cite>'.$quote_author.'</cite>';
                                            echo '</blockquote>';
                                            echo '</div>';
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>

                <!-- Standard layout -->
                <!-- <div class="grid_12">
                    <div class="caption header-alignleft"><h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1></div>
                    <?php $caption = get_field('yo_show_caption'); if ($caption == true OR $caption == NULL): ?>
                    <?php if (yo_has_excerpt()): ?><div class="header-alignleft caption slide-excerpt"><?php the_excerpt(); ?></div><?php endif; ?>
                    <div class="header-alignleft"><p><?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a> '; ?><a class="mini button more-link" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php _e('More &rarr;', 'theme_admin'); ?></a></p></div>
                    <?php endif; ?>
                </div> -->
            </div>

            <div class="slide-next"></div>
            <div class="slides-overlay"></div>

            <?php if (has_post_thumbnail()): $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
            <script type="text/javascript" data-needed="true"> 
                jQuery(document).ready(function($) {
                    $('#post-<?php the_ID(); ?>').anystretch("<?php echo $img[0]; ?>");
                });
            </script>
            <?php endif; ?>
        </article>
        
    <?php endwhile; // Loop ?>

    <?php else: ?>
        <article <?php post_class($entry_class); ?>>
            <div class="caption">
                <div class="entry-content">
                    <p><?php _e('Nothing found!', 'theme_admin'); ?></p>
                </div>
            </div>
        </article>
    <?php endif; ?>

</div>