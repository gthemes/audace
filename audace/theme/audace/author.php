<?php

if (have_posts())
{
	the_post();
	$page_title = __('Posts by', 'theme_admin') .' '. get_the_author();
}

include(locate_template('index.php'));