<?php 
	$quote_author = get_field('yo_quote_author');
	$quote_source = get_field('yo_quote_source');
?>

<?php if (is_singular()) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
    	<div class="caption">
			<blockquote>
				<?php the_content(); ?>
				<?php if ($quote_author) echo ($quote_source) ? '<cite><a href="'.$quote_source.'" target="_blank">'.$quote_author.'</a></cite>' : '<cite>'.$quote_author.'</cite>'; ?>
			</blockquote>
            <?php wp_link_pages(); ?>
            <?php yo_post_languages(); ?>
            <?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<p><a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a></p>'; ?>
		</div>
        <div class="caption meta">
            <p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
            <!-- Post tags -->
            <?php the_tags('<p><strong>'.__('Tags: ', 'theme_admin').'</strong>', ', ', '</p>'); ?>
        </div>
        <?php comments_template(); ?>
    </article>

<?php else : ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
		<div class="caption">
			<blockquote>
				<?php the_content(); ?>
				<?php if ($quote_author) echo ($quote_source) ? '<cite><a href="'.$quote_source.'" target="_blank">'.$quote_author.'</a></cite>' : '<cite>'.$quote_author.'</cite>'; ?>
			</blockquote>
            <?php $caption = get_field('yo_show_caption'); if ($caption == true OR $caption == NULL): ?>
                <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                <p class="entry-date"><?php echo get_the_date(); ?></p>
                <?php if (yo_has_excerpt()): ?><div class="entry-content"><?php the_excerpt(); ?></div><?php endif; ?>
                <p><?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a> '; ?><a class="mini button more-link" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php _e('More &rarr;', 'theme_admin'); ?></a></p>
            <?php endif; ?>
		</div>

		

        <?php $meta_caption = get_field('yo_show_additional_meta'); if ($meta_caption == true OR $meta_caption == NULL): ?>
        <div class="caption meta">
            <p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
        </div>
        <?php endif; ?>
	</article>

<?php endif; ?>