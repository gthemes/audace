<?php if (is_singular()) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
        <?php if (has_post_thumbnail()): ?>
        <div class="object">
            <?php $anchor_href_class = yo_lightbox(true); ?>
            <a title="<?php the_title(); ?>"<?php echo $anchor_href_class; ?>>
                <?php the_post_thumbnail('full', array('class' => 'max-img')); ?>
            </a>
        </div>
        <?php endif; ?>

        <div class="caption clearfix">
            <?php $show_title = get_field('yo_show_title_inside'); if ($show_title == true OR $show_title == NULL): ?><h3 class="entry-title"><?php the_title(); ?></h3><?php endif; ?>
            <p class="entry-date"><?php echo get_the_date(); ?></p>
            <div class="entry-content"><?php the_content(); ?></div>
            <?php wp_link_pages(); ?>
            <?php yo_post_languages(); ?>
            <?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<p><a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a></p>'; ?>
        </div>
        <div class="caption meta">
            <p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
            <!-- Post tags -->
            <?php the_tags('<p><strong>'.__('Tags: ', 'theme_admin').'</strong>', ', ', '</p>'); ?>
        </div>
        <?php comments_template(); ?>
    </article>

<?php else : ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
        <?php if (has_post_thumbnail()): ?>
        <div class="object">
            <?php $anchor_href_class = yo_lightbox(); ?>
            <a title="<?php printf(__('More about %s', 'theme_admin'), get_the_title()); ?>"<?php echo $anchor_href_class; ?>>
                <?php the_post_thumbnail($image_size, array('alt' => get_the_title(), 'title' => get_the_title(), 'class' => 'max-img')); ?>
                <?php yo_overlay(); ?>
            </a>            
        </div>
        <?php endif; ?>
        <?php $caption = get_field('yo_show_caption'); if ($caption == true OR $caption == NULL): ?>
        <div class="caption">
            <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <p class="entry-date"><?php echo get_the_date(); ?></p>
            <?php if (yo_has_excerpt()): ?><div class="entry-content"><?php the_excerpt(); ?></div><?php endif; ?>
            <p><?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a> '; ?><a class="mini button more-link" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php _e('More &rarr;', 'theme_admin'); ?></a></p>
        </div>
        <?php endif; ?>

        <?php $meta_caption = get_field('yo_show_additional_meta'); if ($meta_caption == true OR $meta_caption == NULL): ?>
        <div class="caption meta">
            <?php if (comments_open()) : ?><p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p><?php endif; ?>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
        </div>
        <?php endif; ?>
    </article>

<?php endif; ?>