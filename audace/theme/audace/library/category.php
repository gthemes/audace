<?php

/*
 * Hooking into the "category" screen
 *
 */

$cat_page_layout_fields = array(
    array(
        'name'  => __('Page layout', 'theme_admin'),
        'id'    => 'blog_layout',
        'type'  => 'select',
        'options' => array(
            'fullwidth' => __('Fullwidth', 'theme_admin'),
            '1sidebar'  => __('1 Column + 1 Sidebar', 'theme_admin'),
            '2sidebars' => __('1 Column + 2 Sidebars', 'theme_admin'),
            '2col'      => __('2 Columns', 'theme_admin'),
            '2col-1sidebar'  => __('2 Columns + 1 Sidebar', 'theme_admin'),
            '2col-2sidebars' => __('2 Columns + 2 Sidebars', 'theme_admin'),
            '3col'      => __('3 Columns', 'theme_admin'),
            '3col-1sidebar'  => __('3 Columns + 1 Sidebar', 'theme_admin'),
            '4col'      => __('4 Columns', 'theme_admin'),
            '4col-1sidebar'  => __('4 Columns + 1 Sidebar', 'theme_admin'),
        ),
        // 'class'     => 'yo-chosen',
        // 'mod'       => 'chosen'
    ),
);

$cat_ppp_fields = array(
    array(
        'name'  => __('Posts per page', 'theme_admin'),
        'id'    => 'blog_posts_per_page',
        'type'  => 'text',
        'std'   => '10'
    ),
);

$cat_is_fields = array(
    array(
        'name'  => __('Infinite scroll', 'theme_admin'),
        'id'    => 'infinite_scroll',
        'type'  => 'checkbox',
        'std'   => false,
        'class' => 'yo-oneline'
    )
);

new Turbine_Taxonomy(array('category'), $cat_page_layout_fields);
new Turbine_Taxonomy(array('category'), $cat_ppp_fields);
new Turbine_Taxonomy(array('category'), $cat_is_fields);


/////////////////////////////////////////////////////////////////////////////////////////////////////////


// These are helper functions you can use elsewhere to access this info
function get_taxonomy_term_type($taxonomy, $term_id) {
    return get_option("_term_type_{$taxonomy}_{$term->term_id}");
}
function update_taxonomy_term_type($taxonomy, $term_id, $value) {
    update_option("_term_type_{$taxonomy}_{$term_id}", $value);
}

//This initializes the class.
// TaxonomyTermTypes::on_load();

//This should be called in your own code. This example uses two taxonomies: 'region' & 'opportunity'
// TaxonomyTermTypes::register_taxonomy(array('category'));


/////////////////////////////////////////////////////////////////////////////////////////////////////////


class Turbine_Taxonomy {

    var $ui;

    //This initializes the hooks to allow saving of the
    function __construct($taxonomy, $fields = array()) {
        add_action('created_term', array($this, 'term_type_update'), 10, 3);
        add_action('edit_term', array($this, 'term_type_update'), 10, 3);
        $this->register_taxonomy($taxonomy);
        $this->ui = new YO_Fields($fields);
    }
    //This initializes the hooks to allow adding the dropdown to the form fields
    function register_taxonomy($taxonomy) {
        if (!is_array($taxonomy))
            $taxonomy = array($taxonomy);
        foreach($taxonomy as $tax_name) {
            add_action("{$tax_name}_add_form_fields", array($this, "add_form_fields"));
            add_action("{$tax_name}_edit_form_fields", array($this, "edit_form_fields"), 10, 2);
        }
    }
    // This displays the selections. Edit it to retrieve
    function add_form_fields() {

        echo $this->ui->generate();

        // echo "Type " . self::get_select_html('text');
        // global $yo_category_ui;
        // echo $yo_category_ui->generate();
    }
    // This displays the selections. Edit it to retrieve your own terms however you retrieve them.
    function get_select_html($selected) {
        // var_dump($selected);
        // global $yo_category_ui;
        // return $yo_category_ui->generate($selected);
        // echo 'oooooooooooooooook';

        // 'fullwidth' => __('Fullwidth', 'theme_admin'),
        // '1sidebar'  => __('1 Column + 1 Sidebar', 'theme_admin'),
        // '2sidebars' => __('1 Column + 2 Sidebars', 'theme_admin'),
        // '2col'      => __('2 Columns', 'theme_admin'),
        // '2col-1sidebar'  => __('2 Columns + 1 Sidebar', 'theme_admin'),
        // '2col-2sidebars' => __('2 Columns + 2 Sidebars', 'theme_admin'),
        // '3col'      => __('3 Columns', 'theme_admin'),
        // '3col-1sidebar'  => __('3 Columns + 1 Sidebar', 'theme_admin'),
        // '4col'      => __('4 Columns', 'theme_admin'),
        // '4col-1sidebar'  => __('4 Columns + 1 Sidebar', 'theme_admin'),

        // return $this->ui->generate($selected);

        // $selected_attr = array('fullwidth'=>'', '1sidebar'=>'', '2sidebars'=>'', '2col'=>'', '2col-1sidebar'=>'', '2col-2sidebars'=>'', '3col'=>'', '3col-1sidebar'=>'', '4col'=>'', '4col-1sidebar'=>'');
        // $selected_attr[$selected] = ' selected="selected"';
        // $html = '
        // <select id="tag-layout" name="tag-layout">
        //     <option value="fullwidth"'.$selected_attr['fullwidth'].'>'.__('Fullwidth', 'theme_admin').'</option>
        //     <option value="1sidebar"'.$selected_attr['1sidebar'].'>'.__('1 Column + 1 Sidebar', 'theme_admin').'</option>
        //     <option value="2sidebars"'.$selected_attr['2sidebars'].'>'.__('1 Column + 2 Sidebars', 'theme_admin').'</option>
        //     <option value="2col"'.$selected_attr['2col'].'>'.__('2 Columns', 'theme_admin').'</option>
        //     <option value="2col-1sidebar"'.$selected_attr['2col-1sidebar'].'>'.__('2 Columns + 1 Sidebar', 'theme_admin').'</option>
        //     <option value="2col-2sidebars"'.$selected_attr['2col-2sidebars'].'>'.__('2 Columns + 1 Sidebar', 'theme_admin').'</option>
        //     <option value="3col"'.$selected_attr['3col'].'>'.__('3 Columns', 'theme_admin').'</option>
        //     <option value="3col-1sidebar"'.$selected_attr['3col-1sidebar'].'>'.__('3 Columns + 1 Sidebar', 'theme_admin').'</option>
        //     <option value="4col"'.$selected_attr['4col'].'>'.__('4 Columns', 'theme_admin').'</option>
        //     <option value="4col-1sidebar"'.$selected_attr['4col-1sidebar'].'>'.__('4 Columns + 1 Sidebar', 'theme_admin').'</option>
        // </select>';

        // return $html;
    }
        // This a table row with the drop down for an edit screen

    function get_text_html($data) {
        return "<input type='text' id='tag-ppp' name='tag-ppp' value='".$data."' size='40' />";
    }

    function edit_form_fields($term, $taxonomy) {
        $selected = get_option("_term_type_{$taxonomy}_{$term->term_id}");
        // echo '<br>------------<br>';
        // echo '<pre>';
        // var_dump($term);
        // var_dump($taxonomy);
        // var_dump($selected);
        // echo '</pre>';

        // global $yo_category_ui;
        // echo $yo_category_ui->generate($selected);
        // $select = self::get_select_html($selected);
        // $text = self::get_text_html($selected);
        // $select = $yo_category_ui->generate($selected);

        // var_dump(get_taxonomy_term_type('category', $term->term_id));

        $html = '
        <tr class="form-field form-required">
            <th scope="row" valign="top"><label for="tag-layout">'.__('Category Layout', 'theme_admin').'</label></th>
            <td>'.$this->ui->generate($selected).'</td>
        </tr>';

        // $html = '
        // <tr class="form-field form-required">
        //     <th scope="row" valign="top"><label for="tag-layout">'.__('Category Layout', 'theme_admin').'</label></th>
        //     <td>'.$select.'</td>
        // </tr>
        // <tr class="form-field form-required">
        //     <th scope="row" valign="top"><label for="tag-ppp">'.__('Posts per page', 'theme_admin').'</label></th>
        //     <td>'.$text.'</td>
        // </tr>';

        echo $html;
    }
    // These hooks are called after adding and editing to save $_POST['tag-term']
    function term_type_update($term_id, $tt_id, $taxonomy)
    {
        echo '<br>------------<br>';
        echo '<pre>';
        print_r($_POST);
        echo '<br>---<br>';
        var_dump($term_id);
        echo '<br>---<br>';
        var_dump($tt_id);
        echo '<br>---<br>';
        var_dump($taxonomy);
        echo '</pre>';

        foreach ($this->ui->fields as &$field)
        {
            echo $field['id'];
            if (isset($_POST[$field['id']])) {
                update_taxonomy_term_type($taxonomy, $term_id, $_POST[$field['id']]);
            }
        }

        // if (isset($_POST['tag-layout'])) {
        //     update_taxonomy_term_type($taxonomy, $term_id, $_POST['tag-layout']);
        // }

        // if (isset($_POST['tag-ppp'])) {
        //     update_taxonomy_term_type($taxonomy, $term_id, $_POST['tag-ppp']);
        // }
    }
}
