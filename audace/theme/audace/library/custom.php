<?php

/*
 *------------------------------------------------------------------------------------------------
 * Custom page styles
 *------------------------------------------------------------------------------------------------
 *
 */
global $yo_half_page_background;
$yo_half_page_background = false;

// Output custom CSS and JS to the frontend footer
function yo_page_customizations()
{  
    // Get current page outside the loop
    global $wp_query;
    $current_page = $wp_query->get_queried_object_id();
    
    // 
    // BACKGROUND
    // 
    $background_type = get_field('yo_background_type', $current_page);
    $source = ($background_type && $background_type != 'default') ? $current_page : 'option';
    if ($source == 'option') $background_type = get_field('yo_background_type', 'option');


    // Page background
    $background_color = get_field('yo_background_color', $source);
    $background_pattern = get_field('yo_background_pattern', $source);
    if ($background_pattern) yo_css('#background-overlay { background-image:url("'.get_template_directory_uri().'/assets/img/overlays/'.$background_pattern.'.png"); }', false, false);
    //
    $images = get_field('yo_background_slideshow', $source);             
    $settings = get_field('yo_background_slideshow_settings', $source); 
    
    if (empty($images)) $images = array();
    $body_css = '';
    
    // Body background color
    if ( ! empty($background_color)) $body_css .= yo_css_color($background_color, 'background-color:');
    // if ( ! empty($background_color)) $body_css .= 'background-color:'.$background_color.';';

    switch ($background_type)
    {
        case 'standard':
            $background = get_field('yo_background_standard', $source);
            // Body background image
            if ( ! empty($background['image'])) $body_css .= 'background-image: url("'.get_the_image($background['image']).'");';
            // Body background repeat
            if ( ! empty($background['repeat'])) $body_css .= 'background-repeat: '.$background['repeat'].';';
            // Body background position
            if ( ! empty($background['position'])) $body_css .= 'background-position: '.$background['position'].';';
            // yo_css('#supersized-loader, #supersized { display:none; }', false, false);
            break;

        case 'slideshow':
            $slides = '';
            foreach ($images as &$image)
            {
                // title: '".$image['title']."', 
                // $url = (isset($image['link'])) ? ",url: '".$image['link']."'" : "";
                // $slides .= "{image: '".get_the_image($image['media'])."', thumb: '".get_the_image($image['media'], 'thumbnail')."'".$url."},";
                $slides .= '"'.get_the_image($image['media']).'",';
            }

            $full_page = (isset($settings['full_page'])) ? $settings['full_page'] : false;

            if ($full_page == true)
            {
                $bg_target = 'body';
            }
            else
            {
                $bg_target = '.half-slider';
                // yo_jquery('$(window).resize(function(){ $(".half-slider").css("margin-top", "-"+$("#main-header").height()+"px"); });', true, false);
                // yo_css('.page-title { margin-top:255px; }', false, false);
                yo_css('#next-slide { bottom:auto; top:0; }', false, false);
                yo_css('#prev-slide { bottom:auto; top:23px; }', false, false);
                // yo_html('<div class="half-slider"></div>', true, false);
                global $yo_half_page_background;
                $yo_half_page_background = true;
            }

            // Set the library
            $background_js  = 'setTimeout(function(){ $("'.$bg_target.'").backstretch(['.rtrim($slides, ',').'], {fade:'.$settings['transition_duration'].', duration:'.$settings['delay'].'});';
            $background_js .= 'var background_slideshow = $("'.$bg_target.'").data("backstretch");';

            if (isset($settings['controls']) && $settings['controls'])
            {
                yo_html('<div id="next-slide"></div><div id="prev-slide"></div>', true, false);
                $background_js .= "$('#next-slide').click(function(){ background_slideshow.next(); }); $('#prev-slide').click(function(){ background_slideshow.prev(); });";
            }

            if (!isset($settings['autostart']) || !$settings['autostart'])
                $background_js .= "background_slideshow.pause();";

            $background_js .= '}, 300);';
        
            yo_jquery($background_js, true, false);

            // wp_enqueue_script('yo-supersized', get_template_directory_uri().'/assets/js/supersized.js', array('jquery'), YO_VER, true);
            // yo_jquery("$.supersized({ horizontal_center:true, min_height:480, slide_interval:".$settings['delay'].", transition:'".$settings['transition']."', transition_speed:".$settings['transition_duration'].", slide_links:'blank', slides:[".rtrim($slides, ',')."] });", true, false);
            break;

        case 'video':          

            // Get background video settings
            $background_video = get_field('yo_background_video');

            // Embed (Youtube, Vimeo...)
            if (isset($background_video['video_type']) && $background_video['video_type'] == 'embed')
            {
                if (!empty($background_video['video_url']))
                {
                    $video_id = false;
                    $provider = false;
                    $parsed_url = parse_url($background_video['video_url']);

                    if (strpos($background_video['video_url'], 'youtube.com') !== false)
                    {
                        if (isset($parsed_url['query']))
                            if (strpos($parsed_url['query'], 'v=') === 0)
                            {
                                parse_str($parsed_url['query'], $query_args);
                                if (isset($query_args['v']))
                                {
                                    $provider = 'youtube';
                                    $video_id = $query_args['v'];
                                }
                            }
                    }
                    else if (strpos($background_video['video_url'], 'youtu.be') !== false)
                    {
                        if (isset($parsed_url['path']))
                        {
                            $provider = 'youtube';
                            $video_id = ltrim($parsed_url['path'], '/');
                        }
                    }
                    else if (strpos($background_video['video_url'], 'vimeo.com') !== false)
                    {
                        $provider = 'vimeo';
                        // $video_id = $parsed_url['path'];   
                        $video_id = ltrim($parsed_url['path'], '/');
                    }

                    if ($provider == 'youtube')
                    {
                        wp_enqueue_script('yo-tubular', get_template_directory_uri().'/assets/js/tubular.js', array('jquery'), YO_VER, true);
                        yo_jquery('$("body").tubular({videoId:"'.$video_id.'"});', true, false);
                    }
                    else if ($provider == 'vimeo')
                    {

                        wp_enqueue_script('yo-okvideo', get_template_directory_uri().'/assets/js/okvideo.min.js', array('jquery'), YO_VER, true);
                        yo_jquery('$.okvideo({video: "'.$background_video['video_url'].'", volume: 0, hd: true });', true, false);
                    }
                }
            }
            // Self hosted
            else if (isset($background_video['video_type']) && $background_video['video_type'] == 'hosted')
            {
                wp_enqueue_style('yo-videojs', 'http://vjs.zencdn.net/c/video-js.css', false, YO_VER, true);
                wp_enqueue_script('jquery-ui-core');
                wp_enqueue_script('jquery-ui-slider');
                wp_enqueue_script('yo-videojs', 'http://vjs.zencdn.net/c/video.js', array('jquery'), YO_VER, true);
                wp_enqueue_script('yo-bigvideo', get_template_directory_uri().'/assets/js/bigvideo.js', array('jquery'), YO_VER, true);

                $video_mp4 = (isset($background_video['video_mp4'])) ? wp_get_attachment_url($background_video['video_mp4']) : '';
                $video_ogv = (isset($background_video['video_ogv'])) ? wp_get_attachment_url($background_video['video_ogv']) : '';
                $video_controls = (isset($background_video['video_controls']) && $background_video['video_controls']) ? 'true' : 'false';
                $video_mute = (isset($background_video['video_mute']) && $background_video['video_mute']) ? 'true' : 'false';
                $video_loop = (isset($background_video['video_loop']) && $background_video['video_loop']) ? 'true' : 'false';

                $poster = get_the_post_thumbnail(false, 'full');
                $poster = ($poster && isset($poster['src'])) ? $poster['src'] : '';

                $background_video_output  = 'var BV = new $.BigVideo({useFlashForFirefox:false, ambient:'.$video_mute.', controls:'.$video_controls.', loop:'.$video_loop.'}); BV.init(); if (Modernizr.touch) { BV.show('.$poster.'); }';
                $background_video_output .= "else { BV.show('".$video_mp4."', {altSource:'".$video_ogv."'}); }";
                yo_jquery($background_video_output, true, false);
            }
            

            // yo_jquery("$(function() {var BV = new $.BigVideo({useFlashForFirefox:false, controls:".$video_controls.", loop:".$video_loop."}); BV.init(); BV.show('".$video_mp4."', {altSource:'".$video_ogv."', ambient:".$video_mute."}); });", true, false);


            // wp_enqueue_script('yo-videobg', get_template_directory_uri().'/assets/js/videobg.js', array('jquery'), YO_VER, true);
            // yo_jquery("$('body').videoBG({position:'fixed', zIndex:-1, mp4:'http://localhost/wordpress3/wp-content/uploads/2012/10/video.mp4', ogv:'http://localhost/wordpress3/wp-content/uploads/2012/10/video.ogv', webm:'http://localhost/wordpress3/wp-content/uploads/2012/10/video.webm', opacity:0.5});", true, false);

            break;

        default:
            break;
    }


    // Links color
    $links = get_field('links', 'option');
    if ( ! empty($links['color']))
        yo_css('a, a:link, a:active, a:visited { '.yo_css_color($links['color'], 'color:').' }', false, false);
    if ( ! empty($links['hover']))
        yo_css('a:hover, .meta.caption a:hover { '.yo_css_color($links['hover'], 'color:').' }', false, false);

    // Body color
    $body_text = get_field('body_text', 'option');
    if ( ! empty($body_text['color']))
    {
        $body_css .= yo_css_color($body_text['color'], 'color:');
        yo_css('.meta.caption a { '.yo_css_color($body_text['color'], 'color:').' }', false, false);
    }
    if ( ! empty($body_text['shadow']))
        yo_css('p { text-shadow: 0 1px 0 '.$body_text['shadow'].'; }', false, false);
    if ( ! empty($body_text['size']))
        yo_css('body { font-size: '.$body_text['size'].'; }', false, false);
        // yo_css('body,h1,h2,h4,p,#tagline { text-shadow: 0 1px 0 '.$body_text['shadow'].'; }', false, false);
        // $body_css .= 'text-shadow: 0 1px 0 '.$body_text['shadow'].'; }';
        // yo_css('#tagline { color: '.$body_text['tagline_color'].'; }', false, false);

    // Body CSS styles
    if ( ! empty($body_css)) yo_css('body { '.$body_css.' }', false, false);

    // Favicon2x
    $favicon2x = get_field('favicon2x', 'option');

    if ( ! empty($favicon2x))
    {
        yo_html('<link rel="apple-touch-icon" sizes="256x256" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="apple-touch-icon" sizes="144x144" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="apple-touch-icon" sizes="114x114" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="apple-touch-icon" sizes="72x72" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="icon" sizes="57x57" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="icon" sizes="32x32" href="'.get_the_image($favicon2x).'"/>', false, false);
    }

    // Favicon
    $favicon = get_field('favicon', 'option');
    if ( ! empty($favicon))
    {
        yo_html('<link rel="icon" sizes="16x16" href="'.get_the_image($favicon2x).'"/>', false, false);
        yo_html('<link rel="icon" type="'.get_post_mime_type($favicon).'" href="'.get_the_image($favicon).'">', false, false);
    }

    // Headlines Font
    $headlines_font = get_field('yo_font', 'option');
    if (empty($headlines_font) || $headlines_font === NULL) $headlines_font = 'Noticia+Text:700';
    yo_custom_font($headlines_font, 'h1,h2,h3,h4,h5,h6,blockquote,blockquote p,#tagline');

    // Body Font
    $body_font = get_field('yo_body_font', 'option');
    if (empty($body_font) || $body_font === NULL) $body_font = 'Helvetica';
    yo_custom_font($body_font, 'body');

    // Logo & Tagline color
    $branding = get_field('branding', 'option');
    if ( ! empty($branding['logo_color']))
        yo_css('#logo a, #logo a:link, #logo a:active, #logo a:visited, #logo h1 { '.yo_css_color($branding['logo_color'], 'color:').' }', false, false);
    if ( ! empty($branding['logo_size']))
        yo_css('#logo h1 { font-size: '.$branding['logo_size'].'; }', false, false);
    if ( ! empty($branding['tagline_color']))
        yo_css('#tagline { '.yo_css_color($branding['tagline_color'], 'color:').' }', false, false);
    if ( ! empty($branding['tagline_size']))
        yo_css('#tagline { font-size: '.$branding['tagline_size'].'; }', false, false);
    if ( ! empty($branding['shadow']))
        yo_css('#logo a, #logo h1, #tagline { text-shadow: 0 1px 0 '.$branding['shadow'].'; }', false, false);
    
    if ( ! empty($branding['background']))
        yo_css('#main-header .grid_12 { '.yo_css_color($branding['background'], 'background-color:').' }', false, false);
    else
        yo_css('#logo { padding-left:0; }', false, false);   


    // Logo margin
    $logo_margin = get_field('logo_margin', 'option');
    if ( ! empty($logo_margin))
        yo_css('#logo { margin-top: '.$logo_margin.'; }', false, false);

    // Tagline margin
    $tagline_margin = get_field('tagline_margin', 'option');
    if ( ! empty($tagline_margin))
        yo_css('#tagline { margin-top: '.$tagline_margin.'; }', false, false);

    // Navigation top margin
    $main_nav_top = get_field('main_nav_top', 'option');
    if ( ! empty($main_nav_top))
    {
        yo_css('ul.dropdown li a { padding-top:'.$main_nav_top.'; }', false, false);
    }

    // Navigation
    $navigation = get_field('navigation', 'option');
    // First level
    if ( ! empty($navigation['main_color']))
        yo_css('ul.dropdown a, .tinynav { '.yo_css_color($navigation['main_color'], 'color:').' }', false, false);
    if ( ! empty($navigation['main_background']))
        yo_css('ul.dropdown li { '.yo_css_color($navigation['main_background'], 'background-color:').' }', false, false);
    if ( ! empty($navigation['main_shadow']))
        yo_css('ul.dropdown a { text-shadow: 0 1px 0 '.$navigation['main_shadow'].'; }', false, false);
    // First level hover
    if ( ! empty($navigation['main_hover_color']))
        yo_css('ul.dropdown a:hover { '.yo_css_color($navigation['main_hover_color'], 'color:').' }', false, false);
    if ( ! empty($navigation['main_hover_background']))
        yo_css('ul.dropdown li:hover { '.yo_css_color($navigation['main_hover_background'], 'background-color:').' }', false, false);
    if ( ! empty($navigation['main_hover_shadow']))
        yo_css('ul.dropdown a:hover { text-shadow: 0 1px 0 '.$navigation['main_hover_shadow'].'; }', false, false);
    // Submenu
    if ( ! empty($navigation['submenu_color']))
        yo_css('ul.dropdown ul li a { '.yo_css_color($navigation['submenu_color'], 'color:').' }', false, false);
    if ( ! empty($navigation['submenu_background']))
        yo_css('.sub-menu { '.yo_css_color($navigation['submenu_background'], 'background-color:').' }', false, false);
    if ( ! empty($navigation['submenu_shadow']))
        yo_css('ul.dropdown ul li a { text-shadow: 0 1px 0 '.$navigation['submenu_shadow'].'; }', false, false);
    // Current item
    if ( ! empty($navigation['current_color']))
        yo_css('ul.dropdown li.current-menu-item > a, ul.dropdown li.current-menu-parent > a { '.yo_css_color($navigation['current_color'], 'color:', ' !important').' }', false, false);
    if ( ! empty($navigation['current_background']))
        yo_css('ul.dropdown li.current-menu-item, ul.dropdown li.current-menu-parent { '.yo_css_color($navigation['current_background'], 'background-color:', ' !important').' }', false, false);
    if ( ! empty($navigation['current_shadow']))
        yo_css('ul.dropdown li.current-menu-item > a, ul.dropdown li.current-menu-parent > a { text-shadow: 0 1px 0 '.$navigation['current_shadow'].' !important; }', false, false);

    // Tracking code
    $tracking_code = get_field('tracking_code', 'option');
    if ( ! empty($main_nav_top))
        yo_html($tracking_code, true, false);

    // Top headline
    $top_headline = get_field('top_headline', 'option');
    if ( ! empty($top_headline['color']))
        yo_css('.page-title { '.yo_css_color($top_headline['color'], 'color:').' }', false, false);
    if ( ! empty($top_headline['shadow']))
        yo_css('.page-title { text-shadow: 0 1px 0 '.$top_headline['shadow'].'; }', false, false);
    if ( ! empty($top_headline['background']))
        yo_css('.page-title { '.yo_css_color($top_headline['background'], 'background-color:').' padding:15px 20px; }', false, false);
    if ( ! empty($top_headline['size']))
        yo_css('.page-title { font-size: '.$top_headline['size'].'; }', false, false);

    // Headlines
    $headlines = get_field('headlines', 'option');
    if ( ! empty($headlines['color']))
        yo_css('.entry-title, .entry-title a { '.yo_css_color($headlines['color'], 'color:').' }', false, false);
    if ( ! empty($headlines['shadow']))
        yo_css('.entry-title, .entry-title a { text-shadow: 0 1px 0 '.$headlines['shadow'].'; }', false, false);    
    if ( ! empty($headlines['hover']))
        yo_css('.entry-title a:hover { '.yo_css_color($headlines['hover'], 'color:').' }', false, false);
    if ( ! empty($headlines['size']))
        yo_css('.entry-title, .entry-title a { font-size: '.$headlines['size'].'; }', false, false);

    // Boxes
    $boxes = get_field('boxes', 'option');
    if ( ! empty($boxes['caption']))
        yo_css('.caption { '.yo_css_color($boxes['caption'], 'background-color:').' }', false, false);
    if ( ! empty($boxes['meta']))
        yo_css('.meta.caption { '.yo_css_color($boxes['caption'], 'background-color:').' }', false, false);
    if ( ! empty($boxes['separator']))
        yo_css('.meta.caption { '.yo_css_color($boxes['separator'], 'border-top: 1px solid ').' }', false, false);

    // if ( ! empty($boxes['caption']))
    //     yo_css('.caption { background-color: '.$boxes['caption'].'; }', false, false);
    // if ( ! empty($boxes['meta']))
    //     yo_css('.meta.caption { background-color: '.$boxes['meta'].'; }', false, false);
    // if ( ! empty($boxes['separator']))
    //     yo_css('.meta.caption { border-top: 1px solid '.$boxes['separator'].'; }', false, false);


    // Sidebars
    $sidebars = get_field('sidebars', 'option');
    if ( ! empty($sidebars['headlines_color']))
        yo_css('.widget-title { '.yo_css_color($sidebars['headlines_color'], 'color:').' }', false, false);
    if ( ! empty($sidebars['headlines_shadow']))
        yo_css('.widget-title { text-shadow: 0 1px 0 '.$sidebars['headlines_shadow'].'; }', false, false);
    if ( ! empty($sidebars['text_color']))
        yo_css('.widget { '.yo_css_color($sidebars['text_color'], 'color:').' }', false, false);
    if ( ! empty($sidebars['text_shadow']))
        yo_css('.widget { text-shadow: 0 1px 0 '.$sidebars['text_shadow'].'; }', false, false);
    if ( ! empty($sidebars['links_color']))
        yo_css('.widget a { '.yo_css_color($sidebars['links_color'], 'color:').' }', false, false);
    if ( ! empty($sidebars['links_hover']))
        yo_css('.widget a:hover { '.yo_css_color($sidebars['links_hover'], 'color:').' }', false, false);
    if ( ! empty($sidebars['links_shadow']))
        yo_css('.widget a { text-shadow: 0 1px 0 '.$sidebars['links_shadow'].'; }', false, false);
    if ( ! empty($sidebars['background']))
        yo_css('aside.sidebar, .horizontal-widgets { '.yo_css_color($sidebars['background'], 'background-color:').' }', false, false);
    if ( ! empty($sidebars['separator']))
        yo_css('aside.sidebar .widget { '.yo_css_color($sidebars['separator'], 'border-top:1px solid ').' }', false, false);

    // Buttons color
    $buttons = get_field('buttons', 'option');
    $bg_color = ( ! empty($buttons['bg_color'])) ? yo_css_color($buttons['bg_color'], 'background-color:') : '';
    $bg_hover = ( ! empty($buttons['bg_hover'])) ? yo_css_color($buttons['bg_hover'], 'background-color:') : '';
    $text_color = ( ! empty($buttons['text_color'])) ? yo_css_color($buttons['text_color'], 'color:') : '';
    $text_shadow = ( ! empty($buttons['text_shadow'])) ? 'text-shadow:0 1px 0 '.$buttons['text_shadow'].';' : '';
    yo_css('.button, a.button, button, input[type="submit"] { '.$bg_color.$text_color.$text_shadow.' }', false, false);
    yo_css('.button:hover, a.button:hover, button:hover, input[type="submit"]:hover { '.$bg_hover.' }', false, false);

    // Slider captions
    $slider = get_field('slider', 'option');
    $bg_color = ( ! empty($slider['bg_color'])) ? yo_css_color($slider['bg_color'], 'background-color:') : '';
    $text_color = ( ! empty($slider['text_color'])) ? yo_css_color($slider['text_color'], 'color:') : '';
    $text_shadow = ( ! empty($slider['text_shadow'])) ? 'text-shadow:0 1px 0 '.$slider['text_shadow'].';' : '';
    yo_css('.slider-caption { '.$bg_color.$text_color.$text_shadow.' }', false, false);


    // Retina display logo
    $logo2x = get_field('logo2x', 'option');
    if ( ! empty($logo2x))
    {
        $image_attributes = wp_get_attachment_image_src($logo2x, 'full');

        // yo_css('@media screen and (-webkit-min-device-pixel-ratio: 2), screen and (max--moz-device-pixel-ratio: 2) {
        //   #logo a img {
        //     visibility: hidden;
        //   }
        //   #logo a {
        //     background: url('.get_the_image($logo2x).') no-repeat;
        //     background-size: '.round($image_attributes[1]/2).'px '.round($image_attributes[2]/2).'px;
        //     padding: 3px 0;
        //     width: '.round($image_attributes[1]/2).'px;
        //     height: '.round($image_attributes[2]/2).'px;
        //   }
        // }', false, false);
    }

    // These have to stay at the bottom...

    // Custom CSS
    $custom_css = get_field('css', 'option');
    if ( ! empty($custom_css))
        yo_css($custom_css, false, false);

    // Custom Javascript
    $custom_js = get_field('js', 'option');
    if ( ! empty($custom_js))
        yo_js($custom_js, true, false, false);
}

add_action('wp_head', 'yo_page_customizations', 1);


function yo_custom_font($font, $selectors = 'h1')
{
    $standard_fonts = array('Arial', 'Courier', 'Georgia', 'Lucida', 'Times New Roman', 'Verdana');
    
    $font_id = str_replace(array('+',' ',':'), '', $font);

    if (!in_array($font, $standard_fonts))
        wp_enqueue_style('yo-googlefont-'.$font_id, 'http://fonts.googleapis.com/css?family='.$font, false, false);

    $font = explode(':', $font);
    $font_name = str_replace('+', ' ', $font[0]);
    $font_weight = 'normal';
    $font_style  = 'font-style:normal;';

    if (isset($font[1]))
    {        
        if (strpos($font[1], 'italic') || $font[1] == 'italic')
        {
            $font_style  = 'font-style:italic;';
            $font_weight = str_replace('italic', '', $font[1]);
        }
        else
        {
            $font_style  = 'font-style:normal;';
            $font_weight = $font[1];
        }
    }

    $font_weight = 'font-weight:'.$font_weight.';';
    yo_css($selectors.'{font-family:"'.$font_name.'",serif;'.$font_weight.$font_style.'}', false, false);
}

function yo_css_color($color, $attr, $append = '')
{
    // Set the color and attribute
    $output = $attr.$color.$append.';';

    // If the color is RGBA, prepend the attribute with HEX value for older browsers
    if (substr(strtolower($color), 0, 4) === 'rgba')
    {
        $rgba = str_ireplace('rgba', '', $color);
        $rgba = str_replace(')', '', $rgba);
        $rgba = str_replace('(', '', $rgba);
        $rgba = explode(',', $rgba);
        $hex = yo_rgb2html($rgba[0], $rgba[1], $rgba[2]);
        $output = $attr.$hex.$append.';'.$output;
    }

    return $output;
}

function yo_rgb2html($r, $g=-1, $b=-1)
{
    if (is_array($r) && sizeof($r) == 3)
        list($r, $g, $b) = $r;

    $r = intval($r); $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
    $color .= (strlen($g) < 2?'0':'').$g;
    $color .= (strlen($b) < 2?'0':'').$b;
    return '#'.$color;
}