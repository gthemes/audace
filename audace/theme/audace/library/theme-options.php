<?php 


// Theme Options
$theme_options = array();


// General Settings
$theme_options[] = array(
    'name'   => 'General Settings',
    'id'     => 'generalsettings',
    'fields' => array(
        //----------------------------------------------------------------------------------------
        // Logo Toggle
        //----------------------------------------------------------------------------------------
        array(
            'heading'  => __('General Settings', 'theme_admin'),
            'name'      => __('Enable logo image', 'theme_admin'),
            'id'        => 'logo_enabled',
            'desc'      => __('If this option is disabled the blog title will be used instead.', 'theme_admin'),
            'type'      => 'checkbox',
            'toggle'    => '#yo-section-logo, #yo-section-logo2x',
            'class'     => 'yo-toggle',
            'std'       => 0
        ),
        array(
            'name'     => __('Logo', 'theme_admin'),
            'id'       => 'logo',
            'desc'     => __('This is the logo at the original size, leave it empty to use the default title text.', 'theme_admin'),
            'type'     => 'media',
            'thumb'    => 'full'
        ),
        array(
            'name'  => __('Logo for retina display', 'theme_admin'),
            'id'    => 'logo2x',
            'desc'  => __('Upload a logo image which is exactly the <strong>double size of the standard logo</strong>. Leave it empty to disable this feature.', 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Logo Margin', 'theme_admin'),
            'id'    => 'logo_margin',
            'desc'  => __('The top margin for the logo, useful for alignment with different logo sizes.', 'theme_admin'),
            'type'  => 'text',
            'std'   => '0px'
        ),
        array(
            'name'  => __('Tagline Margin', 'theme_admin'),
            'id'    => 'tagline_margin',
            'desc'  => __('The top margin for the tagline, useful to align with the logo.', 'theme_admin'),
            'type'  => 'text',
            'std'   => '0px'
        ),
        array(
            'name'  => __('Main navigation position', 'theme_admin'),
            'id'    => 'main_nav_top',
            'desc'  => __('The top margin for the main navigation, useful to align the nav with the logo.', 'theme_admin'),
            'type'  => 'text',
            'std'   => '0px'
        ),
        array(
            'name'  => __('Standard Favicon', 'theme_admin'),
            'id'    => 'favicon',
            'desc'  => __("Upload a <strong>16px by 16px PNG/GIF/JPG/ICO</strong> image that will represent your website's favicon.", 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Favicon for retina display', 'theme_admin'),
            'id'    => 'favicon2x',
            'desc'  => __("Upload a <strong>256px by 256px PNG</strong> image that will represent your website's favicon on retina displays.", 'theme_admin'),
            'type'  => 'media',
            'thumb' => 'full'
        ),
        array(
            'name'  => __('Contact Form Email Address', 'theme_admin'),
            'id'    => 'contact_email',
            'desc'  => __('You will receive messages from the contact form to this email address. Leave it empty to use the default admin address.', 'theme_admin'),
            'type'  => 'text',
        ),
        array(
            'name'  => __('Tracking Code', 'theme_admin'),
            'id'    => 'tracking_code',
            'desc'  => __('Paste here the tracking code for the statistics (Google Analytics or other provider).', 'theme_admin'),
            'type'  => 'textarea',
        ),
        array(
            'name'  => __('Search results layout', 'theme_admin'),
            'id'    => 'search_layout',
            'type'  => 'select',
            'options' => array(
                'fullwidth' => __('Fullwidth', 'theme_admin'),
                '1sidebar'  => __('1 Column + 1 Sidebar', 'theme_admin'),
                '2sidebars' => __('1 Column + 2 Sidebars', 'theme_admin'),
                '2col'      => __('2 Columns', 'theme_admin'),
                '2col-1sidebar'  => __('2 Columns + 1 Sidebar', 'theme_admin'),
                '2col-2sidebars' => __('2 Columns + 2 Sidebars', 'theme_admin'),
                '3col'      => __('3 Columns', 'theme_admin'),
                '3col-1sidebar'  => __('3 Columns + 1 Sidebar', 'theme_admin'),
                '4col'      => __('4 Columns', 'theme_admin'),
                '4col-1sidebar'  => __('4 Columns + 1 Sidebar', 'theme_admin'),
            ),
            'std'   => '1sidebar'
        ),
    )
);


// Background
$theme_options[] = array(
    'name'   => __('Background', 'theme_admin'),
    'id'     => 'background',
    'fields' => array(
        array(
            'name'      => __('Background type', 'theme_admin'),
            'id'        => YO_PREFIX.'background_type',
            'desc'      => __("Select the type of background used in all the pages. It's possible to customize the background on each page or post edit screen.", 'theme_admin'),
            'type'      => 'radio',
            'toggle'    => '#yo-section-'.YO_PREFIX.'background_color, '.
                           '#yo-section-'.YO_PREFIX.'background_pattern, '.
                           '#yo-section-'.YO_PREFIX.'background_standard | '.
                           '#yo-section-'.YO_PREFIX.'background_slideshow_settings, '.
                           '#yo-section-'.YO_PREFIX.'background_color, '.
                           '#yo-section-'.YO_PREFIX.'background_pattern, '.
                           '#yo-section-'.YO_PREFIX.'background_slideshow | '.
                           '#yo-section-'.YO_PREFIX.'background_video | ',
            'options'   => array(
                'standard'  => __('Standard', 'theme_admin'),
                'slideshow' => __('Images slideshow', 'theme_admin'),
                'video'     => __('Video', 'theme_admin')),
            'std'       => 'standard'
        ),
        $background_color_field,
        $background_pattern_field,
        $background_image_field,
        $background_settings_field,
        $background_slideshow_field,
        $background_video_field,
    )
);


// Colors
$theme_options[] = array(
    'name'   => __('Fonts & Colors', 'theme_admin'),
    'id'     => 'colors',
    'fields' => array(
        //----------------------------------------------------------------------------------------
        // Headlines Font
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Headlines Font',
            'id'        => YO_PREFIX.'font',
            'desc'      => 'Choose a font for the headlines.',
            'type'      => 'google_fonts',
            'preview'   => true,
        ),
        //----------------------------------------------------------------------------------------
        // Body Font
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Body Font',
            'id'        => YO_PREFIX.'body_font',
            'desc'      => 'Choose a font for the body text.',
            'type'      => 'google_fonts',
            'preview'   => true,
        ),
        //----------------------------------------------------------------------------------------
        // Logo & Tagline
        //----------------------------------------------------------------------------------------
        array(
            'name'      => __('Logo & Tagline', 'theme_admin'),
            'id'        => 'branding',
            'type'      => 'group',
            'desc'     => __("In 'Text Shadow' it's recommended 'white' for dark text and 'black' for light text.", 'theme_admin'),
            'fields'    => array(
                array(
                    'name'      => __('Text Logo Size', 'theme_admin'),
                    'id'        => 'logo_size',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Text Logo Color', 'theme_admin'),
                    'id'        => 'logo_color',
                    'type'      => 'color',
                    // 'std'       => '#ffffff'
                ),
                array(
                    'name'      => __('Tagline Size', 'theme_admin'),
                    'id'        => 'tagline_size',
                    'type'      => 'text',
                ),
                array(
                    'name'      => __('Tagline Color', 'theme_admin'),
                    'id'        => 'tagline_color',
                    'type'      => 'color',
                    // 'std'       => '#c8c8c8'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Header Background', 'theme_admin'),
                    'id'       => 'background',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
            )
        ),
        array(
            'name'     => __('Navigation', 'theme_admin'),
            'id'       => 'navigation',
            'desc'     => __("Change the various states of the main navigation menu.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Text Size', 'theme_admin'),
                    'id'        => 'size',
                    'type'      => 'text',
                    'std'       => '14px'
                ),
                array(
                    'name'     => __('Color', 'theme_admin'),
                    'id'       => 'main_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Background', 'theme_admin'),
                    'id'       => 'main_background',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Shadow', 'theme_admin'),
                    'id'       => 'main_shadow',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Hover Color', 'theme_admin'),
                    'id'       => 'main_hover_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Hover Background', 'theme_admin'),
                    'id'       => 'main_hover_background',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Hover Shadow', 'theme_admin'),
                    'id'       => 'main_hover_shadow',
                    'type'     => 'color',
                ),
                /////////////////////////////////////////////////////////////
                array(
                    'name'     => __('Submenu Color', 'theme_admin'),
                    'id'       => 'submenu_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Submenu Background', 'theme_admin'),
                    'id'       => 'submenu_background',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Submenu Shadow', 'theme_admin'),
                    'id'       => 'submenu_shadow',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Current Item Color', 'theme_admin'),
                    'id'       => 'current_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Current Item Background', 'theme_admin'),
                    'id'       => 'current_background',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Current Item Shadow', 'theme_admin'),
                    'id'       => 'current_shadow',
                    'type'     => 'color',
                ),
            )
        ),
        array(
            'name'     => __('Body', 'theme_admin'),
            'id'       => 'body_text',
            'desc'     => __("General font style.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Text Size', 'theme_admin'),
                    'id'        => 'size',
                    'type'      => 'text',
                    'std'       => '14px'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'shadow',
                    // 'desc'     => __("", 'theme_admin'),
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
            )
        ),
        array(
            'name'     => __('Top Headline', 'theme_admin'),
            'id'       => 'top_headline',
            'desc'     => __("The page title above the content.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Text Size', 'theme_admin'),
                    'id'        => 'size',
                    'type'      => 'text',
                    'std'       => '36px'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Background color', 'theme_admin'),
                    'id'       => 'background',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
            )
        ),
        array(
            'name'     => __('Content Headlines', 'theme_admin'),
            'id'       => 'headlines',
            'desc'     => __("The page title inside the content.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Text Size', 'theme_admin'),
                    'id'        => 'size',
                    'type'      => 'text',
                    'std'       => '24px'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Hover Color', 'theme_admin'),
                    'id'       => 'hover',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
            )
        ),
        array(
            'name'     => __('Content Boxes', 'theme_admin'),
            'id'       => 'boxes',
            'desc'     => __("The post/pages content boxes.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Content Background Color', 'theme_admin'),
                    'id'        => 'caption',
                    'type'      => 'color',
                ),
                array(
                    'name'     => __('Meta Background Color', 'theme_admin'),
                    'id'       => 'meta',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Separator', 'theme_admin'),
                    'id'       => 'separator',
                    'type'     => 'color',
                ),
            )
        ),
        array(
            'name'     => __('Links', 'theme_admin'),
            'id'       => 'links',
            'desc'     => __("The links color.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Links Color', 'theme_admin'),
                    'id'       => 'color',
                    'type'     => 'color',
                    // 'std'      => '#8ed8d2'
                ),
                array(
                    'name'     => __('Hover Color', 'theme_admin'),
                    'id'       => 'hover',
                    'type'     => 'color',
                    // 'std'      => '#307b77'
                ),
            )
        ),
        array(
            'name'     => __('Sidebars and Widgets areas', 'theme_admin'),
            'id'       => 'sidebars',
            'desc'     => __("Change colors of sidebars and other widget areas.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'      => __('Headlines Color', 'theme_admin'),
                    'id'        => 'headlines_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Headlines Text Shadow', 'theme_admin'),
                    'id'       => 'headlines_shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'text_color',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'text_shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Links Color', 'theme_admin'),
                    'id'       => 'links_color',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Links Hover Color', 'theme_admin'),
                    'id'       => 'links_hover',
                    'type'     => 'color',
                    // 'std'      => '#eeeeee'
                ),
                array(
                    'name'     => __('Links Text Shadow', 'theme_admin'),
                    'id'       => 'links_shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Background color', 'theme_admin'),
                    'id'       => 'background',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Separator color', 'theme_admin'),
                    'id'       => 'separator',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
            )
        ),
        array(
            'name'     => __('Buttons', 'theme_admin'),
            'id'       => 'buttons',
            'desc'     => __("The buttons color. For the 'Text Shadow' is recommended '#fff' for dark text and '#000' for light text.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Background Color', 'theme_admin'),
                    'id'       => 'bg_color',
                    'type'     => 'color',
                    // 'std'      => '#333333'
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'text_color',
                    'type'     => 'color',
                    // 'std'      => '#ffffff'
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'text_shadow',
                    'type'     => 'color',
                    // 'std'      => '#000000'
                ),
                array(
                    'name'     => __('Hover Background Color', 'theme_admin'),
                    'id'       => 'bg_hover',
                    'type'     => 'color',
                    // 'std'      => '#ff5500'
                ),
            )
        ),
        array(
            'name'     => __('Slider Captions', 'theme_admin'),
            'id'       => 'slider',
            'desc'     => __("The content slider caption colors.", 'theme_admin'),
            'type'     => 'group',
            'fields'   => array(
                array(
                    'name'     => __('Background Color', 'theme_admin'),
                    'id'       => 'bg_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Text Color', 'theme_admin'),
                    'id'       => 'text_color',
                    'type'     => 'color',
                ),
                array(
                    'name'     => __('Text Shadow', 'theme_admin'),
                    'id'       => 'text_shadow',
                    'type'     => 'color',
                ),
            )
        )
    )
);



$theme_options[] = array(
    'name'   => __('Custom Code', 'theme_admin'),
    'id'     => 'custom_code',
    'fields' => array(
        array(
            'heading'  => __('Custom Code', 'theme_admin'),
            'name'     => __('Custom CSS', 'theme_admin'),
            'id'       => 'css',
            'desc'     => __("Write here the CSS code you wish to include in all pages. You don't need to add any style tags.", 'theme_admin'),
            'type'     => 'textarea'
        ),
        array(
            'name'     => __('Custom Javascript', 'theme_admin'),
            'id'       => 'js',
            'desc'     => __("Write here the Javascript code you wish to include in all pages. You don't need to add any script tags.", 'theme_admin'),
            'type'     => 'textarea'
        ),
    )
);


yo_register_options($theme_options);