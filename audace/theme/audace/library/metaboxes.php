<?php

/*
 *------------------------------------------------------------------------------------------------
 * Background fields
 *------------------------------------------------------------------------------------------------
 *
 */

//----------------------------------------------------------------------------------------
// Background Pattern
//----------------------------------------------------------------------------------------
$background_pattern_field = array(
    'name'  => __('Pattern overlay', 'theme_admin'),
    'id'    => YO_PREFIX.'background_pattern',
    'desc'  => __('Select a background overlay pattern, also useful to mask low quality images. ', 'theme_admin'),
    'type'  => 'select',
    'options' => array(
        '' => __('Disabled', 'theme_admin'),
        '01' => __('Spaced dots', 'theme_admin'),
        '02' => __('Tight dots', 'theme_admin'),
        '06' => __('Very tight dots', 'theme_admin'),
        '03' => __('Vertical lines', 'theme_admin'),
        '04' => __('Horizontal lines', 'theme_admin'),
        '05' => __('Diagonal lines', 'theme_admin'),
        '08' => __('Crosses', 'theme_admin'),
        '12' => __('Tight crosses', 'theme_admin'),
        '13' => __('Round crosses', 'theme_admin'),
        '11' => __('Grid', 'theme_admin'),
        '09' => __('Squares', 'theme_admin'),
        '10' => __('Squares grid', 'theme_admin'),
        '07' => __('Diagonal grid', 'theme_admin'),
        '14' => __('Loose grid', 'theme_admin'),
        '15' => __('Millimetric grid', 'theme_admin'),
    ),
    'std' => ''
);

//----------------------------------------------------------------------------------------
// Background Color
//----------------------------------------------------------------------------------------
$background_color_field = array(
    'name'  => __('Background color', 'theme_admin'),
    'id'    => YO_PREFIX.'background_color',
    'type'  => 'color'
);

//----------------------------------------------------------------------------------------
// Background Image
//----------------------------------------------------------------------------------------
$background_image_field = array(
    'name'      => __('Background color and image', 'theme_admin'),
    'id'        => YO_PREFIX.'background_standard',
    'desc'      => __('Set a basic background color and an optional image with some parameters.', 'theme_admin'),
    'type'      => 'group',
    'class'     => 'yo-hide',
    'fields'    => array(
        array(
            'name'  => __('Image', 'theme_admin'),
            'id'    => 'image',
            'type'  => 'media',
        ),
        array(
            'name'  => __('Image position', 'theme_admin'),
            'id'    => 'position',
            'type'  => 'select',
            'options' => array(
                'top-left'      => __('Top Left', 'theme_admin'),
                'top-center'    => __('Top Center', 'theme_admin'),
                'top-right'     => __('Top Right', 'theme_admin'),
                'left'          => __('Left', 'theme_admin'),
                'center'        => __('Center', 'theme_admin'),
                'right'         => __('Right', 'theme_admin'),
                'bottom-left'   => __('Bottom Left', 'theme_admin'),
                'bottom-center' => __('Bottom Center', 'theme_admin'),
                'bottom-right'  => __('Bottom Right', 'theme_admin'),
            ),
            'std'   => 'center'
        ),
        array(
            'name'  => __('Image repeat', 'theme_admin'),
            'id'    => 'repeat',
            'type'  => 'select',
            'options' => array(
                'no-repeat' => "Don't repeat",
                'repeat'   => 'Repeat',
                'repeat-x' => 'Repeat horizontally',
                'repeat-v' => 'Repeat vertically',
            )
        ),
    )
);

//----------------------------------------------------------------------------------------
// Slideshow Settings
//----------------------------------------------------------------------------------------
$background_settings_field = array(
    'name'   => __('Slideshow settings', 'theme_admin'),
    'id'     => YO_PREFIX.'background_slideshow_settings',
    'desc'   => __('Configure how the slideshow will behave', 'theme_admin'),
    'type'   => 'group',
    'class'  => 'yo-hide',
    'fields' => array(
        array(
            'name'  => __('Full page', 'theme_admin'),
            'id'    => 'full_page',
            'type'  => 'checkbox',
            'std'   => 1
        ),
        array(
            'name'  => __('Autostart', 'theme_admin'),
            'id'    => 'autostart',
            'type'  => 'checkbox',
        ),
        array(
            'name'  => __('Show controls', 'theme_admin'),
            'id'    => 'controls',
            'type'  => 'checkbox'
        ),
        // array(
        //     'name'  => __('Transition type', 'theme_admin'),
        //     'id'    => 'transition',
        //     'type'  => 'select',
        //     'options' => array(
        //         'fade'   => __('Fade', 'theme_admin'),
        //         'slide'  => __('Slide', 'theme_admin'),
        //     ),
        //     'std'   => 'fade'
        // ),
        array(
            'name'  => __('Transition duration', 'theme_admin'),
            'id'    => 'transition_duration',
            'type'  => 'select',
            'options' => array(
                '200'   => __('0.2s', 'theme_admin'),
                '300'   => __('0.3s', 'theme_admin'),
                '400'   => __('0.4s', 'theme_admin'),
                '500'   => __('0.5s', 'theme_admin'),
                '600'   => __('0.6s', 'theme_admin'),
                '700'   => __('0.7s', 'theme_admin'),
                '800'   => __('0.8s', 'theme_admin'),
                '900'   => __('0.9s', 'theme_admin'),
                '1000'  => __('1s', 'theme_admin'),
                '2000'  => __('2s', 'theme_admin'),
                '3000'  => __('3s', 'theme_admin'),
                '4000'  => __('4s', 'theme_admin'),
                '5000'  => __('5s', 'theme_admin'),
            ),
            'std'   => '700'
        ),
        array(
            'name'  => __('Slideshow speed', 'theme_admin'),
            'id'    => 'delay',
            'type'  => 'select',
            'options' => array(
                '1000'   => __('1s', 'theme_admin'),
                '2000'   => __('2s', 'theme_admin'),
                '3000'   => __('3s', 'theme_admin'),
                '4000'   => __('4s', 'theme_admin'),
                '5000'   => __('5s', 'theme_admin'),
                '6000'   => __('6s', 'theme_admin'),
                '7000'   => __('7s', 'theme_admin'),
                '8000'   => __('8s', 'theme_admin'),
                '9000'   => __('9s', 'theme_admin'),
                '10000'  => __('10s', 'theme_admin'),
                '12000'  => __('12s', 'theme_admin'),
                '14000'  => __('14s', 'theme_admin'),
                '16000'  => __('16s', 'theme_admin'),
            ),
            'std' => '7000'
        ),
    )
);

//----------------------------------------------------------------------------------------
// Background Slideshow
//----------------------------------------------------------------------------------------
$background_slideshow_field = array(
    'name'      => __('Slideshow images', 'theme_admin'),
    'id'        => YO_PREFIX.'background_slideshow',
    'desc'      => __('Drag&drop the images to use in the background slideshow.', 'theme_admin'),
    'type'      => 'group',
    'clone'     => true,
    'clone_button' => false,
    'sortable'  => true,
    'empty'     => true,
    'plupload'  => true,
    // 'extensions' => 'jpg,jpeg,gif,png',
    'class'     => 'yo-custom-thumbs yo-plupload yo-hide',
    'fields'    => array(
        // array(
        //     'name'  => __('Link', 'theme_admin'),
        //     'id'    => 'link',
        //     'type'  => 'text',
        //     'class' => 'yo-tooltip-item',
        // ),
        array(
            'id'        => 'media',
            'type'      => 'image',
            'bar'       => true,
            'edit'      => false,
            'delete'    => true,
            'class'     => 'yo-receiver',
            'receiver'  => true
        )
    )
);


//----------------------------------------------------------------------------------------
// Background Video
//----------------------------------------------------------------------------------------
$background_video_field = array(
    'name'      => __('Background video', 'theme_admin'),
    'id'        => YO_PREFIX.'background_video',
    'desc'      => __("Choose a video to reproduce in the background. Please note this doesn't work on smartphones and tablets, and for hosted videos it's necessary to provide both the MP4 and OGV formats.", 'theme_admin'),
    'type'      => 'group',
    // 'clone'     => true,
    // 'clone_button' => false,
    // 'sortable'  => true,
    // 'empty'     => true,
    // 'plupload'  => true,
    // 'extensions' => 'jpg,jpeg,gif,png',
    // 'class'     => 'yo-custom-thumbs yo-plupload yo-hide',
    'fields'    => array(
        array(
            'name'      => __('Video type', 'theme_admin'),
            'id'        => 'video_type',
            'type'      => 'radio',
            'toggle'    => '#yo-section-yo_background_video .yo-text | '.
                           '#yo-section-yo_background_video .yo-media, #yo-section-yo_background_video .yo-checkbox',
            'options'   => array(
                'embed'   => __('Embed', 'theme_admin'),
                'hosted'  => __('Self hosted', 'theme_admin')),
            'std'       => 'embed'
        ),
        // array(
        //     'name'  => __('Video embed', 'theme_admin'),
        //     'id'    => 'video_embed',
        //     'type'  => 'textarea',
        // ),
        array(
            'name'  => __('Youtube/Vimeo URL', 'theme_admin'),
            'id'    => 'video_url',
            // 'desc'  => __("Paste only the ID of the Youtube video to use in the background. Please note this doesn't work on smartphones and tablets.", 'theme_admin'),
            'type'  => 'text',
        ),
        array(
            'name'  => __('Show controls', 'theme_admin'),
            'id'    => 'video_controls',
            'type'  => 'checkbox',
            'std'   => false
        ),
        array(
            'name'  => __('Play loop', 'theme_admin'),
            'id'    => 'video_loop',
            'type'  => 'checkbox',
            'std'   => true
        ),
        array(
            'name'  => __('Mute sound', 'theme_admin'),
            'id'    => 'video_mute',
            'type'  => 'checkbox',
            'std'   => false
        ),
        array(
            'name'  => __('Video MP4 file', 'theme_admin'),
            'id'    => 'video_mp4',
            'type'  => 'media',
        ),
        array(
            'name'  => __('Video OGV file', 'theme_admin'),
            'id'    => 'video_ogv',
            'type'  => 'media',
        ),
        // array(
        //     'name'  => __('Video WEBMV file', 'theme_admin'),
        //     'id'    => 'video_webmv',
        //     'type'  => 'media',
        // ),
    )
);


$background_fields = array(
	//----------------------------------------------------------------------------------------
    // Background Type
    //----------------------------------------------------------------------------------------
    array(
        'name'      => __('Background type', 'theme_admin'),
        'id'        => YO_PREFIX.'background_type',
        'desc'      => __('Select the type of background for the current page. Choose "Default" to use the global background settings.', 'theme_admin'),
        'type'      => 'radio',
        // 'toggle'    => '.'.YO_PREFIX.'background_image | '.
        //                '.'.YO_PREFIX.'background_video | ',
        'toggle'    => '#default-background | '.
                       '#yo-section-'.YO_PREFIX.'background_color, '.
                       '#yo-section-'.YO_PREFIX.'background_pattern, '.
                       '#yo-section-'.YO_PREFIX.'background_standard | '.
                       '#yo-section-'.YO_PREFIX.'background_slideshow_settings, '.
                       '#yo-section-'.YO_PREFIX.'background_color, '.
                       '#yo-section-'.YO_PREFIX.'background_pattern, '.
                       '#yo-section-'.YO_PREFIX.'background_slideshow | '.
                       '#yo-section-'.YO_PREFIX.'background_video, '.
                       '#yo-section-'.YO_PREFIX.'background_color | ',
        'options'   => array(
            'default'   => __('Default', 'theme_admin'),
            'standard'  => __('Standard', 'theme_admin'),
            'slideshow' => __('Images slideshow', 'theme_admin'),
            'video'     => __('Video', 'theme_admin')),
        'std'       => 'default'
    ),
    $background_color_field,
    $background_pattern_field,
    $background_image_field,
    $background_settings_field,
    $background_slideshow_field,
    $background_video_field
);



/*
 *------------------------------------------------------------------------------------------------
 * Audio - Post Format
 *------------------------------------------------------------------------------------------------
 *
 */
$audio_format_fields = array(
    array(
        'name'      => __('Audio type', 'theme_admin'),
        'id'        => YO_PREFIX.'audio_type',
        'desc'      => __('Select the type of background for the current page. Choose "Default" to use the global background settings.', 'theme_admin'),
        'type'      => 'radio',
        // 'toggle'    => '.'.YO_PREFIX.'background_image | '.
        //                '.'.YO_PREFIX.'background_video | ',
        'toggle'    => '#yo-section-'.YO_PREFIX.'audio_embed | '.
                       '#yo-section-'.YO_PREFIX.'audio_file ',
        'options'   => array(
            'embed'   => __('Embed', 'theme_admin'),
            'hosted'  => __('Self hosted', 'theme_admin')),
        'std'       => 'embed'
    ),
    // array(
    //     'name'  => __('Audio cover image', 'theme_admin'),
    //     'id'    => YO_PREFIX.'audio_cover',
    //     'type'  => 'media'
    // ),
    array(
        'name'  => __('Audio embed', 'theme_admin'),
        'id'    => YO_PREFIX.'audio_embed',
        'type'  => 'textarea'
    ),
    array(
        'name'  => __('Audio mp3/m4a file', 'theme_admin'),
        'id'    => YO_PREFIX.'audio_file',
        'type'  => 'media'
    ),
);

/*
 *------------------------------------------------------------------------------------------------
 * Gallery - Post Format
 *------------------------------------------------------------------------------------------------
 *
 */
$gallery_format_fields = array(
    array(
        'name'   => __('Gallery settings', 'theme_admin'),
        'id'     => YO_PREFIX.'gallery_settings',
        'desc'   => __('Configure how the gallery slider will behave', 'theme_admin'),
        'type'   => 'group',
        'fields' => array(
            array(
                'name'  => __('Run the slideshow on load', 'theme_admin'),
                'id'    => 'slideshow',
                'type'  => 'checkbox'
            ),
            array(
                'name'  => __('Transition type', 'theme_admin'),
                'id'    => 'transition',
                'type'  => 'select',
                'options' => array(
                    'fade'   => __('Fade', 'theme_admin'),
                    'slide'  => __('Slide', 'theme_admin'),
                )
            ),
            array(
                'name'  => __('Transition duration', 'theme_admin'),
                'id'    => 'duration',
                'type'  => 'select',
                'options' => array(
                    '200'   => __('0.2s', 'theme_admin'),
                    '300'   => __('0.3s', 'theme_admin'),
                    '400'   => __('0.4s', 'theme_admin'),
                    '500'   => __('0.5s', 'theme_admin'),
                    '600'   => __('0.6s', 'theme_admin'),
                    '700'   => __('0.7s', 'theme_admin'),
                    '800'   => __('0.8s', 'theme_admin'),
                    '900'   => __('0.9s', 'theme_admin'),
                    '1000'  => __('1s', 'theme_admin'),
                    '2000'  => __('2s', 'theme_admin'),
                    '3000'  => __('3s', 'theme_admin'),
                ),
            ),
            array(
                'name'  => __('Slideshow speed', 'theme_admin'),
                'id'    => 'delay',
                'type'  => 'select',
                'options' => array(
                    '1000'   => __('1s', 'theme_admin'),
                    '2000'   => __('2s', 'theme_admin'),
                    '3000'   => __('3s', 'theme_admin'),
                    '4000'   => __('4s', 'theme_admin'),
                    '5000'   => __('5s', 'theme_admin'),
                    '6000'   => __('6s', 'theme_admin'),
                    '7000'   => __('7s', 'theme_admin'),
                    '8000'   => __('8s', 'theme_admin'),
                    '9000'   => __('9s', 'theme_admin'),
                    '10000'  => __('10s', 'theme_admin'),
                    '12000'  => __('12s', 'theme_admin'),
                    '14000'  => __('14s', 'theme_admin'),
                    '16000'  => __('16s', 'theme_admin'),
                ),
                'std' => '7'
            )
        )
    ),
    array(
        'name'      => __('Images', 'theme_admin'),
        'id'        => YO_PREFIX.'gallery_images',
        'desc'      => __('Drag&drop the images that will used in the gallery slider. Each image has an optional Title, Description and Link.', 'theme_admin'),
        'type'      => 'group',
        'clone'     => true,
        'clone_button' => false,
        'sortable'  => true,
        'empty'     => true,
        'plupload'  => true,
        'extensions' => 'jpg,jpeg,gif,png',
        'class'     => 'yo-custom-thumbs yo-plupload',
        'fields'    => array(
            array(
                'name'  => __('Title', 'theme_admin'),
                'id'    => 'title',
                'type'  => 'text',
                'class' => 'yo-tooltip-item',
            ),
            array(
                'name'  => __('Description', 'theme_admin'),
                'id'    => 'description',
                'type'  => 'text',
                'class' => 'yo-tooltip-item',
            ),
            array(
                'name'  => __('Link', 'theme_admin'),
                'id'    => 'link',
                'type'  => 'text',
                'class' => 'yo-tooltip-item',
            ),
            array(
                'name'      => __('Link target', 'theme_admin'),
                'id'        => 'target',
                'type'      => 'select',
                'std'       => '_self',
                'class'     => 'yo-tooltip-item yo-oneline',
                'options'   => array(
                    '_self'        => __('_self', 'theme_admin'),
                    '_blank'       => __('_blank', 'theme_admin'),
                    '_top'         => __('_top', 'theme_admin'),
                ),
            ),
            array(
                'name'  => __('Caption style', 'theme_admin'),
                'id'    => 'caption',
                'type'  => 'select',
                'class' => 'yo-tooltip-item yo-oneline',
                'options' => array(
                    'top-left-caption ' => __('Top left caption ', 'theme_admin'),
                    'top-right-caption ' => __('Top right caption ', 'theme_admin'),
                    'bottom-left-caption ' => __('Bottom left caption ', 'theme_admin'),
                    'full-top-caption ' => __('Full top caption ', 'theme_admin'),
                    'full-bottom-caption ' => __('Full bottom caption ', 'theme_admin'),
                )
            ),
            array(
                'id'        => 'media',
                'type'      => 'image',
                'bar'       => true,
                'class'     => 'yo-receiver',
                'receiver'  => true
            )
        )
    ),
);

/*
 *------------------------------------------------------------------------------------------------
 * Link - Post Format
 *------------------------------------------------------------------------------------------------
 *
 */
$link_format_fields = array(
    array(
        'name'      => __('Link URL', 'theme_admin'),
        'id'        => YO_PREFIX.'link_url',
        'type'      => 'text'
    ),
    array(
        'name'  => __('Link target', 'theme_admin'),
        'id'    => YO_PREFIX.'link_target',
        'type'  => 'select',
        'options' => array(
            '_blank' => __('New window', 'theme_admin'),
            '_self' => __('Same window', 'theme_admin'),
        )
    ),
);

/*
 *------------------------------------------------------------------------------------------------
 * Quote - Post Format
 *------------------------------------------------------------------------------------------------
 *
 */
$quote_format_fields = array(
    array(
        'name'      => __('Quote author', 'theme_admin'),
        'id'        => YO_PREFIX.'quote_author',
        'type'      => 'text'
    ),
    array(
        'name'      => __('Quote source URL', 'theme_admin'),
        'id'        => YO_PREFIX.'quote_source',
        'type'      => 'text'
    ),
);

/*
 *------------------------------------------------------------------------------------------------
 * Video - Post Format
 *------------------------------------------------------------------------------------------------
 *
 */
$video_format_fields = array(
    array(
        'name'      => __('Video type', 'theme_admin'),
        'id'        => YO_PREFIX.'video_type',
        'desc'      => __("For embedded videos just copy the code provided by the websites 'share' feature. For self hosted videos it's recommended to upload them in at least two formats (for cross-browser compatibility), with the MP4 being the most important.", 'theme_admin'),
        'type'      => 'radio',
        // 'toggle'    => '.'.YO_PREFIX.'background_image | '.
        //                '.'.YO_PREFIX.'background_video | ',
        'toggle'    => '#yo-section-'.YO_PREFIX.'video_embed | '.
                       '#yo-section-'.YO_PREFIX.'video_m4v, ' . 
                       '#yo-section-'.YO_PREFIX.'video_ogv, ' . 
                       '#yo-section-'.YO_PREFIX.'video_webmv ',
        'options'   => array(
            'embed'   => __('Embed', 'theme_admin'),
            'hosted'  => __('Self hosted', 'theme_admin')),
        'std'       => 'embed'
    ),
    array(
        'name'  => __('Use featured image', 'theme_admin'),
        'desc'  => __('If checked, the featured image will be shown instead of the video in the archives.', 'theme_admin'),
        'id'    => YO_PREFIX.'video_use_image',
        'type'  => 'checkbox'
    ),
    array(
        'name'  => __('Video embed', 'theme_admin'),
        'desc'  => __('Place here the video embed code, usually found in the video page.', 'theme_admin'),
        'id'    => YO_PREFIX.'video_embed',
        'type'  => 'textarea'
    ),
    array(
        'name'  => __('Video M4V file', 'theme_admin'),
        'id'    => YO_PREFIX.'video_m4v',
        'type'  => 'media'
    ),
    array(
        'name'  => __('Video OGV file', 'theme_admin'),
        'id'    => YO_PREFIX.'video_ogv',
        'type'  => 'media'
    ),
    array(
        'name'  => __('Video WEBMV file', 'theme_admin'),
        'id'    => YO_PREFIX.'video_webmv',
        'type'  => 'media'
    ),
);


/*
 *------------------------------------------------------------------------------------------------
 * Blog template options
 *------------------------------------------------------------------------------------------------
 *
 */

$blog_template_fields = array(
    array(
        'name'  => __('Page layout', 'theme_admin'),
        'id'    => YO_PREFIX.'blog_layout',
        'type'  => 'select',
        'options' => array(
            'fullwidth'      => __('Fullwidth', 'theme_admin'),
            // 'fluid'          => __('Fluid', 'theme_admin'),
            // 'accordion'      => __('Accordion', 'theme_admin'),
            'slides'         => __('Slides', 'theme_admin'),
            '1sidebar'       => __('1 Column + 1 Sidebar', 'theme_admin'),
            '2sidebars'      => __('1 Column + 2 Sidebars', 'theme_admin'),
            '2col'           => __('2 Columns', 'theme_admin'),
            '2col-1sidebar'  => __('2 Columns + 1 Sidebar', 'theme_admin'),
            '2col-2sidebars' => __('2 Columns + 2 Sidebars', 'theme_admin'),
            '3col'           => __('3 Columns', 'theme_admin'),
            '3col-1sidebar'  => __('3 Columns + 1 Sidebar', 'theme_admin'),
            '4col'           => __('4 Columns', 'theme_admin'),
            '4col-1sidebar'  => __('4 Columns + 1 Sidebar', 'theme_admin'),
        ),
        // 'class'     => 'yo-chosen',
        // 'mod'       => 'chosen'
    ),
    array(
        'name'  => __('Posts per page', 'theme_admin'),
        'id'    => YO_PREFIX.'blog_posts_per_page',
        'type'  => 'text',
        'std'   => '10'
    ),
    array(
        'name'  => __('Infinite scroll', 'theme_admin'),
        'id'    => YO_PREFIX.'infinite_scroll',
        'type'  => 'checkbox',
        'std'   => false
    ),
    array(
        'name'  => __('Show meta information (author etc.)', 'theme_admin'),
        'id'    => YO_PREFIX.'show_meta',
        'type'  => 'checkbox',
        'std'   => true
    ),
    array(
        'name'     => __('Blog Categories', 'theme_admin'),
        'id'       => YO_PREFIX.'blog_categories',
        'desc'     => __('Pick the post categories that will be displayed in the page.', 'theme_admin'),
        'type'     => 'select',
        'multiple' => true,
        'options'  => 'categories',
        'class'    => 'yo-chosen yo-oneline',
        'mod'      => 'chosen'
    ),
    array(
        'name'     => __('Excluded Categories', 'theme_admin'),
        'id'       => YO_PREFIX.'blog_excluded_categories',
        'desc'     => __('Pick the post categories that will NOT be displayed in the page.', 'theme_admin'),
        'type'     => 'select',
        'multiple' => true,
        'options'  => 'categories',
        'class'    => 'yo-chosen yo-oneline',
        'mod'      => 'chosen'
    ),
);

/*
 *------------------------------------------------------------------------------------------------
 * Custom template options
 *------------------------------------------------------------------------------------------------
 *
 */
$custom_template_fields = array(
    array(
        'name'  => __('Page margin', 'theme_admin'),
        'id'    => YO_PREFIX.'custom_layout',
        'desc'  => __('Use this to balance the vertical position of the page content. It supports px, %, em values. Default is 13%.', 'theme_admin'),
        'type'  => 'text',
        'std'   => '13%',
        'class' => 'yo-oneline'
    ),
);


/*
 *------------------------------------------------------------------------------------------------
 * Archive Options
 *------------------------------------------------------------------------------------------------
 *
 */
$archive_options_fields = array(
    array(
        'name'  => __('Meta', 'theme_admin'),
        'desc'  => __('Show title, date and excerpt. Useful especially for post types like image and video to show only the media.', 'theme_admin'),
        'id'    => YO_PREFIX.'show_caption',
        'type'  => 'checkbox',
        'std'   => true,
        // 'class' => 'yo-oneline'
    ),
    array(
        'name'  => __('Additional meta', 'theme_admin'),
        'desc'  => __('Show comments, author and categories information.', 'theme_admin'),
        'id'    => YO_PREFIX.'show_additional_meta',
        'type'  => 'checkbox',
        'std'   => true,
        // 'class' => 'yo-oneline'
    ),
    array(
        'name'  => __('Lightbox', 'theme_admin'),
        'desc'  => __('If checked, the post thumbnail will zoom the featured image when clicked.', 'theme_admin'),
        'id'    => YO_PREFIX.'lightbox',
        'type'  => 'checkbox',
        'std'   => false,
        // 'class' => 'yo-oneline'
    ),    
    array(
        'name'  => __('Overlay content', 'theme_admin'),
        'desc'  => __('Choose what to display in the overlay: the title, title + excerpt or an icon.', 'theme_admin'),
        'id'    => YO_PREFIX.'overlay_icon',
        'type'  => 'select',
        'options' => array(
            ''              => __('Nothing', 'theme_admin'),
            '  '            => '---',
            'title'         => __('Title', 'theme_admin'),
            'title-excerpt' => __('Title + Excerpt', 'theme_admin'),
            ' '             => '---',
            'carousel'      => __('Carousel', 'theme_admin'),
            'cart'          => __('Cart', 'theme_admin'),
            'catalogue'     => __('Catalogue', 'theme_admin'),
            'close'         => __('Close', 'theme_admin'),
            'code'          => __('Code', 'theme_admin'),
            'coffee'        => __('Coffee', 'theme_admin'),
            'comments'      => __('Comments', 'theme_admin'),
            'download'      => __('Download', 'theme_admin'),
            'pencil'        => __('Pencil', 'theme_admin'),
            'email'         => __('Email', 'theme_admin'),
            'facebook'      => __('Facebook', 'theme_admin'),
            'faq'           => __('Faq', 'theme_admin'),
            'flickr'        => __('Flickr', 'theme_admin'),
            'gallery'       => __('Gallery', 'theme_admin'),
            'googleplus'    => __('Googleplus', 'theme_admin'),
            'home'          => __('Home', 'theme_admin'),
            'image'         => __('Image', 'theme_admin'),
            'infoi'          => __('Info', 'theme_admin'),
            'key'           => __('Key', 'theme_admin'),
            'link'          => __('Link', 'theme_admin'),
            'lock'          => __('Lock', 'theme_admin'),
            'loudspeaker'   => __('Loudspeaker', 'theme_admin'),
            'loupe'         => __('Loupe', 'theme_admin'),
            'map'           => __('Map', 'theme_admin'),
            'move'          => __('Move', 'theme_admin'),
            'pie_graph'     => __('Pie Graph', 'theme_admin'),
            'poll'          => __('Poll', 'theme_admin'),
            'refresh'       => __('Refresh', 'theme_admin'),
            'rss'           => __('RSS', 'theme_admin'),
            'settings'      => __('Settings', 'theme_admin'),
            'structure'     => __('Structure', 'theme_admin'),
            'text'          => __('Text', 'theme_admin'),
            'themes'        => __('Themes', 'theme_admin'),
            'thumbnails'    => __('Thumbnails', 'theme_admin'),
            'thumbnails2'   => __('Thumbnails 2', 'theme_admin'),
            'trash'         => __('Trash', 'theme_admin'),
            'twitter'       => __('Twitter', 'theme_admin'),
            'unlock'        => __('Unlock', 'theme_admin'),
            'upload'        => __('Upload', 'theme_admin'),
            'users'         => __('Users', 'theme_admin'),
            'video'         => __('Video', 'theme_admin'),
            'weather'       => __('Weather', 'theme_admin'),
        ),
        'std'   => ''
    ),
    array(
        'name'  => __('Overlay background color', 'theme_admin'),
        'desc'  => __('If set, displays a color overlay on the thumbnail.', 'theme_admin'),
        'id'    => YO_PREFIX.'color_overlay',
        'type'  => 'color',
    ),
    array(
        'name'  => __('Overlay text color', 'theme_admin'),
        'desc'  => __('Change the color of the overlay text, only works if title and/or excerpt are selected.', 'theme_admin'),
        'id'    => YO_PREFIX.'text_color_overlay',
        'type'  => 'color',
    ),
    array(
        'name'  => __('Hide overlay on hover', 'theme_admin'),
        'desc'  => __("If checked, the overlay is always visible and it disappears on hover.", 'theme_admin'),
        'id'    => YO_PREFIX.'overlay_hover',
        'type'  => 'checkbox',
        'std'   => false,
        // 'class' => 'yo-oneline'
    ),
);


/*
 *------------------------------------------------------------------------------------------------
 * Post layout
 *------------------------------------------------------------------------------------------------
 *
 */
$post_layout_fields = array(
    array(
        'name'  => __('Post layout', 'theme_admin'),
        'id'    => YO_PREFIX.'post_layout',
        'type'  => 'select',
        'options' => array(
            'fullwidth' => __('Fullwidth', 'theme_admin'),
            '1sidebar'  => __('1 Sidebar', 'theme_admin'),
            '2sidebars' => __('2 Sidebars', 'theme_admin')
        ),
        'std'   => '1sidebar'
    )
);


/*
 *------------------------------------------------------------------------------------------------
 * Widget areas
 *------------------------------------------------------------------------------------------------
 *
 */
$widget_areas_fields = array(
    array(
        'name'  => __('Before header', 'theme_admin'),
        'id'    => YO_PREFIX.'before_header',
        'type'  => 'checkbox',
        'std'   => true
    ),
    array(
        'name'  => __('Before loop', 'theme_admin'),
        'id'    => YO_PREFIX.'before_loop',
        'type'  => 'checkbox',
        'std'   => true
    ),
    array(
        'name'  => __('After loop', 'theme_admin'),
        'id'    => YO_PREFIX.'after_loop',
        'type'  => 'checkbox',
        'std'   => true
    ),
    array(
        'name'  => __('Footer', 'theme_admin'),
        'id'    => YO_PREFIX.'footer',
        'type'  => 'checkbox',
        'std'   => true
    )
);


/*
 *------------------------------------------------------------------------------------------------
 * Show Title
 *------------------------------------------------------------------------------------------------
 *
 */
$show_title_fields = array(
    array(
        'name'  => __('Show title above the content box', 'theme_admin'),
        'id'    => YO_PREFIX.'show_title_above',
        'type'  => 'checkbox',
        'std'   => true
    ),
    array(
        'name'  => __('Show title inside the content box', 'theme_admin'),
        'id'    => YO_PREFIX.'show_title_inside',
        'type'  => 'checkbox',
        'std'   => true
    )
);

//////////////////////////////////////////////////////////////////////////////////////////////


/*
 *------------------------------------------------------------------------------------------------
 * Metaboxes
 *------------------------------------------------------------------------------------------------
 *
 */

$metaboxes = array();

// Background
$metaboxes[] = array(
    'id' => 'page-background',
    'title' => __('Background', 'theme_admin'),
    'pages' => array('post', 'page', 'portfolio'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'low', // 'high', 'core', 'default' or 'low'
    'fields' => $background_fields
);

// Audio - Post Format
$metaboxes[] = array(
    'id' => 'yo-post-format-audio',
    'title' => __('Audio Details', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $audio_format_fields
);

// Gallery - Post Format
$metaboxes[] = array(
    'id' => 'yo-post-format-gallery',
    'title' => __('Gallery Details', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $gallery_format_fields
);

// Link - Post Format
$metaboxes[] = array(
    'id' => 'yo-post-format-link',
    'title' => __('Link Details', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $link_format_fields
);

// Quote - Post Format
$metaboxes[] = array(
    'id' => 'yo-post-format-quote',
    'title' => __('Quote Details', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $quote_format_fields
);

// Video - Post Format
$metaboxes[] = array(
    'id' => 'yo-post-format-video',
    'title' => __('Video Details', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $video_format_fields
);

// Blog template layout
$metaboxes[] = array(
    'id' => 'yo-blog-template',
    'title' => __('Blog Page Options', 'theme_admin'),
    'pages' => array('page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'side', // 'normal', 'advanced', or 'side'
    'priority' => 'default', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $blog_template_fields
);

// Custom template layout
$metaboxes[] = array(
    'id' => 'yo-custom-template',
    'title' => __('Custom Page Options', 'theme_admin'),
    'pages' => array('page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'side', // 'normal', 'advanced', or 'side'
    'priority' => 'default', // 'high', 'core', 'default' or 'low'
    'class' => 'yo-hide',
    'fields' => $custom_template_fields
);

// Post layout
$metaboxes[] = array(
    'id' => 'yo-post-layout',
    'title' => __('Page Layout', 'theme_admin'),
    'pages' => array('post', 'page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'side', // 'normal', 'advanced', or 'side'
    'priority' => 'default', // 'high', 'core', 'default' or 'low'
    'fields' => $post_layout_fields
);

// Archive options
$metaboxes[] = array(
    'id' => 'yo-show-meta',
    'title' => __('Archive Options', 'theme_admin'),
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'low', // 'high', 'core', 'default' or 'low'
    'fields' => $archive_options_fields
);

unset($archive_options_fields[2]);
unset($archive_options_fields[3]);
unset($archive_options_fields[4]);
unset($archive_options_fields[5]);
unset($archive_options_fields[6]);

$metaboxes[] = array(
    'id' => 'yo-show-meta',
    'title' => __('Archive Options', 'theme_admin'),
    'pages' => array('page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'low', // 'high', 'core', 'default' or 'low'
    'fields' => $archive_options_fields
);


// Widget areas
$metaboxes[] = array(
    'id' => 'yo-widget-areas',
    'title' => __('Widget areas', 'theme_admin'),
    'pages' => array('post', 'page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'side', // 'normal', 'advanced', or 'side'
    'priority' => 'default', // 'high', 'core', 'default' or 'low'
    'fields' => $widget_areas_fields
);

// Show title
$metaboxes[] = array(
    'id' => 'yo-show-title',
    'title' => __('Show title', 'theme_admin'),
    'pages' => array('post', 'page'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'side', // 'normal', 'advanced', or 'side'
    'priority' => 'default', // 'high', 'core', 'default' or 'low'
    'fields' => $show_title_fields
);


// Test
// $metaboxes[] = array(
//     'id' => 'yo-test-metabox',
//     'title' => __('Group Test', 'theme_admin'),
//     'pages' => array('post', 'page'), // 'post', 'page', 'link', or 'custom_post_type'
//     'context' => 'side', // 'normal', 'advanced', or 'side'
//     'priority' => 'default', // 'high', 'core', 'default' or 'low'
//     'fields' => array(
//         array(
//             'id' => 'group_test',
//             'name' => 'Group Test',
//             'type' => 'group',
//             'clone' => true,
//             'sortable' => true,
//             // 'empty' => true,
//             'fields' => array(
//                 array(
//                     'id' => 'field1',
//                     'name' => 'Field 1',
//                     'type' => 'text'
//                 ),
//                 array(
//                     'id' => 'field2',
//                     'name' => 'Field 2',
//                     'type' => 'text'
//                 )
//             )
//         )
//     )
// );


yo_register_metabox($metaboxes);

