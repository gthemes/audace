<?php

/*
 *------------------------------------------------------------------------------------------------
 * Contact Form
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_ajax_contact()
{
    if( ! empty($_POST))
    {
        $name = stripslashes($_POST['name']);
        $email = trim($_POST['email']);
        $subject = stripslashes($_POST['subject']);
        $message = stripslashes($_POST['message']);

        $response = array();
        $errors = array();

        // Check name
        if(!$name)
            $errors['name'] = __('Please enter your name.', 'theme_admin');

        // Check email
        if(!$email)
            $errors['email'] = __('Please enter an e-mail address.', 'theme_admin');

        if($email && !is_email($email))
            $errors['email'] = __('Please enter a valid e-mail address.', 'theme_admin');

        // Check message
        if(!$message)
            $errors['message'] = __('Please leave a message.', 'theme_admin');


        if(empty($errors))
        {
            $contact_email = get_field('contact_email', 'theme_admin');
            if ( ! $contact_email) $contact_email = get_bloginfo('admin_email');

            $mail = mail($contact_email, $subject, $message,
             "From: ".$name." <".$email.">\r\n"
            ."Reply-To: ".$email."\r\n"
            ."X-Mailer: PHP/" . phpversion());

            if($mail)
                $response['success'] = true;
        }
        else
        {
            $response['success'] = false;
            $response['errors'] = $errors;
        }
    }

    // Returns the array in JSON format
    die(array2json($response));
}

add_action('wp_ajax_contact_form', 'yo_ajax_contact');
add_action('wp_ajax_nopriv_contact_form', 'yo_ajax_contact');


/////////////////////////////////////////////////////////////////////////////


function yo_media($id, $type = 'audio')
{ ?>
    <div id="post-<?php echo $id; ?>-media" class="media-container"></div>
    <div class="jp-<?php echo $type; ?>">
        <div class="jp-type-single">
            <div id="post-<?php echo $id; ?>-media-ui" class="jp-interface">
                <table class="jp-controls">
                    <tr>
                        <td class="jp-play"><a href="#">play</a></td>
                        <td class="jp-pause"><a href="#">pause</a></td>
                        <td class="jp-progress-bar">
                            <div class="jp-progress-container">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="jp-volume">
                            <div class="jp-volume-bar-container">
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </td>
                        <td class="jp-mute"><a href="#">mute</a></td>
                        <td class="jp-unmute"><a href="#">unmute</a></td>
                        <?php /*if ($type == 'video'):  <td class="jp-full-screen"><a href="#">fullscreen</a></td> */ ?>
                        <?php /*endif;*/ ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<?php
}


function yo_audio($autoplay = false)
{
    global $post;
    if (isset($post->ID))
    {
        $audio_type = get_field('yo_audio_type');
        if ($audio_type == 'hosted')
        {
            // Audio file
            $audio = wp_get_attachment_url(get_field('yo_audio_file'));
            if (!empty($audio))
            {
                $swfPath = get_template_directory_uri().'/assets/js/';
                $autoplay = ($autoplay) ? '.jPlayer("play")' : '';
                // Output
                yo_media($post->ID, 'audio');
                // Javascript
                yo_jquery('$("#post-'.$post->ID.'-media").jPlayer({ready: function(event) {$(this).jPlayer("setMedia", {mp3: "'.$audio.'"})'.$autoplay.'; }, swfPath: "'.$swfPath.'", supplied: "mp3", cssSelectorAncestor: "#post-'.$post->ID.'-media-ui"});', true, false);
                // yo_jquery('$("#post-'.$post->ID.'-media-ui .jp-full-screen").click(function(){ $("#post-'.$post->ID.'-media"). });', true, false);
                // yo_jquery('$("#post-'.$post->ID.'-media").jPlayer({ready: function(event) {$(this).jPlayer("setMedia", {mp3: "'.$audio.'", poster: "'.$poster.'"}); }, swfPath: "'.$swfPath.'", supplied: "mp3", cssSelectorAncestor: "#post-'.$post->ID.'-media-ui", play: function(){fitVideos(null); }, seeking: function(){fitVideos(null); }, seeked: function(){fitVideos(null); } });', true, false);
            }
        }
        else if ($audio_type == 'embed')
        {
            // Embedded audio
            echo '<div class="embedded-content">';
            the_field('yo_audio_embed');
            echo '</div>';
        }
    }
}

function yo_video($autoplay = false)
{
    global $post;
    if (isset($post->ID))
    {
        $video_type = get_field('yo_video_type');
        if ($video_type == 'hosted')
        {
            // Video files
            $m4v = wp_get_attachment_url(get_field('yo_video_m4v'));
            $ogv = wp_get_attachment_url(get_field('yo_video_ogv'));
            $webmv = wp_get_attachment_url(get_field('yo_video_webmv'));
            // Supplied files and formats
            $files = array();
            $formats = array();

            if (!empty($m4v)) { $files[] = 'm4v: "'.$m4v.'"'; $formats[] = 'm4v'; }
            if (!empty($ogv)) { $files[] = 'ogv: "'.$ogv.'"'; $formats[] = 'ogv'; }
            if (!empty($webmv)) { $files[] = 'webmv: "'.$webmv.'"'; $formats[] = 'webmv'; }

            if (!empty($m4v) OR !empty($ogv) OR !empty($webmv))
            {
                $files = implode(',', $files);
                $formats = implode(',', $formats);
                $poster = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                $poster = (isset($poster[0])) ? $poster[0] : '';
                $swfPath = get_template_directory_uri().'/assets/js/';
                $autoplay = ($autoplay) ? '.jPlayer("play")' : '';
                // Output
                yo_media($post->ID, 'video');

                // Javascript
                echo '<script type="text/javascript" data-needed="true">jQuery(document).ready(function($){';
                echo '$("#post-'.$post->ID.'-media").jPlayer({ready: function(event) {$(this).jPlayer("setMedia", {'.$files.', poster: "'.$poster.'"})'.$autoplay.'; }, swfPath: "'.$swfPath.'", supplied: "'.$formats.'", cssSelectorAncestor: "#post-'.$post->ID.'-media-ui", play:fitVideos, seeking:fitVideos, seeked:fitVideos});';
                echo '});</script>';

                // yo_jquery('$("#post-'.$post->ID.'-media").jPlayer({ready: function(event) {$(this).jPlayer("setMedia", {'.$files.', poster: "'.$poster[0].'"})'.$autoplay.'; }, swfPath: "'.$swfPath.'", supplied: "'.$formats.'", cssSelectorAncestor: "#post-'.$post->ID.'-media-ui", play:fitVideos, seeking:fitVideos, seeked:fitVideos});', true, false);
            }
        }
        else if ($video_type == 'embed')
        {
            // Embedded video
            echo '<div class="embedded-content">';
            the_field('yo_video_embed');
            echo '</div>';
            // if ($autoplay)
                 // yo_jquery('var video'.$post->ID.' = $("#post-'.$post->ID.' embed, #post-'.$post->ID.' iframe")[0]; video'.$post->ID.'.play();', true, false);
        }
    }
}

function yo_gallery($css_class = '', $thumbnails = false)
{
    global $post;
    if (isset($post->ID))
    {
        $images = get_field('yo_gallery_images');

        // echo '<pre>';
        // print_r($images);
        // echo '</pre>';
        // echo '<br>-------------<br>';

        // Get the images from the custom gallery or from the attached images (wp gallery)
        if (!empty($images))
        {
            foreach ($images as &$image)
            {
                $image['thumb'] = get_the_image($image['media'], 'thumbnail');
                $image['media'] = get_the_image($image['media']);
            }
        }
        else
        {
            $images = array();
            $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID); 
            $attachments = get_posts($args);

            // echo '<pre>';
            // print_r($attachments);
            // echo '</pre>';
            // echo '<br>-------------<br>';

            foreach ($attachments as &$attachment)
            {
                // echo $attachment->post_content;
                // echo '<pre>';
                //     print_r($attachment);
                // echo '</pre>';
                // echo '<br>-------------<br>';
                $images[] = array(
                    'thumb' => get_the_image($attachment->ID, 'thumbnail'),
                    'media' => wp_get_attachment_url($attachment->ID),
                    'title' => $attachment->post_title,
                    'description' => $attachment->post_content,
                );
            }
        }

        reset($images);

        // Slides
        $output = '<div class="flexslider '.$css_class.'"><ul class="slides">';
        foreach ($images as &$image)
        {
            $title = (isset($image['title']) && !empty($image['title'])) ? $image['title'] : '';
            $caption = (isset($image['caption'])) ? $image['caption'] : 'top-right-caption';
            $link = (isset($image['link']) && !empty($image['link'])) ? $image['link'] : '';
            $target = (isset($image['target']) && !empty($image['target'])) ? $image['target'] : '';
            $output .= '<li>';
            if ($link) $output .= '<a href="'.$link.'" target="'.$target.'">';
            $output .= '<img src="'.$image['media'].'" alt="'.$title.'">';
            if (isset($image['description']) && !empty($image['description'])) $output .= '<p class="slider-caption '.$caption.'">'.$image['description'].'</p>';
            if ($link) $output .= '</a>';
            $output .= '</li>';
        }

        $output .= '</ul></div>';

        // reset($images);

        // echo '<pre>';
        // print_r($images);
        // echo '</pre>';
        // echo '<br>-------------<br>';

        // Javascript
        $settings   = get_field('yo_gallery_settings');
        $slideshow  = (isset($settings['slideshow']) && !empty($settings['slideshow'])) ? $settings['slideshow'] : false;
        $slideshow  = ($slideshow) ? 'true' : 'false'; // Boolean string for JS
        $transition = (isset($settings['transition']) && !empty($settings['transition'])) ? $settings['transition'] : 'fade';
        $duration   = (isset($settings['duration']) && !empty($settings['duration'])) ? $settings['duration'] : '300';
        $delay      = (isset($settings['delay']) && !empty($settings['delay'])) ? $settings['delay'] : '7000';
        $sync       = '';

        // Thumbnails carousel
        if ($thumbnails)
        {
            $sync = '#post-'.$post->ID.'-carousel';
            $output .= '<div id="post-'.$post->ID.'-carousel" class="flexslider carousel compact-nav"><ul class="slides">';
            foreach ($images as &$image)
                $output .= '<li><img src="'.$image['thumb'].'" alt=""></li>';
            $output .= '</ul></div>';

            // echo '<pre>';
            // print_r($images);
            // echo '</pre>';

            yo_jquery('$("'.$sync.'").flexslider({animation: "slide", controlNav: false, animationLoop: false, slideshow: false, asNavFor: "#post-'.$post->ID.' .flexslider", itemWidth:150, itemMargin:5 });', true, false);
        }

        yo_jquery('$("#post-'.$post->ID.' .flexslider").flexslider({animation: "'.$transition.'", easing: "swing", controlNav: false, after: $.refreshMasonry, slideshow: '.$slideshow.', slideshowSpeed: '.$delay.', animationSpeed: '.$duration.', sync:"'.$sync.'"});', true, false);

        return $output;
    }

    return NULL;
}


// Remove gallery shortcode
remove_shortcode('gallery');
add_shortcode('gallery', 'yo_gallery_shortcode');
function yo_gallery_shortcode($attr) { }


/*
 *------------------------------------------------------------------------------------------------
 * Returns the 'class' and 'href' paramenters for the lightbox anchor
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_lightbox($force = false)
{
    if (get_field('yo_lightbox') == true || $force == true)
    {
        global $post;
        if ($post) $href = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
        return (isset($href[0])) ? ' class="zoom" href="'.$href[0].'"' : '';
    }
    else
    {
        return ' href="'.get_permalink().'"';
    }
}


/*
 *------------------------------------------------------------------------------------------------
 * Output the overlay HTML tags
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_overlay($icon = false)
{
    // if (!$icon) $icon = get_field('yo_overlay_icon');
    // if (!$icon) $icon = '';

    $overlay_content = '';

    if (!$icon)
    {
        $icon = get_field('yo_overlay_icon');
        $text_color = get_field('yo_text_color_overlay');
        $text_color = ($text_color) ? ' style="color:'.$text_color.';"' : '';

        if ($icon == 'title')
        {
            $overlay_content = '<h3'.$text_color.'>'.get_the_title().'</h3>';
            $icon = '';   
        }
        else if ($icon == 'title-excerpt')
        {
            $overlay_content  = '<h3'.$text_color.'>'.get_the_title().'</h3>';
            $overlay_content .= '<p'.$text_color.'>'.get_the_excerpt().'</p>';
            $icon = ''; 
        }
    }

    $color = get_field('yo_color_overlay');
    $color = ($color) ? ' style="background-color:'.$color.';"' : '';

    $hover = (get_field('yo_overlay_hover')) ? ' visible' : '';
    // $hover = ($hover || $hover == NULL) ? ' visible' : '';

    if ($icon || $color)
    {
        echo '
        <div class="overlay '.$icon.$hover.'"'.$color.'>
            <div class="overlay-inner">
                <div class="overlay-content">'.$overlay_content.'</div>
            </div>
        </div>';    
    }    
}



/*
 *------------------------------------------------------------------------------------------------
 * Template for Comments and Pingbacks
 *------------------------------------------------------------------------------------------------
 *
 */
if ( ! function_exists('yo_comment')) :
function yo_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    // echo '<pre>';
    // print_r($comment);
    // echo '</pre>';

    // Reduce the author name's characters (prevent ultra long names)
    $comment->comment_author = substr(get_comment_author(), 0, 20);

    switch ($comment->comment_type):
        case 'pingback':
        case 'trackback':
    ?>
    <li class="post pingback">
        <p><?php _e('Pingback:', 'theme_admin'); ?> <?php comment_author_link(); ?><?php edit_comment_link(__('Edit', 'theme_admin'), '<span class="edit-link">', '</span>'); ?></p>
    <?php
            break;
        default:
    ?>

    <!-- Comment -->
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <a class="object" href="#">
                <?php 
                    $avatar_size = 68;
                    if ('0' != $comment->comment_parent)
                        $avatar_size = 39;

                    echo get_avatar($comment, $avatar_size);
                ?>
            </a>
            
            <div class="comment-content clearfix">
                <ul class="comment-meta">
                    <li><?php comment_author_link(); ?> &mdash; </li>
                    <li><time pubdate datetime="<?php comment_time('c'); ?>"><?php comment_date(); ?> <?php comment_time(); ?></time></li>
                    <!-- <li><a href="#" class="micro button">Reply</a></li> -->
                    <li><?php comment_reply_link(array_merge($args, array('reply_text' => __('<span class="micro button">Reply &darr;</span>', 'theme_admin'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?></li>
                    <li><?php edit_comment_link(__('Edit', 'theme_admin'), '<span class="edit-link micro button">', '</span>'); ?></li>
                </ul>

                <?php if ($comment->comment_approved == '0') : ?>
                    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'theme_admin'); ?></em>
                    <br />
                <?php endif; ?>

                <?php comment_text(); ?>
            </div>
        </article>
    </li> <!-- / Comment -->

    <?php
            break;
    endswitch;
}
endif; // ends check for yo_comment()



/*
 *------------------------------------------------------------------------------------------------
 * Add custom styles to the TinyMCE editor
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_mce_buttons_2($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'yo_mce_buttons_2');

function yo_mce_before_init($settings)
{
    $style_formats = array(
        array(
            'title' => 'Button',
            'selector' => '*',
            'classes' => 'button'
        ),
        array(
            'title' => 'Align left',
            'selector' => '*',
            // 'block' => 'div',
            'classes' => 'header-alignleft',
            // 'wrapper' => true
        ),
        array(
            'title' => 'Align center',
            'selector' => '*',
            // 'block' => 'div',
            'classes' => 'header-aligncenter',
            // 'wrapper' => true
        ),
        array(
            'title' => 'Align right',
            'selector' => '*',
            // 'block' => 'div',
            'classes' => 'header-alignright',
            // 'wrapper' => true
        ),
        array(
            'title' => 'Black background',
            'selector' => '*',
            'classes' => 'black-highlight'
        ),
        array(
            'title' => 'White background',
            'selector' => '*',
            'classes' => 'white-highlight'
        ),
        array(
            'title' => 'Black text shadow',
            'selector' => '*',
            'classes' => 'black-text-shadow'
        ),
        array(
            'title' => 'White text shadow',
            'selector' => '*',
            'classes' => 'white-text-shadow'
        ),
        array(
            'title' => 'Responsive image',
            'selector' => 'img',
            'classes' => 'max-img'
        ),
        array(
            'title' => 'Responsive embed',
            'block' => 'div',
            'classes' => 'media',
            'wrapper' => true
        ),
        array(
            'title' => 'Lightbox',
            'selector' => 'a',
            'classes' => 'zoom'
        ),
        // array(
        //     'title' => 'Bold Red Text',
        //     'inline' => 'span',
        //     'styles' => array(
        //         'color' => '#f00',
        //         'fontWeight' => 'bold'
        //     )
        // )
    );

    $settings['style_formats'] = json_encode($style_formats);

    return $settings;

}
add_filter('tiny_mce_before_init', 'yo_mce_before_init');



// Wordpress WPML
function yo_post_languages()
{
    if (function_exists('icl_get_languages'))
    {
      $languages = icl_get_languages('skip_missing=1');
      if (1 < count($languages))
      {
        echo '<p class="clearfix" style="clear:both;">';
        echo __('This post is also available in: ', 'theme_admin');
        foreach($languages as $l)
          if(!$l['active']) $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
        echo join(', ', $langs);
        echo '</p>';
      }
    }
}


function yo_infinite_scroll()
{
    // $offset = $_POST['offset']; 
    // $numberposts = $_POST['numberposts'];   
    // $pageid = $_POST['pageid'];
    $nonce = $_POST['nonce'];       
    
    if (!wp_verify_nonce($nonce, 'yo_is_nonce'))
        die();

    global $query_string;

    $args = array(
        'post_type' => 'post',
        'paged' => get_query_var('paged'),
        'orderby' => 'date',
        'order' => 'DESC',
        'cat' => ''
    );

    // $page_title = get_the_title();
    $page_layout = get_field('yo_blog_layout');
    $categories = get_field('yo_blog_categories');
    $excluded_categories = get_field('yo_blog_excluded_categories');
    $posts_per_page = get_field('yo_blog_posts_per_page');
    // $infinite_scroll = get_field('yo_infinite_scroll');

    if (!empty($categories)) $args['cat'] .= implode(',', $categories);
    if (!empty($excluded_categories)) 
        foreach ($excluded_categories as &$excluded)
            $args['cat'] .= ',-'.$excluded;
    if (!empty($posts_per_page)) $args['posts_per_page'] = $posts_per_page;

    query_posts($args);

    include(locate_template('loop.php'));

    wp_reset_query();

   // die($results);
    exit;
}

// if(!is_admin())
// {
    add_action('wp_ajax_nopriv_infinite_scroll', 'yo_infinite_scroll');
    add_action('wp_ajax_infinite_scroll', 'yo_infinite_scroll');
// }