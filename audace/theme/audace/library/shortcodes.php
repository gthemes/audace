<?php

/*
 *------------------------------------------------------------------------------------------------
 * Horizontal Rule
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_hr($atts, $content = null)
{
    // extract(shortcode_atts(array('span' => ''), $atts));
    // $span = ( ! empty($span)) ? 'span'.$span : '';
    $output = '<div class="hr_pattern"></div>';
    return $output;
}
add_shortcode('yo_hr', 'yo_hr');


/*
 *------------------------------------------------------------------------------------------------
 * Columns
 *------------------------------------------------------------------------------------------------
 *
 */
// function yo_column($atts, $content = null)
// {
//     extract(shortcode_atts(array('grid' => '',
//                                  'title' => ''
//                                  ), $atts));

//     $output = '<div class="grid_'.$grid.'">';

//     $content_parsed = do_shortcode($content);

//     if ( ! empty($title))
//         $output .= '<h3>'.$title.'</h3>';
    
//     if ($content)
//         $output .= ($content_parsed != $content) ? $content_parsed : '<p>'.$content.'</p>';
    
//     $output .= '</div>';

//     return $output;
// }

// add_shortcode('yo_column', 'yo_column');

function yo_columns($atts, $content)
{
    $GLOBALS['yo_columns_data'] = array();

    do_shortcode($content);

    if(is_array($GLOBALS['yo_columns_data']))
    {
        $output = '<div class="row-fluid">';

        foreach($GLOBALS['yo_columns_data'] as $column)
        {
            $output .= '<div class="span'.$column['span'].'">';

            if ( ! empty($column['title']))
                $output .= '<h3>'.$column['title'].'</h3>';
            
            if ($column['content'])
            {
                $content_parsed = do_shortcode($column['content']);
                $output .= ($content_parsed != $column['content']) ? $content_parsed : '<p>'.$column['content'].'</p>';
            }
    
            $output .= '</div>';
            // $default_tab = (isset($tab['default']) && ! empty($tab['default'])) ? ' class="default-tab"' : '';
            // $tabs[] = '<li><a href="#'.$tab['id'].'"'.$default_tab.' rel="nofollow">'.$tab['title'].'</a></li>';
            // $panes[] = '<div id="'.$tab['id'].'">'.$tab['content'].'</div>';
        }

        $output .= '</div>';

        // $output = '<ul class="tabs clearfix">'.implode( "\n", $tabs ).'</ul><div class="tabs-content">'.implode( "\n", $panes ).'</div>';
    }
    return $output;
}

add_shortcode('yo_columns', 'yo_columns');


function yo_column($atts, $content)
{
    extract(shortcode_atts(array('id' => '', 'title' => '', 'span' => ''), $atts));
    $GLOBALS['yo_columns_data'][] = array('id' => $id, 'title' => $title, 'span' => $span, 'content' => $content);
}

add_shortcode('yo_column', 'yo_column');




/*
 *------------------------------------------------------------------------------------------------
 * Hero Text
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_hero($atts, $content = null)
{
    extract(shortcode_atts(array('text' => ''), $atts));
    $text = ( ! empty($text)) ? $text : '';
    $output = '<h2 class="intro-heading">'.$text.'</h2>';
    return $output;
}

add_shortcode('yo_hero', 'yo_hero');



/*
 *------------------------------------------------------------------------------------------------
 * Tabs
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_tabs($atts, $content)
{
    $GLOBALS['yo_tabs_data'] = array();

    do_shortcode($content);

    if(is_array($GLOBALS['yo_tabs_data']))
    {
        foreach($GLOBALS['yo_tabs_data'] as $tab)
        {
            $default_tab = (isset($tab['default']) && ! empty($tab['default'])) ? ' class="default-tab"' : '';
            $tabs[] = '<li><a href="#'.$tab['id'].'"'.$default_tab.' rel="nofollow">'.$tab['title'].'</a></li>';
            $panes[] = '<div id="'.$tab['id'].'">'.$tab['content'].'</div>';
        }
        $output = '<ul class="tabs clearfix">'.implode( "\n", $tabs ).'</ul><div class="tabs-content">'.implode( "\n", $panes ).'</div>';
    }
    return $output;
}

add_shortcode('yo_tabs', 'yo_tabs');


function yo_tab($atts, $content)
{
    $default = (count($GLOBALS['yo_tabs_data']) == 0) ? true : '';
    extract(shortcode_atts(array('id' => '', 'title' => '', 'default' => $default), $atts));
    $GLOBALS['yo_tabs_data'][] = array('id' => $id, 'title' => $title, 'default' => $default, 'content' => $content);
}

add_shortcode('yo_tab', 'yo_tab');


/*
 *------------------------------------------------------------------------------------------------
 * Accordion
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_accordions($atts, $content)
{
    $content = do_shortcode($content);

    $output = '<div class="accordion clearfix">'.$content.'</div>';

    return $output;
}

add_shortcode('yo_accordions', 'yo_accordions');


function yo_accordion($atts, $content)
{
    $content = do_shortcode($content);
    extract(shortcode_atts(array('title' => '', 'default' => ''), $atts));
    
    $output = '<div class="accordion-trigger"><a href="#">'.$title.'</a></div>
               <div class="accordion-content">'.$content.'</div>';

    return $output;
}

add_shortcode('yo_accordion', 'yo_accordion');


/*
 *------------------------------------------------------------------------------------------------
 * Slider
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_slider($atts, $content)
{
    $content = do_shortcode($content);
    $output = '<div class="content-slider compact-nav slider-shortcode"><div class="flexslider"><ul class="slides">'.$content.'</ul></div></div>';    
    return $output;
}

add_shortcode('yo_slider', 'yo_slider');


function yo_slide($atts, $content = null)
{
    extract(shortcode_atts(array('title' => '', 'image' => '', 'link' => '', 'target' => '_self'), $atts));
    
    $output = '<img src="'.$image.'" alt="'.$title.'">';
    if ($link) $output = '<a href="'.$link.'" target="'.$target.'">'.$output.'</a>';
    $output = '<li>'.$output.'</li>';

    return $output;
}

add_shortcode('yo_slide', 'yo_slide');



/*
 *------------------------------------------------------------------------------------------------
 * Video/Audio Embed
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_embed($atts) {
    extract(shortcode_atts(array('type' => '', 'src' => '','width' => '','height' => ''), $atts));
    $show_width = ($width) ? ' width="'.$width.'"' : '';
    $show_height = ($height) ? ' height="'.$height.'"' : '';

    switch ($type)
    {
        case 'youtube':
        default:
            if ($src) $show_src = esc_url(str_replace('watch?v=','embed/',$src), array('http'));
            $output = '<div class="object"><iframe '.$show_width.$show_height.' src="'.$show_src.'" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe></div>';
            break;

        case 'vimeo':
            if ($src) $show_src = esc_url(str_replace('http://vimeo.com/', 'http://player.vimeo.com/video/', $src), array('http'));
            $output = '<div class="object"><iframe src="'.$show_src.'?title=0&amp;byline=0&amp;portrait=0" '.$show_width.$show_height.' frameborder="0" webkitAllowFullScreen allowFullScreen></iframe></div>';
            break;

        case 'ustream':
            if ($src) $show_src = esc_url(preg_replace('/recorded/','/embed/$0',$src), array('http'));
            $output = '<div class="object"><iframe src="'.$show_src.'" '.$show_width.$show_height.'  scrolling="no" frameborder="0" style="border: 0px none transparent;"></iframe></div>';
            break;

        default:
            $output = $src;
            break;
    }

    return $output;
}

add_shortcode('yo_embed', 'yo_embed');




/*
 *------------------------------------------------------------------------------------------------
 * Maps
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_maps($atts, $content = null) {
   extract(shortcode_atts(array('width' => '', 'height' => '', 'src' => ''), $atts));
   if (empty($width)) $width = '100%';
   if (empty($height)) $height = '381';
   return '<iframe width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'&output=embed"></iframe>';
}

add_shortcode("yo_maps", "yo_maps");




/*
 *------------------------------------------------------------------------------------------------
 * Contact Form
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_contact($atts)
{
    // yo_js('var ajaxurl = "'.admin_url('admin-ajax.php').'";');
    // yo_js('var contact_data = {ajaxurl:"'.admin_url('admin-ajax.php').'", thank_you:"'.__('Thank you for contacting us!', 'theme_admin').'"};');
    // wp_localize_script('yo-script', 'contact_data', array('ajaxurl' => admin_url('admin-ajax.php'), 'thank_you' => __('Thank you for contacting us!', 'theme_admin')));
    // wp_localize_script('yo-script', 'contact_data', array('ajaxurl' => admin_url('admin-ajax.php'), 'thank_you' => __('Thank you for contacting us!', 'theme_admin')));

    $output = '
        <form id="contact-form" action="" method="post">
          <fieldset id="fields">

                <label for="name">'.__('Name', 'theme_admin').' *</label>
                <input type="text" id="name" name="name">

                <label for="email">'.__('E-mail', 'theme_admin').' *</label>
                <input type="text" id="email" name="email">
            
                <label for="subject">'.__('Subject', 'theme_admin').'</label>
                <input type="text" id="subject" name="subject">

                <label for="message">'.__('Your Message', 'theme_admin').' *</label>
                <textarea id="message" name="message"></textarea>

                <button class="big full button">'.__('Send', 'theme_admin').' &rarr;</button>

          </fieldset>
          <div id="info"></div>
        </form>';

    return $output;
}

add_shortcode("yo_contact", "yo_contact");





/*
 *------------------------------------------------------------------------------------------------
 * Blockquotes
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_quote($atts, $content = null) {
   extract(shortcode_atts(array('author' => ''), $atts));
   $output = '<blockquote>'.$content.'<cite>'.$author.'</cite></blockquote>';
   return $output;
}

add_shortcode("yo_quote", "yo_quote");




/*
 *------------------------------------------------------------------------------------------------
 * Buttons
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_button($atts, $content = null) {
   extract(shortcode_atts(array('href' => '#', 'target' => '_blank', 'type' => ''), $atts));
   $output = '<a href="'.$href.'" target="'.$target.'" class="'.$type.' button">'.$content.'</a>';
   return $output;
}

add_shortcode("yo_button", "yo_button");



/*
 *------------------------------------------------------------------------------------------------
 * Messages
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_message($atts, $content = null) {
   extract(shortcode_atts(array('type' => 'info'), $atts));
   $output = '<div class="'.$type.'">'.$content.'</div>';
   return $output;
}

add_shortcode("yo_message", "yo_message");




/*
 *------------------------------------------------------------------------------------------------
 * List
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_ul($atts, $content)
{
    $content = do_shortcode($content);

    $output = '<ul>'.$content.'</ul>';

    return $output;
}

add_shortcode('yo_ul', 'yo_ul');


function yo_li($atts, $content)
{
    $content = do_shortcode($content);
    extract(shortcode_atts(array('icon' => '', 'icon_color' => 'dark'), $atts));
    $icon_color = ($icon_color == 'light') ? '-light' : '';
    $icon = (empty($icon)) ? '' : '<i class="icon-'.$icon.$icon_color.'"></i>';

    // get_template_directory_uri().'/img/icons/lists/light/

    $output = '<li class="custom-list">'.$icon.$content.'</li>';

    return $output;
}

add_shortcode('yo_li', 'yo_li');






// Direct shortcode
// function yo_hr($atts, $content = null)
// {
//     extract(shortcode_atts(array('id' => ''), $atts));
//     $output = '<div class="clearfix hr_pattern"></div>';
//     return $output;
// }
// add_shortcode('yo_hr', 'yo_hr');