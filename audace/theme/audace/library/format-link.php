<?php
    $link_url = get_field('yo_link_url');
    $link_target = get_field('yo_link_target');
    if (empty($link_url)) $link_url = get_permalink();
    if (empty($link_target)) $link_target = '_self';
?>

<?php if (is_singular()) : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>

        <?php if (has_post_thumbnail()): ?>
        <div class="object">
            <a title="<?php printf(__('Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                <?php the_post_thumbnail($image_size, array('class' => 'max-img')); ?>
            </a>
        </div>
        <?php endif; ?>

        <div class="caption">
            <h3 class="entry-title"><a title="<?php printf(__('Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php the_title(); ?> &rarr;</a></h3>
            <p class="entry-date"><?php echo get_the_date(); ?></p>
            <div class="entry-content"><?php the_content(); ?></div>
            <?php wp_link_pages(); ?>
            <?php yo_post_languages(); ?>
            <?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<p><a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a></p>'; ?>
        </div>
        
        <div class="caption meta">
            <p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
            <!-- Post tags -->
            <?php the_tags('<p><strong>'.__('Tags: ', 'theme_admin').'</strong>', ', ', '</p>'); ?>
        </div>
        <?php comments_template(); ?>
    </article>

<?php else : ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class($entry_class); ?>>
        
        <?php if (has_post_thumbnail()): ?>
        <div class="object">
            <a title="<?php printf(__('Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                <?php the_post_thumbnail($image_size, array('class' => 'max-img')); ?>
            </a>
        </div>
        <?php endif; ?>

        <div class="caption">
            <h3 class="entry-title"><a title="<?php printf(__('Link to %s', 'theme_admin'), get_the_title()); ?>" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php the_title(); ?> &rarr;</a></h3>
            <p class="entry-date"><?php echo get_the_date(); ?></p>
            <?php if (yo_has_excerpt()): ?><div class="entry-content"><?php the_excerpt(); ?></div><?php endif; ?>
            <?php $edit_link = get_edit_post_link(get_the_ID()); if ($edit_link) echo '<p><a href="'.$edit_link.'" class="mini button">'.__('Edit', 'theme_admin').'</a></p>'; ?>
        </div>

        <?php $meta_caption = get_field('yo_show_additional_meta'); if ($meta_caption == true OR $meta_caption == NULL): ?>
        <div class="caption meta">
            <p><strong><?php _e('Comments', 'theme_admin'); ?>:</strong> <a href="<?php the_permalink(); ?>"><?php comments_number(0, 1, '%'); ?></a></p>
            <p><strong><?php _e('Author', 'theme_admin'); ?>:</strong> <a href="#"><?php the_author_posts_link(); ?></a></p>
            <p><strong><?php _e('Categories', 'theme_admin'); ?>:</strong> <?php echo get_the_category_list(', '); ?></p>
        </div>
        <?php endif; ?>

    </article>

<?php endif; ?>