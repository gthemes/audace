<?php

function meta_box_enqueue_scripts($page){
    // check if this your page here with the upload form!
    if(($page !== 'post.php') || (get_post_type() !== 'post'))
        return;

    wp_enqueue_script('plupload-all');
}

// include js
add_action('admin_enqueue_scripts', 'meta_box_enqueue_scripts');





function add_meta_box_gallery() {
    add_meta_box('gallery_photos', __('Photos', 'theme_admin'), 'upload_meta_box', 'post', 'normal', 'high');  
}

// this adds a simple metabox with the upload form on the edit-post page
add_action('add_meta_boxes', 'add_meta_box_gallery');                                               





// so here's the actual uploader
// most of the code comes from media.php and handlers.js
function upload_meta_box(){ ?>
   <div id="plupload-upload-ui" class="hide-if-no-js">
     <div class="yo-dropped-items"></div>
     <div id="drag-drop-area">
       <div class="drag-drop-inside">
        <p class="drag-drop-info"><?php _e('Drop files here'); ?></p>
        <p><?php _ex('or', 'Uploader: Drop files here - or - Select Files'); ?></p>
        <p class="drag-drop-buttons"><input id="plupload-browse-button" type="button" value="<?php esc_attr_e('Select Files'); ?>" class="button" /></p>
      </div>
     </div>
  </div>

  <?php

  $plupload_init = array(
    'runtimes'            => 'html5,silverlight,flash,html4',
    'browse_button'       => 'plupload-browse-button',
    'container'           => 'plupload-upload-ui',
    'drop_element'        => 'drag-drop-area',
    'file_data_name'      => 'async-upload',
    'multiple_queues'     => true,
    'max_file_size'       => wp_max_upload_size().'b',
    'url'                 => admin_url('admin-ajax.php'),
    'flash_swf_url'       => includes_url('js/plupload/plupload.flash.swf'),
    'silverlight_xap_url' => includes_url('js/plupload/plupload.silverlight.xap'),
    'filters'             => array(array('title' => __('Allowed Files', 'theme_admin'), 'extensions' => '*')),
    'multipart'           => true,
    'urlstream_upload'    => true,

    // additional post data to send to our ajax hook
    'multipart_params'    => array(
      '_ajax_nonce' => wp_create_nonce('photo-upload'),
      'action'      => 'photo_gallery_upload',            // the ajax action name
    ),
  );

  // we should probably not apply this filter, plugins may expect wp's media uploader...
  /*$plupload_init = apply_filters('plupload_init', $plupload_init);*/ ?>

  <script type="text/javascript">

    jQuery(document).ready(function($){

      // create the uploader and pass the config from above
      var uploader = new plupload.Uploader(<?php echo json_encode($plupload_init); ?>);

      // checks if browser supports drag and drop upload, makes some css adjustments if necessary
      uploader.bind('Init', function(up){
        var uploaddiv = $('#plupload-upload-ui');

        if(up.features.dragdrop){
          uploaddiv.addClass('drag-drop');
            $('#drag-drop-area')
              .bind('dragover.wp-uploader', function(){ uploaddiv.addClass('drag-over'); })
              .bind('dragleave.wp-uploader, drop.wp-uploader', function(){ uploaddiv.removeClass('drag-over'); });

        }else{
          uploaddiv.removeClass('drag-drop');
          $('#drag-drop-area').unbind('.wp-uploader');
        }
      });

      uploader.init();

      // a file was added in the queue
      uploader.bind('FilesAdded', function(up, files){
        var hundredmb = 100 * 1024 * 1024, max = parseInt(up.settings.max_file_size, 10);

        plupload.each(files, function(file){
          if (max > hundredmb && file.size > hundredmb && up.runtime != 'html5'){
            // file size error?

          }else{

            // a file was added, you may want to update your DOM here...
            // console.log(file);
          }
        });

        up.refresh();
        up.start();
      });

      // Progress bar ////////////////////////////////////////////
      // Add the progress bar when the upload starts
      // Append the tooltip with the current percentage
      uploader.bind('UploadProgress', function(up, file) {
          var progressBarValue = up.total.percent;
          console.log('loading: '+progressBarValue);
          // $('#progressbar').fadeIn().progressbar({
          //     value: progressBarValue
          // });
          // $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
      });

      // a file was uploaded 
      uploader.bind('FileUploaded', function(up, file, response) {

        console.log(response.response);
        response = $.parseJSON(response['response']);

        if (response)
        {
          $('.yo-dropped-items').append('<img src="'+response.thumb+'">');
        }
        else
        {
          console.log('Upload Error!');
        }
        // this is your ajax response, update the DOM with it or something...
        
        // console.log(up);
        // console.log(file.id);
        // console.log('------------------');
        console.log(response);
        // console.log(response.response.type);


        // plu_show_thumbs(file.id);
      });

    });   

  </script>
  <?php
}


function photo_gallery_upload()
{
    // global $post;

    check_ajax_referer('photo-upload');

    $file = $_FILES['async-upload'];
    $upload = wp_handle_upload($file, array('test_form'=>true, 'action' => 'photo_gallery_upload'));

    if (empty($status['error'])) {

      $upload['id'] = wp_insert_attachment( array(
                        'post_mime_type' => $upload['type'],
                        'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['name'])),
                        'post_content' => '',
                        'post_status' => 'inherit',
                        // 'post_type' => 'attachment',
                        // 'post_parent' => $post->ID,
                        ), $upload['file']);

      // $thumb = image_resize($upload['file'], 100, 100, true);

      // if (is_wp_error($thumb)) exit;

      // $uploads = wp_upload_dir();
      // $upload['thumb'] = $uploads['url'].'/'.basename($thumb);

      yo_resize_only('thumbnail');

      $attach_data = wp_generate_attachment_metadata( $upload['id'], $upload['file'] );
      wp_update_attachment_metadata( $upload['id'], $attach_data );

      // wp_generate_attachment_metadata($upload['id'], $upload['file']); 

      $thumb = wp_get_attachment_image_src($upload['id'], 'thumbnail');
      $upload['thumb'] = $thumb[0];

      // Create an Attachment in WordPress
      // $id = wp_insert_attachment($status['id'], $status[ 'file' ], $post->ID);
      // wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $status['file'] ) );
      // update_post_meta($post->ID, 'async-upload', $id);

      header( "Content-Type: application/json" );
      echo json_encode($upload);
    }

    exit;
}

// handle uploaded file here
add_action('wp_ajax_photo_gallery_upload', 'photo_gallery_upload');







/*
* Resize images dynamically using wp built in functions
* Victor Teixeira
*
* php 5.2+
*
* Exemplo de uso:
*
* <?php
* $thumb = get_post_thumbnail_id();
* $image = vt_resize($thumb, '', 140, 110, true);
* ?>
* <img src="<?php echo $image[url]; ?>" width="<?php echo $image[width]; ?>" height="<?php echo $image[height]; ?>" />
*
* @param int $attach_id
* @param string $img_url
* @param int $width
* @param int $height
* @param bool $crop
* @return array
*/
if(!function_exists('vt_resize')){

function vt_resize($attach_id = null, $img_url = null, $width, $height, $crop = false){

    if($attach_id){
        // this is an attachment, so we have the ID
        $image_src = wp_get_attachment_image_src($attach_id, 'full');
        $file_path = get_attached_file($attach_id);
    } elseif($img_url){
        // this is not an attachment, let's use the image url
        $file_path = parse_url($img_url);
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path['path'];
        // Look for Multisite Path
        if(file_exists($file_path) === false){
            global $blog_id;
            $file_path = parse_url($img_url);
            if(preg_match('/files/', $file_path['path'])){
                $path = explode('/', $file_path['path']);
                foreach($path as $k => $v){
                    if($v == 'files'){
                        $path[$k-1] = 'wp-content/blogs.dir/'.$blog_id;
                    }
                }
                $path = implode('/', $path);
            }
            $file_path = $_SERVER['DOCUMENT_ROOT'].$path;
        }
        //$file_path = ltrim( $file_path['path'], '/' );
        //$file_path = rtrim( ABSPATH, '/' ).$file_path['path'];
        $orig_size = getimagesize($file_path);
        $image_src[0] = $img_url;
        $image_src[1] = $orig_size[0];
        $image_src[2] = $orig_size[1];
    }

    $file_info = pathinfo($file_path);
    // check if file exists
    $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];
    if(!file_exists($base_file))
    return;
    $extension = '.'. $file_info['extension'];
    // the image path without the extension
    $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
    $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;
    // checking if the file size is larger than the target size
    // if it is smaller or the same size, stop right here and return
    if($image_src[1] > $width){
        // the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
        if(file_exists($cropped_img_path)){
            $cropped_img_url = str_replace(basename($image_src[0]), basename($cropped_img_path), $image_src[0]);
            $vt_image = array(
                'url'   => $cropped_img_url,
                'width' => $width,
                'height'    => $height
            );
            return $vt_image;
        }
        // $crop = false or no height set
        if($crop == false OR !$height){
            // calculate the size proportionaly
            $proportional_size = wp_constrain_dimensions($image_src[1], $image_src[2], $width, $height);
            $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;
            // checking if the file already exists
            if(file_exists($resized_img_path)){
                $resized_img_url = str_replace(basename($image_src[0]), basename($resized_img_path), $image_src[0]);
                $vt_image = array(
                    'url'   => $resized_img_url,
                    'width' => $proportional_size[0],
                    'height'    => $proportional_size[1]
                );
                return $vt_image;
            }
        }
        // check if image width is smaller than set width
        $img_size = getimagesize($file_path);
        if($img_size[0] <= $width) $width = $img_size[0];
            // Check if GD Library installed
            if(!function_exists('imagecreatetruecolor')){
                echo 'GD Library Error: imagecreatetruecolor does not exist - please contact your webhost and ask them to install the GD library';
                return;
            }
            // no cache files - let's finally resize it
            $new_img_path = image_resize($file_path, $width, $height, $crop);
            $new_img_size = getimagesize($new_img_path);
            $new_img = str_replace(basename($image_src[0]), basename($new_img_path), $image_src[0]);
            // resized output
            $vt_image = array(
                'url'   => $new_img,
                'width' => $new_img_size[0],
                'height'    => $new_img_size[1]
            );
            return $vt_image;
        }
        // default output - without resizing
        $vt_image = array(
            'url'   => $image_src[0],
            'width' => $width,
            'height'    => $height
        );
        return $vt_image;
    }
}