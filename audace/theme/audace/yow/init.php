<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Turbine is a lightweight and extensible suite of Wordpress plugins for theme developers.
Version		: 1.1.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/


// error_reporting(E_ALL);
// ini_set('display_errors', 1);

/**
 *------------------------------------------------------------------------------------------------
 * Configuration
 *------------------------------------------------------------------------------------------------
 *
 * To override the default values, define the constants before including the 'init.php' file.
 *
 */

// Script version
define('YO_VER', '1.1.0');

// Fields prefix (Used to avoid collisions in the database or other HTML elements)
if ( ! defined('YO_PREFIX'))
	define('YO_PREFIX', 'yo_');

// Define the ID key to store theme options
// TODO: change name to YO_OPTIONS
if ( ! defined('THEME_OPTIONS'))
	define('THEME_OPTIONS', YO_PREFIX.'theme_options');

// Define plugin URLs, for fast enqueuing scripts and styles
if ( ! defined('YO_URL'))
	define('YO_URL', get_template_directory_uri().'/yow/');

if ( ! defined('YO_JS_URL'))
	define('YO_JS_URL', YO_URL.'assets/js/');

if ( ! defined('YO_CSS_URL'))
	define('YO_CSS_URL', YO_URL.'assets/css/');

if ( ! defined('YO_IMG_URL'))
	define('YO_IMG_URL', YO_URL.'assets/img/');

if ( ! defined('YO_FIELDS_URL'))
	define('YO_FIELDS_URL', YO_URL.'fields/');

// Plugin paths, for including files
if ( ! defined('YO_DIR'))
	define('YO_DIR', trailingslashit(plugin_dir_path(__FILE__)));

if ( ! defined('YO_FIELDS_DIR'))
	define('YO_FIELDS_DIR', YO_DIR.'fields/');

if ( ! defined('YO_LIB'))
	define('YO_LIB', YO_DIR.'library/');

// A global array to store the multiple/cloneable fields
global $yo_multiple_fields;
$yo_multiple_fields = array();

// Include utilities
require_once YO_LIB.'utilities.php';

// Load the plugin files ONLY in the admin area
if (defined('WP_ADMIN') && WP_ADMIN)
// if (is_admin())
{
	// Load admin language file
	// $locale = get_locale();
	// $dir    = trailingslashit(YO_DIR . 'languages');
	// $mofile = "{$dir}{$locale}.mo";
	// In themes/current-theme/admin/ directory
	// load_theme_textdomain('theme_admin', $mofile);

	load_theme_textdomain('theme_admin', trailingslashit(get_template_directory() . '/languages'));
	// load_theme_textdomain('theme_admin', trailingslashit(YO_DIR . 'languages'));
    // $locale = get_locale();
    // $locale_file = get_template_directory() . "/languages/$locale.php";
    // if (is_readable($locale_file))
    //     require_once($locale_file);

	// Include field classes by single files
	// foreach (glob(YO_FIELDS_DIR.'*.php') as $file)
		// require_once $file;

    // Include field classes inside their own folders
    $dirs = array_filter(glob(YO_FIELDS_DIR.'*'), 'is_dir');
    foreach ($dirs as $dir)
    	require_once $dir.'/'.str_replace(YO_FIELDS_DIR, '', $dir).'.php';

	// Include main files
	require_once YO_LIB.'fields.php';
	require_once YO_LIB.'metabox.php';
	require_once YO_LIB.'options.php';
	require_once YO_LIB.'shortcodes.php';
}



/**
 *------------------------------------------------------------------------------------------------
 * Register Fields
 *------------------------------------------------------------------------------------------------
 *
 * Helper function that stores the multiple/cloneable field names in one array. This is done to
 * differentiate single (strings) and multiple (arrays) items when using them in theme pages.
 * You should not call this function directly, use yo_register_metabox and yo_register_options.
 *
 */

function yo_register_fields($fields)
{
	global $yo_multiple_fields;

	// TODO: Add automatic check in the Fields class
	$multiple_fields = array('checkbox_list', 'file', 'image', 'plupload_image');

	foreach ($fields as $field)
	{
		$multiple = in_array($field['type'], $multiple_fields);

		if (isset($field['clone']) || isset($field['multiple']) || $multiple)
			$yo_multiple_fields[] = $field['id'];
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Register Meta Boxes
 *------------------------------------------------------------------------------------------------
 *
 * Helper function to register the defined $metaboxes for admin pages.
 *
 */

function yo_register_metabox($metaboxes)
{
	if (class_exists('YO_Metabox'))
	{
	    foreach ($metaboxes as $metabox)
	        new YO_Metabox($metabox);
	}
	else
	{
		foreach ($metaboxes as $metabox)
	    	yo_register_fields($metabox['fields']);
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Register Theme Options
 *------------------------------------------------------------------------------------------------
 *
 * Helper function to register the defined $options for admin pages.
 *
 */

function yo_register_options($options)
{
	if (class_exists('YO_Options'))
	{
	    new YO_Options($options);
	}
	else
	{
		foreach ($options as $option)
	    	yo_register_fields($option['fields']);
	}
}


/**
 *------------------------------------------------------------------------------------------------
 * Theme Helpers
 *------------------------------------------------------------------------------------------------
 *
 * A set of functions to easily display the data inside theme pages.
 *
 */

function get_field($field_name, $post_id = false) 
{
	global $post; 
	
	// Use the current post ID if not set
	if( ! $post_id)
		if (isset($post->ID))
			$post_id = $post->ID; 

	if ( ! $post_id)
		return;

	// allow for option == options
	if($post_id == "option")
		$post_id = "options";

	// Set the current scope between post custom fields and theme options
	$prefix = 'yo_cache_';
	$scope = (is_numeric($post_id)) ? 'meta_'.$post_id : 'options';

	// Return the cache if already set
	$cache = wp_cache_get($prefix.$scope); 
	if ($cache)
		return (isset($cache[$field_name])) ? yo_check_multiple_field($field_name, $cache[$field_name]) : NULL;
		// return (isset($cache[$field_name])) ? unserialize_field($field_name, $cache[$field_name]) : false;

	// No cache available so get the fresh data
	$data = array();
	$data = (is_numeric($post_id)) ? get_post_custom($post_id) : get_option(THEME_OPTIONS);

	// Set the new value and update the cache
	$value = '';
	if ( ! empty($data))
	{
		wp_cache_set($prefix.$scope, $data); 
		$value = (isset($data[$field_name])) ? yo_check_multiple_field($field_name, $data[$field_name]) : NULL;
	}

	return $value;
}

function the_field($field_name, $post_id = false)
{
	$value = get_field($field_name, $post_id);
	
	if (is_array($value))
		$value = @implode(', ',$value);

	$value = do_shortcode($value);

	echo $value;
}

function yo_get_option($field_name)
{
	$value = get_field($field_name, 'option');
	return $value;
}

function the_option($field_name)
{
	$value = yo_get_option($field_name);
	
	if (is_array($value))
		$value = @implode(', ',$value);

	echo $value;
}

// Check if it's a multiple or cloneable field.
// Returns a multiple array to be used in a loop, or the single value for unique items.
function yo_check_multiple_field($field_name, $value)
{
	global $yo_multiple_fields;

	// First thing, check if it's a multiple or cloneable field
	if (in_array($field_name, $yo_multiple_fields))
		return (yo_is_serialized($value[0])) ? unserialize($value[0]) : $value;

	// Otherwise it's a single field...

	// Unserialize the array if necessary
	if (isset($value[0]) && yo_is_serialized($value[0]))
		$value = unserialize($value[0]);

	// Check if it's an array (group field) or a single item
	if (is_array($value))
		if (!is_associative($value))
			$value = (count($value) > 1) ? $value : $value[0];

	return $value;
}


// Echo the image URL
function the_image($media_id, $size = 'full')
{
	echo get_the_image($media_id, $size);
}

// Get the image URL value
function get_the_image($media_id, $size = 'full')
{
	$image = wp_get_attachment_image_src($media_id, $size);
	$image = (isset($image[0])) ? $image[0] : '';
	return $image;
}


/*
 *------------------------------------------------------------------------------------------------
 * Check Associative Arrays
 *------------------------------------------------------------------------------------------------
 *
 */
function is_associative($array)
{
	return (is_numeric(key($array))) ? false : true;
}


/*
 *------------------------------------------------------------------------------------------------
 * Check Serialized Arrays
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_is_serialized($str)
{
    // $data = @unserialize($str);
    // if ($str === 'b:0;' || $data !== false) {
    //     return true;
    // } else {
    //     return false;
    // }
    if (is_string($str))
    	return preg_match( "/^(O:|a:)/", $str);
    else
    	return false;
}


/*
 *------------------------------------------------------------------------------------------------
 * Force Wordpress to only resize the specified formats
 *------------------------------------------------------------------------------------------------
 *
 * Usage: yo_resize_only('thumbnail', 'medium', 'custom-format', ...);
 *
 */
function yo_resize_only()
{
  global $smart_resize;
  foreach(func_get_args() as $format) $smart_resize[] = $format;
}

function smart_resize($data)
{
  global $smart_resize;

  if (isset($smart_resize) && is_array($smart_resize))
    if (count($smart_resize) > 0)
      foreach ($data as $size => $params)
        if ( ! in_array($size, $smart_resize))
          unset($data[$size]);

  unset($smart_resize);

  return $data;
}

add_filter('intermediate_image_sizes_advanced', 'smart_resize');



/*
 *------------------------------------------------------------------------------------------------
 * Force Wordpress to only resize the specified formats
 *------------------------------------------------------------------------------------------------
 *
 * Usage: yo_resize_only('thumbnail', 'medium', 'custom-format', ...);
 *
 */

function yo_post_thumbnail2($size = 'full', $attr, $attachment_id = false)
{
	if (!$attachment_id)
		$attachment_id = get_post_thumbnail_id();

	$image_src = wp_get_attachment_image_src($attachment_id, $size);
    $file_path = get_attached_file($attachment_id);
    $file_info = pathinfo($file_path);
    // $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];

    // return;
    $extension = '.'. $file_info['extension'];
    // the image path without the extension
    $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
    $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;

    if(!file_exists($cropped_img_path))
    {
    	yo_resize_only($size);
	    $attach_data = wp_generate_attachment_metadata($attachment_id, $image_src);
	    wp_update_attachment_metadata($attachment_id, $attach_data);
    	$image_src = wp_get_attachment_image_src($attachment_id, $size);
    }

    $image_src = $image_src[0];
    
    // $default_attr = array('title' => );

    // $attr = wp_parse_args($attr, $default_attr);

    $img_html = '<img src="'.$image_src.'"';
    
    if (!empty($attr))
    	foreach ($attr as $key => $value)
			$img_html .= ' '.$key.'="'.$value.'"';

	$img_html = '>';

	return $img_html;

	// echo '<pre>Attachment ID: '.$attachment_id.'</pre>';
	// echo '<pre>Size: '.$size.'</pre>';
	// echo '<pre>Attr: ';
	// print_r($attr);
	// echo '</pre>';
	

	

	// return $html;
}

// add_filter('post_thumbnail_html', 'yo_thumbnail_html', 10, 5);


function yo_post_thumbnail($size = 'full', $attr, $attachment_id = false)
{
	if (!$attachment_id)
		$attachment_id = get_post_thumbnail_id();

	// var_dump(image_get_intermediate_size($attachment_id, $size));

	yo_maybe_create_missing_intermediate_images($attachment_id, $size);
	
	$image_src = wp_get_attachment_image_src($attachment_id, $size);

	$img_html = '';

	if ($image_src)
	{
		$image_src = $image_src[0];
		$img_html = '<img src="'.$image_src.'"';
    
	    if (!empty($attr))
	    	foreach ($attr as $key => $value)
				$img_html .= ' '.$key.'="'.$value.'"';

		$img_html .= '>';
	}

	echo $img_html;
}




/*
 * @param int $id Image attachment ID
 * @param string $size_name Name of custom image size as added with add_image_size()
 * return bool True if intermediate image exists or was created. False if failed to create.
 */

// function yo_maybe_create_missing_intermediate_images($id, $size_name) {

// 	$sizes = image_get_intermediate_size($id, $size_name);

// 	if (!$sizes) { //if size doesn't exist for given image

// 		require_once(ABSPATH . 'wp-admin/includes/image.php');

// 		// echo "new thumb need for id $id<br />";

// 		$upload_dir = wp_upload_dir();
// 		$image_path = str_replace($upload_dir['baseurl'], $upload_dir['basedir'], wp_get_attachment_url($id));
// 		$new = wp_generate_attachment_metadata($id, $image_path);
// 		wp_update_attachment_metadata($id, $new);

// 		if (image_get_intermediate_size($id, $size_name)) {
// 			// echo "new image size created for id $id<br />";
// 			return true;
// 		}
// 		else {
// 			// echo "failed to create new image size for id $id<br />";
// 			return false;
// 		}

// 	} else {
// 		// echo 'already exists<br />';
// 		return true;
// 	}

// }




/*
 *------------------------------------------------------------------------------------------------
 * Debug
 *------------------------------------------------------------------------------------------------
 *
 */

global $yo_debug_stack;
$yo_debug_stack = array();

function yo_debug($var, $label = '')
{
	global $yo_debug_stack;

	$debug  = '<div>';
	$debug .= '<h3>'.$label.'</h3>';
	$debug .= '<pre>';
	ob_start();
	print_r($var);
	$var_content = ob_get_contents();
	ob_end_clean();
	$debug .= htmlspecialchars($var_content, ENT_QUOTES);
	$debug .= '</pre>';
	// $debug .= '<small>Called by: ' . YO_Fields::get_caller_class() . '</small>';
	$debug .= '</div><br><br><br>';

	$yo_debug_stack[] = $debug;
}


function yo_print_debug()
{
	global $yo_debug_stack;
	foreach ($yo_debug_stack as $debug)
		echo '<div style="background:#fff;border-top:1px solid #333; padding:30px;">'.$debug.'</div>';
}
add_action('admin_footer', 'yo_print_debug');