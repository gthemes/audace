<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists('YO_Checkbox_List_Field')) 
{
	class YO_Checkbox_List_Field 
	{
		/**
		 * Get field HTML
		 *
		 * @param string $output
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($output, $data, $field) 
		{
			$data 			= (array) $data;
			$std      	  	= (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			$output = array();

			foreach ($field['options'] as $key => $value) 
			{
				$checked = checked( in_array( $key, $data ), true, false );
				$name = "name='{$field['field_name']}'";
				$val     = " value='{$key}'";
				$output[]  = "<label><input type='checkbox' class='yo-checkbox-list'{$name}{$val}{$checked} /> {$value}</label>";
			}

			return implode('<br>', $output);
		}
	}
}