<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Radio_Field' ) ) 
{
	class YO_Radio_Field 
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data, $field)
		{
			$std      	  	= (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			$toggle_items 	= (isset($field['toggle'])) ? explode('|', $field['toggle']) : '';

			$html = '';
			$counter = 0;

			// echo $data;

			foreach ($field['options'] as $key => $value) 
			{
				$toggle   = (isset($toggle_items[$counter])) ? ' data-toggle="'.$toggle_items[$counter].'"' : '';
				$checked  = checked($data, $key, false);
				$selected = ($checked) ? ' yo-radio-img-selected' : '';
				$id_only  = strstr($field['id'], '[]') ? str_replace('[]', "-{$key}[]", $field['id']) : $field['id'];
				$id		  = " id='{$id_only}'";
				$name 	  = "name='{$field['field_name']}'";
				$val      = " value='{$key}'";
				$mod	  = (isset($field['mod'])) ? $field['mod'] : false;

				switch ($mod)
				{
					case 'images':
						$html .= '<span>';
						// $html .= '<input type="radio" class="checkbox yo-radio-img-radio" value="'.$key.'" name="'.$field['id'].'" '.$checked.' />';
						$html .= "<input type='radio' class='hidden'{$name}{$id}{$val}{$toggle}{$checked} />";
						// $html .= '<div class="yo-radio-img-label">'. $key .'</div>';
						$html .= "<img src='{$value}' alt='' class='yo-radio-img{$selected}' />";
						// $html .= '<img src="'.$value.'" alt="" class="yo-radio-img-img '. $selected .'" onClick="document.getElementById(\'yo-radio-img-'. $field['id'] . $i.'\').checked = true;" />';
						$html .= '</span>';		
						break;

					case false:
					default:
						$html   .= "<label><input type='radio'{$name}{$id}{$val}{$toggle}{$checked} /> {$value}</label> ";
						break;
				}
				
				$counter++;
			}

			return $html;
		}
	}
}