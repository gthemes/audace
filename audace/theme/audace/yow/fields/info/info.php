<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists('YO_Info_Field'))
{
	class YO_Info_Field
	{
		/**
		 * Show begin HTML markup for fields
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function begin_html( $html, $data, $field )
		{
			return '<div>';
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data, $field)
		{	
			$name = isset($field['name']) ? '<h3>'.$field['name'].'</h3>' : '';
			$desc = isset($field['desc']) ? '<p>'.$field['desc'].'</p>' : '';

			$html = $name.$desc;

			return $html;
		}

	}
}