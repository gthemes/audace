jQuery(document).ready(function($){

	function waitForWebfonts(fonts, callback) {
	    var loadedFonts = 0;
	    for(var i = 0, l = fonts.length; i < l; ++i) {
	        (function(font) {
	            var node = document.createElement('span');
	            // Characters that vary significantly among different fonts
	            node.innerHTML = 'giItT1WQy@!-/#^';
	            // Visible - so we can measure it - but not on the screen
	            node.style.position      = 'absolute';
	            node.style.left          = '-10000px';
	            node.style.top           = '-10000px';
	            // Large font size makes even subtle changes obvious
	            node.style.fontSize      = '300px';
	            // Reset any font properties
	            node.style.fontFamily    = 'sans-serif';
	            node.style.fontVariant   = 'normal';
	            node.style.fontStyle     = 'normal';
	            node.style.fontWeight    = 'normal';
	            node.style.letterSpacing = '0';
	            document.body.appendChild(node);

	            // Remember width with no applied web font
	            var width = node.offsetWidth;

	            node.style.fontFamily = '"'+font+'"';

	            var interval, timeout;
	            function checkFont() {
	                // Compare current width with original width
	                if(node && node.offsetWidth != width) {
	                    ++loadedFonts;
	                    node.parentNode.removeChild(node);
	                    node = null;
	                }

	                // If all fonts have been loaded
	                if(loadedFonts >= fonts.length) {
	                    if(interval) {
	                        clearInterval(interval);
	                    }
	                    if (timeout) {
	                        clearTimeout(timeout);
	                    }
	                    if(loadedFonts == fonts.length) {
	                        callback();
	                        return true;
	                    }
	                }
	            };

	            if(!checkFont()) {
	                interval = setInterval(checkFont, 50);
		            timeout = setTimeout(callback, 5000);
	            }

	        })(fonts[i]);
	    }
	};

	function render_preview(font_family, font_variation, preview) {
		var font_weight = parseInt(font_variation),
			font_style = 'regular';
		if ( ! font_weight) font_weight = 400;
		if (font_variation.indexOf("italic") != -1) font_style = 'italic'; 

		preview.css('font-family', font_family);
		preview.css('font-weight', font_weight);
		preview.css('font-style', '');
		preview.css('font-style', font_style);
	}

	$('.yo-google_fonts select').change(function() {
		var font = $(this).val(),
			font_details = font.split(':'),
			font_family = font_details[0].replace('+', ' ');
			font_variation = font_details[1];
			preview = $('.yo-fonts-preview', $(this).parent('.yo-control'));

		if(preview.is(':visible')) {
			preview.fadeOut(50);
		}

		var standard_fonts = ['Arial', 'Courier', 'Georgia', 'Lucida', 'Times New Roman', 'Verdana'];

		if (jQuery.inArray(font, standard_fonts) != -1) {
			render_preview(font, 'regular', preview);
			preview.fadeIn(500);
		} else {
			// Load CSS
			$('head link[data-font="'+$(this).attr('id')+'"]').remove();
			$('head').append('<link href="http://fonts.googleapis.com/css?family='+font+'" rel="stylesheet" type="text/css" data-font="'+$(this).attr('id')+'">');

			waitForWebfonts([font_family], function() {
			    render_preview(font_family, font_variation, preview);
				preview.fadeIn(500);
			});
		}
	});

	$('.yo-google_fonts select').trigger('change');
});