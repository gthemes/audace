<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists('YO_Group_Field'))
{
	class YO_Group_Field
	{
		public static function add_actions()
		{
			// Filter when retrieving the data
			// add_filter('yo_group_data', array(__CLASS__, 'filter_data'), 10, 2);
		}


		/**
		 * Get field HTML
		 *
		 * @param int    $counter
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($counter, $data, $field)
		{
			$output = '';

			// yo_debug($data, $field['id']);


			foreach ($field['fields'] as &$grouped_field)
			{
				// Set a unique ID for the field tag
				// $grouped_field['field_id'] = $grouped_field['id'].$counter;

				if (!empty($counter))
					$grouped_field['field_name'] = str_replace('[0]', '['.$counter.']', $grouped_field['field_name']);

				if (!empty($data))
				{
					$temp_data = (isset($data[$grouped_field['id']])) ? $data[$grouped_field['id']] : '';

					// Look for the value in $data[group_name][field_id]
					if (empty($temp_data))
						$temp_data = (isset($data[$grouped_field['group']][$grouped_field['id']])) ? $data[$grouped_field['group']][$grouped_field['id']] : '';

					// Nothing found, try $data[field_id] or leave it empty
					// if (empty($temp_data))
						
					// if (empty($temp_data))
						// yo_debug($data, $field['id'].' //////////////////////////////////////////////');
						// $temp_data = $data[$field['id']];
				}
				else
				{
					$temp_data = '';
				}



				$output .= YO_Fields::create($grouped_field, $temp_data);
			}

			return $output;
		}


		public static function normalize_field($field)
		{
			foreach ($field['fields'] as &$grouped_field)
			{
				// $grouped_field['field_name'] = $field['id'].'[0]['.$grouped_field['id'].']';
				// $grouped_field['original_id'] = $grouped_field['id'];
				// $grouped_field['id'] = $field['id'].'_'.$grouped_field['id'];
				$grouped_field['group'] = $field['id'];
				// if ($grouped_field['type'] == 'group') $grouped_field['parent'] = $field['id'];

				if ($grouped_field['type'] == 'group')
				{
					$grouped_field = YO_Fields::normalize($grouped_field);
					// $grouped_field['fields'] = YO_Fields::normalize_many($grouped_field['fields']);
				}
				// else
				// {
				if (isset($field['clone']) && $field['clone'] === true)
				{
					// TODO: substitute [0] with a $counter
					$grouped_field['field_name'] = $grouped_field['group'].'[0]['.$grouped_field['id'].']';
				}
				else
				{
					$grouped_field['field_name'] = $grouped_field['group'].'['.$grouped_field['id'].']';
				}
				// }
			}

			// yo_debug($field, $field['id']);

			return $field;
		}

	}
}