<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists('YO_Group_Field'))
{
	class YO_Group_Field
	{
		public static function add_actions()
		{
			// Filter when retrieving the data
			// add_filter( "yo_group_meta", array(__CLASS__, 'filter_meta'), 10, 3);
		}


		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $meta, $field)
		{
			// echo $field['id'] . ' / ' . $field['counter'];
			// $field['field_name'] = $field['group'].'['.$field['counter'].']['.$field['original_id'].']';


			$values = array();

			echo '<br>';
			echo $field['id'];
			echo '<pre>';
			print_r($meta);
			echo '</pre>';
			echo '===================';

			// foreach($meta as $key => $value)
			// 	$values[$key] = $value;

			if ( ! YO_Fields::is_array_empty($meta))
			{
				// echo '<pre>';
				// print_r($meta);
				// echo '</pre>';
				$counter = (isset($field['counter'])) ? '['.$field['counter'].']' : '';

				foreach ($field['fields'] as &$grouped_field)
				{
					// Set the field name in the format: group_id[n][field_id]
					$grouped_field['field_name'] = $grouped_field['group'].$counter.'['.$grouped_field['original_id'].']';

					// Set the field value
					if (isset($meta[$grouped_field['original_id']]))
						$values[$grouped_field['id']] = $meta[$grouped_field['original_id']];

					// if ($grouped_field['type'] == 'group') $html .= self::html($html, $meta, self::normalize_field($grouped_field));
				}
			}


			// echo '<pre>';
			// print_r($field['fields']);
			// echo '</pre>';

			// $html = '';

			$fields = new YO_Fields($field['fields']);
			$html = $fields->generate($values);

			// foreach ($field['fields'] as &$grouped_field)
			// {
			// 	if ($grouped_field['type'] == 'group')
			// 	{
			// 		$values = array();

			// 		foreach ($grouped_field['fields'] as &$nested_field)
			// 		{
			// 			if (isset($meta[$nested_field['original_id']]))
			// 				$values[$nested_field['id']] = $meta[$nested_field['original_id']];					
			// 			// $html .= self::html($html, $meta, self::normalize_field($grouped_field));
			// 		}

			// 		$fields = new YO_Fields($grouped_field['fields']);
			// 		$html .= $fields->generate($values);
			// 	}

			// }

			// echo '<pre>';
			// print_r($values);
			// echo '</pre>';
			// echo '===================';

			// $i = 0;
			// foreach ($field['fields'] as &$grouped_field)
			// {
			// 	if (isset($meta[$i]))
			// 	{
			// 		$values[$grouped_field['id']] = $meta[$i];
			// 		$i++;
			// 	}
			// }

			// $html = self::recursive_html($field['fields'], $values);

			return $html;
		}


		public static function recursive_html($fields, $values)
		{
			$html = '';

			$output = new YO_Fields($fields);
			$html .= $output->generate($values);

			foreach ($fields as $field)
			{
				if ($field['type'] == 'group')
					$html .= self::recursive_html($field['fields'], $values);
			}
			
			return $html;
		}


		public static function normalize_field($field)
		{
			// This is a dirty trick to show the group fields if there aren't values set.
			// This is only necessary for clonable groups.
			// if ( empty($field['std']))
			// {
			// 	$empty_std = array_fill(0, count($field['fields']), '');
			// 	$field['std'] = $empty_std;
			// }

			foreach ($field['fields'] as &$grouped_field)
			{
				// if ( ! isset($field['std']))
				// 	$empty_std[] = ' ';

				// $grouped_field['multiple'] = true;
				// $grouped_field['id'] = $field['id'].'_'.$grouped_field['id'];
				$grouped_field['field_name'] = $field['id'].'[0]['.$grouped_field['id'].']';
				$grouped_field['original_id'] = $grouped_field['id'];
				$grouped_field['id'] = $field['id'].'_'.$grouped_field['id'];
				$grouped_field['group'] = $field['id'];
				if ($grouped_field['type'] == 'group') $grouped_field['parent'] = $field['id'];

				// $grouped_field['multiple'] = true;
			}

			// $field['multiple'] = true;

			// $field['id'] = $field['id'].'_'.$grouped_field['id'];
			// $field['field_name'] = $field['id'].'[]['.$grouped_field['id'].']';

			// echo '<pre>';
			// print_r($field);
			// echo '</pre>';

			return $field;
		}

	}
}