<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Text_Field' ) )
{
	class YO_Text_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data, $field)
		{	
			$std      = (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;		
			$name     = " name='{$field['field_name']}'";
			// $id       = isset($field['clone']) && $field['clone'] ? '' : " id='{$field['id']}'";
			$id       = (isset($field['field_id'])) ? " id='{$field['field_id']}'" : " id='{$field['id']}'";
			$val      = " value='{$data}'";
			$size     = isset($field['size']) ? $field['size'] : '30';
			

			$html    = "<input type='text' {$name}{$id}{$val} size='{$size}' />";

			return $html;
		}
	}
}