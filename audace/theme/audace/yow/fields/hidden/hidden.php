<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Hidden_Field' ) )
{
	class YO_Hidden_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $output
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($output, $data, $field)
		{
			$std   = (isset($field['std'])) ? $field['std'] : '';
			$val   = " value='{$std}'";
			// $name  = " name='{$field['id']}'";
			$name  = " name='{$field['field_name']}'";
			// $id    = " id='{$field['id']}'";
			$id    = (isset($field['field_id'])) ? " id='{$field['field_id']}'" : " id='{$field['id']}'";
			$output .= "<input type='hidden' {$name}{$id}{$val} />";

			return $output;
		}
	}
}