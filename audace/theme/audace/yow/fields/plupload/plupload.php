<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Plupload_Field' ) )
{
	// Enqueue scripts if the plupload is applied on a field and not defined specifically
	add_action("normalize_group_field", array('YO_Plupload_Field', 'normalize'), 10, 1);

	// Add a filter outside the class to intercept group fields
	add_filter("yo_group_control", array('YO_Plupload_Field', 'control_filter'), 10, 3);


	class YO_Plupload_Field
	{
		public static $instances = array();
		public static $templates = array();

		static function add_actions()
		{
			// add_action('wp_ajax_plupload', array('YO_Plupload_Field', 'upload'), 0);
			add_action('wp_ajax_plupload', array(__CLASS__, 'upload'), 0);
			// add_filter( "yo_plupload_end_html", array(__CLASS__, '_end_html'), 10, 3);
		}


		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			// CSS
			wp_enqueue_style('yo-plupload', YO_FIELDS_URL.'plupload/plupload.css', false, YO_VER);
			// wp_enqueue_style('yo-popbox', YO_FIELDS_URL.'plupload/popbox.css', false, YO_VER);

			// Javascript
			wp_enqueue_script('plupload-all');
			// wp_register_script('yo-popbox', YO_FIELDS_URL.'plupload/popbox.js', array('jquery'), YO_VER);
			wp_register_script('yo-plupload', YO_FIELDS_URL.'plupload/plupload.js', array('jquery'), YO_VER);
		}


		/**
		 * Check enqueue scripts and styles if the plupload is applied on a field
		 *
		 * @return void
		 */
		static function normalize($field)
		{
			if (isset($field['plupload']) && $field['plupload'] != false)
			{
				// Make sure the actions are set
				self::add_actions();
			}
		}


		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data, $field)
		{
			$name     = " name='{$field['field_name']}'";
			$id       = isset($field['clone']) && $field['clone'] ? '' : " id='{$field['id']}'";
			$val      = " value='{$data}'";
			$std      = isset($field['disabled']) ? $field['disabled'] : false;
			$disabled = disabled($std, true, false);
			$thumb 	  = (isset($field['thumb'])) ? $field['thumb'] : 'thumbnail';
			$mod 	  = (isset($field['mod'])) ? $field['mod'] : false;
			$extensions = (isset($field['extensions'])) ? $field['extensions'] : '*';

			if ($mod === 'receiver')
			{
				
			}


			$html  = '<div id="plupload-upload-ui-'.$field['id'].'" class="hide-if-no-js">';

			$html .= '<div id="drag-drop-area-'.$field['id'].'" class="yo-drag-drop-area">';

			$html .= '
		       <div class="drag-drop-inside">
		       	<div class="yo-dropped-items"></div>
		        <p class="drag-drop-info">'.__('Drop files here', 'theme_admin').'</p>
		        <p>'._x('or', 'Uploader: Drop files here - or - Select Files', 'theme_admin').'</p>
		        <p class="drag-drop-buttons">
		        	<input id="plupload-browse-button-'.$field['id'].'" type="button" value="'.esc_attr__('Select Files', 'theme_admin').'" class="button" />
		        	<input id="plupload-library-button-'.$field['id'].'" type="button" value="'.esc_attr__('Browse Library', 'theme_admin').'" class="button" />
		        </p>
		      </div>
		    </div>';

			$html .= '</div>';

			self::droparea($field['id'], false, $extensions);

			return $html;
		}

		static function droparea($field_id, $receiver, $extensions = '*')
		{
			$plupload_init = array(
			    'runtimes'            => 'html5,silverlight,flash,html4',
			    'unique_id'			  => $field_id,
			    'browse_button'       => 'plupload-browse-button-'.$field_id,
			    'container'           => 'plupload-upload-ui-'.$field_id,
			    'drop_element'        => 'drag-drop-area-'.$field_id,
			    'dropped_items'    	  => '.yo-dropped-items',
			    'receiver'    	  	  => $receiver,
			    'file_data_name'      => 'async-upload',
			    'multiple_queues'     => true,
			    'max_file_size'       => wp_max_upload_size().'b',
			    'url'                 => admin_url('admin-ajax.php'),
			    'flash_swf_url'       => includes_url('js/plupload/plupload.flash.swf'),
			    'silverlight_xap_url' => includes_url('js/plupload/plupload.silverlight.xap'),
			    'filters'             => array(array('title' => __('Allowed Files', 'theme_admin'), 'extensions' => $extensions)),
			    'multipart'           => true,
			    'urlstream_upload'    => true,

			    // additional post data to send to our ajax hook
			    'multipart_params'    => array(
			      '_ajax_nonce' => wp_create_nonce('plupload'),
			      'action'      => 'plupload',  // the ajax action name
			    ),
			);
			
			wp_enqueue_script('yo-plupload');

			// Pass the settings using a Javascript object
			// TODO: Fix duplicated variable in the HTML... don't localizeeach time, wait for the last field
			self::$instances[$field_id] = json_encode($plupload_init);
			wp_localize_script('yo-plupload', 'yo_plupload', self::$instances);
		}


		static function upload()
		{
			// global $post;

		    check_ajax_referer('plupload');

		    $file = $_FILES['async-upload'];

		    $upload = wp_handle_upload($file, array('test_form'=>false, 'action' => 'plupload'));

		    if (empty($upload['error']))
		    {
				$upload['id'] = wp_insert_attachment(array(
				                'post_mime_type' => $upload['type'],
				                'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['name'])),
				                'post_content' => '',
				                'post_status' => 'inherit',
				                // 'post_type' => 'attachment',
				                // 'post_parent' => $post->ID,
				                ), $upload['file']);

				// $thumb = image_resize($upload['file'], 100, 100, true);

				// if (is_wp_error($thumb)) exit;

				// $uploads = wp_upload_dir();
				// $upload['thumb'] = $uploads['url'].'/'.basename($thumb);

				// yo_resize_only('thumbnail');

				// TODO: Move the attachment update to yo_resize_only()
				// TODO: Rename yo_resize_only() to yo_force_resize($upload['id'], $upload['file'], array('formats'))
				$attach_data = wp_generate_attachment_metadata($upload['id'], $upload['file']);
				wp_update_attachment_metadata($upload['id'], $attach_data);

				// wp_generate_attachment_metadata($upload['id'], $upload['file']);

				$thumb = wp_get_attachment_image_src($upload['id'], 'thumbnail');
				$upload['thumb'] = $thumb[0];


				unset($upload['file']);

				// Create an Attachment in WordPress
				// $id = wp_insert_attachment($status['id'], $status[ 'file' ], $post->ID);
				// wp_update_attachment_metadata($id, wp_generate_attachment_metadata($id, $status['file']));
				// update_post_meta($post->ID, 'async-up5load', $id);

				header('Content-Type: application/json');
				echo json_encode($upload);
		    }

		    exit;
		}

		static function control_filter($output, $field, $data)
		{
			// Make sure the scripts are loaded
			self::admin_enqueue_scripts();

			// do_action('admin_enqueue_scripts');

			if (isset($field['plupload']) && $field['plupload'] != false)
			{
				// The ID of the field that will 'receive' the thumbnail
				$receiver = (isset($field['receiver'])) ? $field['receiver'] : false;
				// The allowed file extensions (separated by comma)
				$extensions = (isset($field['extensions'])) ? $field['extensions'] : '*';

				if ($receiver === false && $field['type'] === 'group')
					foreach ($field['fields'] as &$grouped_field)
						if (isset($grouped_field['receiver']))
						{
							$receiver = $grouped_field['id'];
							break;
						}		

				switch ($field['plupload'])
				{
					default:
						$output = '<div class="yo-dropped-items">'.$output.'</div>';
						$output = $output.'<p class="drag-drop-info">'.__('Drop files here', 'theme_admin').'</p><p>'._x('or', 'Uploader: Drop files here - or - Select Files', 'theme_admin').'</p>
		        		<p class="drag-drop-buttons"><input id="plupload-browse-button-'.$field['id'].'" type="button" value="'.esc_attr__('Select Files', 'theme_admin').'" class="button" /></p>';
						// $output = YO_Fields::wrap($output, false, 'yo-dropped-items');
						$output = YO_Fields::wrap($output, false, 'drag-drop-inside');
						$output = YO_Fields::wrap($output, 'drag-drop-area-'.$field['id'], 'yo-drag-drop-area');
						$output = YO_Fields::wrap($output, 'plupload-upload-ui-'.$field['id'], 'hide-if-no-js');
						break;
				}
				// yo_debug($output, 'Output');
				// yo_debug($data, 'Data');
				// yo_debug($field, 'Field');

				self::droparea($field['id'], $receiver, $extensions);
			}

			return $output;
		}
	}	
}