jQuery(document).ready(function($){

  // console.log(yo_plupload.init);

  // create the uploader and pass the config from above
  // var uploader = new plupload.Uploader($.parseJSON(yo_plupload.init));

  var $yo_hovered_clone = false;
  
  //var uploaders = $.parseJSON(yo_plupload[]);

  //
  // Recursively find an available ID
  //
  // TODO: Also substitute IDs for input, label for, etc...
  function set_new_id(id, clone)
  {
    if (id == null) id = 0;

    $('input[name], select[name]', clone).each(function() {
      this.name = this.name.replace(/\[(\d+)\]/, function(str, p1){
                return '[' + id + ']';
            });
    });

    $('input[name], select[name]', clone.siblings('.yo-clone')).each(function()
    {
      if ($('input[name="'+this.name+'"], select[name="'+this.name+'"]', clone).length > 0)
      {
        id++;
        set_new_id(id, clone);
        return false;
      }
    });

    clone.find(':input').val('');
    $('.yo-media-item', clone).remove();
  }

  // This block is repeated for each plupload instance
  for (var instance in yo_plupload)
  {
    // console.log(yo_plupload[instance]);
    
    var uploader = new plupload.Uploader($.parseJSON(yo_plupload[instance]));

    // console.log(uploader);

    // checks if browser supports drag and drop upload, makes some css adjustments if necessary
    uploader.bind('Init', function(up){

      // console.log(up);

      var uploaddiv = $('#'+up.settings.container);

      // console.log(up.settings.container);

      if(up.features.dragdrop){
          uploaddiv.addClass('drag-drop');
          $('#'+up.settings.drop_element)
            .bind('dragover.wp-uploader', function(){ uploaddiv.addClass('drag-over'); })
            .bind('dragleave.wp-uploader, drop.wp-uploader', function(){ uploaddiv.removeClass('drag-over'); });

          // $('.yo-dropped-items .yo-clone')
          //   .bind('dragover.wp-uploader', function(){ $(this).addClass('yo-thumb-over'); })
          //   .bind('dragleave.wp-uploader', function(){ $(this).removeClass('yo-thumb-over'); });

      } else {
        uploaddiv.removeClass('drag-drop');
        $('#'+up.settings.drop_element).unbind('.wp-uploader');
      }
    });

    uploader.init();

    // a file was added in the queue
    uploader.bind('FilesAdded', function(up, files){
    // uploader.bind('QueueChanged', function(up, files){
      var hundredmb = 100 * 1024 * 1024, max = parseInt(up.settings.max_file_size, 10);
      // console.log(files);
      plupload.each(files, function(file){
        if (max > hundredmb && file.size > hundredmb && up.runtime != 'html5'){
          // file size error?
        } else {
          // a file was added, you may want to update your DOM here...
          console.log(up);
          console.log(files);
          $('#yo-section-'+up.settings.unique_id+' > .yo-label').append('<div class="media-item"><div class="progress"><div class="percent">100%</div><div class="bar" style="width: 200px; "></div></div></div>');
        }
      });

      up.refresh();
      up.start();
    });

    // uploader.bind('QueueChanged', function (up, files) {
    //    uploader.start();
    //    up.refresh();
    // });

    // Progress bar ////////////////////////////////////////////
    // Add the progress bar when the upload starts
    // Append the tooltip with the current percentage
    uploader.bind('UploadProgress', function(up, file) {
        var progressBarValue = up.total.percent;
        // console.log('loading: '+progressBarValue);
        console.log(up.settings.unique_id);
        // <div class="progress"><div class="percent">100%</div><div class="bar"></div></div>        
        $('#yo-section-'+up.settings.unique_id+' .bar').css('width', progressBarValue+'%');

        // $('#progressbar').fadeIn().progressbar({
        //     value: progressBarValue
        // });
        // $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
    });

    // a file was uploaded 
    uploader.bind('FileUploaded', function(up, file, response) {

      // console.log(up.settings.unique_id);
      // console.log(response['response']);
      $('#yo-section-'+up.settings.unique_id).remove();  

      response = $.parseJSON(response['response']);
      
      // console.log('xxxxxxxxxx');

      if (response)
      {
        var $clone;

        if ($yo_hovered_clone)
        {
          // $yo_hovered_clone.append('teeeeeeeeeeeeeeeeeest');
          $clone = $yo_hovered_clone;
          $yo_hovered_clone = false;
          $clone.removeClass('yo-thumb-over');
        }
        else
        {
          var template = window[up.settings.unique_id];
          $(up.settings.dropped_items, '#'+up.settings.container).append(template);

          $clone = $(up.settings.dropped_items + ' > .yo-clone:last-child', '#'+up.settings.container);

          $clone.hide().fadeIn(500);

          // Set the new ID
          var id = $($clone).siblings('.yo-clone').length;

          // console.log('New ID: ' + id);
          set_new_id(id, $clone);
        }



        // $(".yo-dropped-items input[name='"+up.settings.receiver+"']", '#'+up.settings.container).append('<img src="'+response.thumb+'">');
        
        // if ($yo_hovered_clone)
        // {
        //   // $yo_hovered_clone.append('teeeeeeeeeeeeeeeeeest');
        //   $clone = $yo_hovered_clone;
        //   $yo_hovered_clone = false;
        // }


        var upload_field = $('input[name*="['+up.settings.receiver+']"]', $clone);
        // var upload_field = $('input[name="'+up.settings.receiver+'"]', $('#'+up.settings.container));

        

        // console.log(upload_field);

        // console.log($yo_hovered_clone);

        
        $('.yo-media-item', $clone).remove();
        $(upload_field).parent().append('<img src="'+response.thumb+'" class="yo-media-item" />');
        $(upload_field).val(response.id);

        // $('input[name=inputName\\[\\]][value=someValue]')
        
        // $('.yo-dropped-items', '#'+up.settings.container).append('<img src="'+response.thumb+'">');
        // $('.yo-dropped-items', '#'+up.settings.container).append('<input type="text" class="hide" name="'+up.settings.unique_id+'['+response.id+'][media_item]" id="'+up.settings.unique_id+'_media_item" value="'+response.id+'">');
      }
      else
      {
        console.log('Upload Error!');
      }
    });

  } // End for loop


  

  $('.yo-dropped-items').delegate('.yo-delete-thumb', 'click', function(e) {
  // $(".yo-dropped-items").on("click", ".yo-delete-thumb", function() {
      e.preventDefault();
      $(this).parents('.yo-clone').fadeOut(300, function() { $(this).remove(); });
  });

  $('.yo-dropped-items').delegate('.yo-edit-thumb', 'click', function(e) {
      e.preventDefault();
      var $tooltip_items = $('.yo-tooltip-item', $(this).parents('.yo-clone')),
          $tooltip_wrapper = $('.yo-tooltip', $(this).parents('.yo-clone'));

      if (!$tooltip_wrapper.length)
      {
          $tooltip_items.wrapAll('<div class="yo-tooltip"></div>');
          $tooltip_wrapper = $('.yo-tooltip', $(this).parents('.yo-clone'));
          $tooltip_wrapper.hide();
          $tooltip_items.show();
          $tooltip_wrapper.append('<a href="#" class="yo-button button">Close</a>');
          $('.yo-button', $tooltip_wrapper).click(function(e){
            e.preventDefault();
            $tooltip_wrapper.fadeOut(300);
          });

          $tooltip_wrapper.css('z-index', 2500);
      }

      $(document).bind('click', function(event){
        if(!$(event.target).closest('.yo-clone').length){
          $tooltip_wrapper.fadeOut(300);
          $(document).unbind('click');
        }
      });

      if ($tooltip_wrapper.is(':visible')) {
        $tooltip_wrapper.fadeOut(300);
      } else {
        $('.yo-clone', $(this).parents('.yo-dropped-items')).css('z-index', 1);
        $('.yo-tooltip', $(this).parents('.yo-dropped-items')).fadeOut(300);
        $tooltip_wrapper.parents('.yo-clone').css('z-index', 2500);
        $tooltip_wrapper.fadeIn(300);
      }
  });


$('.yo-dropped-items').delegate('.yo-clone', 'dragover.wp-uploader', function(e) {
    $yo_hovered_clone = $(this);
    $(this).addClass('yo-thumb-over');
});

$('.yo-dropped-items').delegate('.yo-clone', 'dragleave.wp-uploader', function(e) {
    $yo_hovered_clone = false;
    $(this).removeClass('yo-thumb-over');
});


});