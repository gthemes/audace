<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Select_Field' ) )
{
	class YO_Select_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts($field)
		{
			if (isset($field['mod']) && $field['mod'] === 'chosen')
			{
				wp_enqueue_style('yo-chosen', YO_FIELDS_URL.'select/chosen.css', false, YO_VER);
				wp_enqueue_script('yo-chosen', YO_FIELDS_URL.'select/chosen.js', array('jquery'), YO_VER);
				yo_jquery('$(".yo-chosen select").chosen();');
			}
		}

		/**
		 * Get field HTML
		 *
		 * @param string $output
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($output, $data, $field)
		{
			// Field parameters
			$std      	  	= (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			$data 		= (array) $data;
			// yo_debug($data, $field['id']);
			$id       	= (isset($field['field_id'])) ? " id='{$field['field_id']}'" : " id='{$field['id']}'";
			$name	 	= " name='{$field['field_name']}'";
			$multiple	= (isset($field['multiple']) && $field['multiple'] === true) ? ' multiple' : '';
			// if (!empty($multiple))

			// Field output
			$output	 = "<select {$name}{$id}{$multiple}>";
			foreach ($field['options'] as $key => $value)
			{
				$disabled 	 = ($key == '{disabled}') ? ' disabled="disabled"' : '';
				$selected	 = selected(in_array($key, $data), true, false);
				$output		.= "<option value='{$key}'{$selected}{$disabled}>{$value}</option>";
			}
			$output	.= "</select>";

			return $output;
		}
	}
}