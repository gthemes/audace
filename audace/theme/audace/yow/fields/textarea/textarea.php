<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Textarea_Field' ) ) 
{
	class YO_Textarea_Field 
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data, $field) 
		{
			$std     = (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;

			$cols	 = isset($field['cols']) ? $field['cols'] : "60";
			$rows	 = isset($field['rows']) ? $field['rows'] : "10";
			$name	 = "name='{$field['field_name']}'";
			$id		 = " id='{$field['id']}'";
			
			$html	.= "<textarea {$name}{$id} cols='{$cols}' rows='{$rows}'>{$data}</textarea>";

			return $html;
		}
	}
}