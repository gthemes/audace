<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Image_Field' ) )
{
	class YO_Image_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $output
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($output, $data, $field)
		{
			$std   = (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			$id    = (isset($field['field_id'])) ? " id='{$field['field_id']}'" : " id='{$field['id']}'";
			$name  = " name='{$field['field_name']}'";
			$val   = " value='{$data}'";

			$output .= "<input type='hidden' {$name}{$id}{$val} />";

			if (!empty($data))
			{
				$image_src = wp_get_attachment_image_src($data, 'thumbnail');
				$output .= '<img src="'.$image_src[0].'" class="yo-media-item" />';
			}

			if (isset($field['bar']))
			{
				$output .= '<div class="yo-thumbs-bar">';
				// $output .= '<li class=""><a href="#" class="yo-button button yo-edit-thumb">'.__('Details', 'theme_admin').'</a><li>';
				if ((isset($field['edit']) && $field['edit'] === true) || !isset($field['edit']))
					$output .= '<a href="#" class="yo-button button small yo-edit-thumb"><img src="'.YO_IMG_URL.'icon-edit2.png" /></a>';
				if ((isset($field['delete']) && $field['delete'] === true) || !isset($field['delete']))
					$output .= '<a href="#" class="yo-button button small yo-delete-thumb"><img src="'.YO_IMG_URL.'icon-delete2.png" /></a>';
				// $output .= '<li><a href="#" class="yo-button button yo-delete-thumb">'.__('Delete', 'theme_admin').'</a><li>';
				$output .= '</div>';
			}
				// $output .= '<div class="yo-thumbs-bar"><a href="#" class="yo-edit-thumb">'.__('Details', 'theme_admin').'</a> | <a href="#" class="yo-delete-thumb">'.__('Delete', 'theme_admin').'</a></div>';

			return $output;
		}
	}
}