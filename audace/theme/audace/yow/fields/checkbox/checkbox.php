<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Checkbox_Field' ) ) 
{
	class YO_Checkbox_Field 
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($html, $data = false, $field)
		{
			$std      	  	= (isset($field['std'])) ? $field['std'] : '';
			// var_dump($data);
			// if (empty($data) && $data !== false && $data !== true) $data = $std;
			$checked = checked( ! empty( $data ), true, false );
			// echo $checked;
			$name    = "name='{$field['field_name']}'";
			$id      = " id='{$field['id']}'";
			$toggle  = (isset($field['toggle'])) ? ' data-toggle="'.$field['toggle'].'"' : '';

			$html    = "<input type='checkbox' class='yo-checkbox'{$name}{$id}{$toggle}{$checked} />";

			return $html;
		}

		/**
		 * Set the value of checkbox to 1 or 0 instead of 'checked' and empty string
		 * This prevents using default value once the checkbox has been unchecked
		 *
		 * @link https://github.com/rilwis/meta-box/issues/6
		 *
		 * @param mixed $new
		 * @param mixed $old
		 * @param int   $post_id
		 * @param array $field
		 *
		 * @return int
		 */
		static function value( $new, $old, $post_id, $field ) 
		{
			return empty( $new ) ? 0 : 1;
		}
	}
}