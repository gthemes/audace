<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Media_Field' ) )
{
	class YO_Media_Field
	{
		static function add_actions()
		{
			// add_action('wp_ajax_plupload', array('YO_Plupload_Field', 'upload'), 0);
			add_action('wp_ajax_get_media', array(__CLASS__, 'get_media'), 0);
			// add_filter( "yo_plupload_end_html", array(__CLASS__, '_end_html'), 10, 3);
		}

		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			// Javascript
			wp_enqueue_script('yo-ajaxupload', YO_FIELDS_URL.'media/ajaxupload.js', array('jquery', 'thickbox'), YO_VER);
			wp_enqueue_script('yo-medialibrary', YO_FIELDS_URL.'media/medialibrary-uploader.js', array('jquery', 'thickbox', 'yo-ajaxupload'), YO_VER);

			wp_localize_script('yo-medialibrary', 'yo_media', array('_ajax_nonce' => wp_create_nonce('medialibrary')));
		}


		/**
		 * Get field HTML
		 *
		 * @param string $output
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html($output, $data, $field)
		{
			$name     = " name='{$field['field_name']}'";
			$id       = isset($field['clone']) && $field['clone'] ? '' : " id='{$field['id']}'";
			$val      = " value='{$data}'";
			$std      = (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			$disabled = disabled( $std, true, false );
			$label 	  = (isset($field['label'])) ? " value='{$field['label']}'" : '';
			$desc 	  = (isset($field['desc'])) ? $field['desc'] : '';
			$mod 	  = (isset($field['mod'])) ? $field['mod'] : 'min';
			$size 	  = (isset($field['size'])) ? $field['size'] : 'thumbnail';
			$force_icon = (isset($field['force_icon'])) ? $field['force_icon'] : false;

			$output = '';
			$hide = '';
			
			if ($mod == "min") { $hide ='hide'; }
		
			$output .= '<div class="yo-media-thumb">';



			if(!empty($data))
			{
				// $file = wp_get_attachment_url($data);
				// echo $file;
				// $thumb = wp_get_attachment_thumb_url($data);

				// $thumb = wp_get_attachment_image_src($data, $thumb);

				$output .= self::get_thumbnail($data, $size, $force_icon);

				// $output .= '<a class="yo-uploaded-image image_'.$field['id'].'" href="'. $data . '">';
		  //   	$output .= '<img src="'.$thumb[0].'" alt="" />';
		  //   	$output .= '</a>';
			}

			$output .= '</div>';


			// This field stores the media post ID
			$output  .= "<input type='text' class='{$hide}'{$name}{$id}{$val} />";

			// This stores the media attachment URL
			// $name_url = " name='{$field['field_name']}";
			// $output .= "<input type='hidden'{$name_url}{$val} />";
			
			// The upload button
			$output .= '<div class="yo-media-buttons"><span class="button yo-upload-media '.$field['id'].'_upload">'.__('Insert File', 'theme_admin').'</span>';
			
			$hide = (empty($data)) ? 'hide' : '';

			$output .= '<span class="button yo-remove-media reset_'. $field['id'] .' '. $hide.'">'.__('Remove', 'theme_admin').'</span>';
			$output .='</div>' . "\n";

			return $output;
		}

		static function get_thumbnail($id, $size = 'thumbnail', $force_icon = false)
		{
			$mimetype = self::get_mimetype($id);

			if ($mimetype['image'] === true && $force_icon === false)
			{
				$thumb = wp_get_attachment_thumb_url($id, $size);
			}
			else
			{
				$thumb = $mimetype['icon'];
			}
			
			$file = wp_get_attachment_url($id);

			$output  = '<a class="yo-uploaded-image" href="'. $file . '" target="_blank">';
			$output .= '<img src="'.$thumb.'" alt="'.basename($file).'" />';
	    	$output .= '</a>';

	    	return $output;
		}

		static function get_mimetype($id)
		{
		  	$type = get_post_mime_type($id);
		  	$image = false;

			switch ($type)
			{
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/png':
				case 'image/gif':
					  $icon = YO_IMG_URL.'media-image.png';
					  $image = true;
					  break;

				case 'audio/mpeg':
				case 'audio/mp4': 
				case 'audio/ogg': 
				case 'audio/webm': 
				case 'audio/wav': 
					  $icon = YO_IMG_URL.'media-audio.png';
					  break;

				case 'video/mpeg':
				case 'video/mp4': 
				case 'video/ogg': 
				case 'video/webm': 
				case 'video/quicktime':
					  $icon = YO_IMG_URL.'media-video.png';
					  break;

				case 'text/csv':
				case 'text/plain': 
				case 'text/xml':
					  $icon = YO_IMG_URL."media-text.png";
					  break;

				default:
					  $icon = YO_IMG_URL."media-file.png";
					  break;
			}

			return array('type' => $type, 'icon' => $icon, 'image' => $image);
		}

		static function get_media()
		{
			// global $post;

		    check_ajax_referer('medialibrary');

		    $id = (int) $_POST['id'];

		    echo self::get_thumbnail($id);

			// $file = wp_get_attachment_metadata($id);

			// if (empty($file))
			// {
			// 	// not an image
			// }
			// else
			// {

			// }

		    // header('Content-Type: application/json');
		    // echo json_encode($file);


		    exit;
		}

	// 	/**
	// 	 * Native media library uploader
	// 	 *
	// 	 * @uses get_option()
	// 	 *
	// 	 * @access public
	// 	 * @since 1.0.0
	// 	 *
	// 	 * @return string
	// 	 */
	// 	public static function media_uploader($data, $id, $name, $std, $int, $mod)
	// 	{
	// 	    // $options_data = get_option(THEME_OPTIONS);
			
	// 		$uploader = '';
	// 	    // $upload = (isset($options_data[$id])) ? $options_data[$id] : '';
	// 		$hide = '';
			
	// 		// print_r($upload);

	// 		if ($mod == "min") {$hide ='hide';}
			
	// 	    if ($data != '') { $val = $data; } else { $val = $std; }
		    
	// 		// $output    .= "<input type='text' class='yo-text'{$name}{$id}{$val}{$disabled} size='{$size}' />";

	// 		$uploader .= '<input type="text" class="'.$hide.' upload of-input" name="'. $id .'" id="'. $id .'_upload" value="'. $val .'" />';	
			
	// 		$uploader .= '<div class="upload_button_div"><span class="button media_upload_button" id="'.$id.'" rel="' . $int . '">Upload</span>';
			
	// 		if(!empty($data)) {$hide = '';} else { $hide = 'hide';}
	// 		$uploader .= '<span class="button mlu_remove_button '. $hide.'" id="reset_'. $id .'" title="' . $id . '">Remove</span>';
	// 		$uploader .='</div>' . "\n";
	// 		$uploader .= '<div class="screenshot">';
	// 		if(!empty($data)){	
	// 	    	$uploader .= '<a class="of-uploaded-image" href="'. $data . '">';
	// 	    	$uploader .= '<img class="of-option-image" id="image_'.$id.'" src="'.$data.'" alt="" />';
	// 	    	$uploader .= '</a>';			
	// 			}
	// 		$uploader .= '</div>';
	// 		$uploader .= '<div class="clear"></div>' . "\n"; 
		
	// 		return $uploader;
			
	// 	}
	}
}