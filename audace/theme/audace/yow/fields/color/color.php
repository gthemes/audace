<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Color_Field' ) )
{
	class YO_Color_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style('yo-color', YO_FIELDS_URL.'color/color.css', false, YO_VER);
			wp_enqueue_script('yo-color', YO_FIELDS_URL.'color/color.js', array('jquery'), YO_VER, true);
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $data, $field )
		{
			$std      	  	= (isset($field['std'])) ? $field['std'] : '';
			if (empty($data) && !empty($std)) $data = $std;
			if (empty($data)) $data = '#';
			$name  = " name='{$field['field_name']}'";
			$id    = isset( $field['clone'] ) && $field['clone'] ? '' : " id='{$field['id']}'";
			$value = " value='{$data}'";

			$html = <<<HTML
<input class="yo-color" type="text"{$name}{$id}{$value} size="28" />
<div class="yo-color-picker"></div>
HTML;

			return $html;
		}
	}
}