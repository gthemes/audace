<?php if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

if ( ! class_exists( 'YO_Password_Field' ) )
{
	class YO_Password_Field
	{
		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $data
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $data, $field )
		{
			$val   = " value='{$data}'";
			$name = "name='{$field['field_name']}'";
			$id    = " id='{$field['id']}'";
			$html .= "<input type='password' {$name}{$id}{$val} size='30' />";

			return $html;
		}
	}
}