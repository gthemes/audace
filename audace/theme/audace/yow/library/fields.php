<?php
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/

// Prevent direct access to this file
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

// Fields Generator Class
if ( ! class_exists('YO_Fields'))
{
	class YO_Fields
	{
		/**
		 * Fields information
		 */
		var $fields;

		/**
		 * Contains all field types of current meta box
		 */
		var $types;


		/**
		 *------------------------------------------------------------------------------------------------
		 * Create the form fields based on the parameters
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function __construct($fields = array())
		{
			if ( ! empty($fields))
			{
				$this->fields = self::normalize_many($fields);
				// unset($fields);
				// foreach ($fields as $field)
				// 	self::create($field, (isset($data[$field['id']])));

				// Enqueue common styles and scripts
				add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
			}
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Enqueue the main CSS/Javascript files
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function admin_enqueue_scripts()
		{
			if ( ! self::can_load_assets()) return;
			
			// Main CSS
			wp_enqueue_style('yo-fields', YO_CSS_URL.'style.css', array('thickbox'), YO_VER);

			// Main Javascript
			wp_enqueue_script('yo-cookie', YO_JS_URL.'cookie.js', array('jquery'), YO_VER);
			wp_enqueue_script('yow', YO_JS_URL.'admin.js', array('jquery'), YO_VER);

			// Enqueue scripts defined in the fields
			self::fields_enqueue_scripts($this->fields);
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Enqueue the fields CSS/Javascript files
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		static function fields_enqueue_scripts($fields)
		{
			$has_clone = false;
			$has_toggle = false;
			$has_sortable = false;

			foreach ($fields as $field)
			{
				if (isset($field['clone']))
					$has_clone = true;

				if (isset($field['toggle']))
					$has_toggle = true;

				if (isset($field['sortable']) && $field['sortable'] === true)
					$has_sortable = true;

				// Enqueue scripts and styles for fields
				$class = self::get_class_name($field['type']);
				if (method_exists($class, 'admin_enqueue_scripts'))
					call_user_func(array($class, 'admin_enqueue_scripts'), $field);
 
				// In case it's a group, also load the scripts for its fields
				if ($field['type'] == 'group')
					self::fields_enqueue_scripts($field['fields']);
			}

			if ($has_clone)
				wp_enqueue_script('yo-clone', YO_JS_URL.'clone.js', array('jquery', 'yow'), YO_VER, true);

			if ($has_toggle)
				wp_enqueue_script('yo-toggles', YO_JS_URL.'toggles.js', array('jquery', 'yow'), YO_VER, true);

			if ($has_sortable) {
				wp_enqueue_script('jquery-ui-core');
				wp_enqueue_script('jquery-ui-sortable');
			}	
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Generate the fields defined in the current instance
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function generate($data = '')
		{
			$output = self::create_many($this->fields, $data);
			return $output;
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Render a single field
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return string $output 	The field's HTML output
		 */
		static function create($field, $data = '', $normalize = false, $force_scripts = false)
		{
			if ($normalize) $field = self::normalize($field);
			if ($force_scripts) self::fields_enqueue_scripts(array($field));

			// Unserialize the data if necessary (WPML fix)
			if (is_serialized($data))
				$data = unserialize($data);
			if (isset($data[0]) && is_serialized($data[0]))
				$data = unserialize($data[0]);

			// Cloneable field
			if (self::is_cloneable($field))
			{
				// Initialize some values
				$data = (array) $data;
				// Used for the group field
				$clones_counter = 0;
				// The output of all the clones together
				$clones_output = '';

				foreach ($data as $key => $clone_data)
				{
					// Set a unique ID for the field tag
					// $field['field_id'] = $field['id'].$clones_counter;
					// Render the field
					$control = self::apply_field_class_filters($field, 'html', $clones_counter, $clone_data);
					// All the controls
					$control = apply_filters("yo_clone", $control, $field, $clone_data);
					// Add the delete_clone button to the field if not disabled
					if ( ! isset($field['clone_button']) OR $field['clone_button'] === true)
						add_filter( "yo_{$field['id']}_html", array(__CLASS__, 'add_delete_clone_button'), 10, 3);
					// Apply filter to field HTML
					// 1st filter applies to all fields with the same type
					// 2nd filter applies to current field only
					// $control = apply_filters("yo_{$type}_html", $control, $field, $clone_data);
					$control = apply_filters("yo_{$field['id']}_html", $control, $field, $clone_data);

					// Wrap the field in a clone div
					$control = self::wrap($control, false, 'yo-clone');
					// Append the current clone to the output
					$clones_output .= $control;
					// Get one rendered field to be used as template, it'll be passed as a Javascript variable
					$field_template = $control;
					// Increase the clones counter
					$clones_counter++;
				}		

				$button = '';
				if (!isset($field['clone_button']) || $field['clone_button'] === true)
				{
					// Add the clone button and set the instances limit
					$max_clones = (isset($field['max'])) ? ' data-max-clones="'.$field['max'].'"' : '';
					// Set the clone button label
					$label = (isset($field['clone_label'])) ? $field['clone_label'] : __('Add New Item', 'theme_admin');
					// Create the clone button
					$button  = '<a href="#" class="yo-button button yo-add-clone" data-clone="'.$field['id'].'"'.$max_clones.'>' . $label . '</a>';
					// $button .= '<a href="#" class="yo-button button-secondary yo-remove-clone" title="'.__('Delete Item', 'theme_admin').'" data-unclone="'.$field['id'].'">&ndash;</a>';
				}

				// Hide the field if the values are empty and the parameter 'empty' is set
				if (self::is_array_empty($data) && isset($field['empty'])) $clones_output = '';
			}
			// Not cloneable field
			else
			{
				// Render the field
				$control = self::apply_field_class_filters($field, 'html', '', $data);
				if (isset($field['export_template']) && $field['export_template'] === true)
					$field_template = $control;
			}

			// Add something before the field
			if (isset($field['before']))
				$control = $field['before'].$control;

			// Add something after the field
			if (isset($field['after']))
				$control = $control.$field['after'];

			// Merge the control and button
			if (isset($button))
				$control = $clones_output.$button;

			// All the controls
			$control = apply_filters("yo_control", $control, $field, $data);
			// All the controls of a specific type
			$control = apply_filters("yo_{$field['type']}_control", $control, $field, $data);
			// Current control only
			$control = apply_filters("yo_{$field['id']}_control", $control, $field, $data);

			// Control wrapper
			if (!empty($control))
				$control = self::wrap($control, false, 'yo-control');

			// CSS classes
			$class = 'yo-field';
			// Default field-type class
			$class = self::add_cssclass('yo-'.$field['type'], $class);
			// Add a specific class to sortable items
			if (isset($field['sortable']))
				$class = self::add_cssclass('yo-sortable', $class);
			// Add CSS classes if set
			if (isset($field['class']))
				$class = self::add_cssclass($field['class'], $class);

			// Label wrapper
			$label_for = (isset($field['field_id'])) ? $field['field_id'] : $field['id'];
			$name = (isset($field['name'])) ? '<label for="'.$label_for.'">'.$field['name'].'</label>' : '';
			$desc = (isset($field['desc'])) ? '<p class="yo-description">'.$field['desc'].'</p>' : '';
			$label = (!empty($name) || !empty($desc)) ? self::wrap($name.$desc, false, 'yo-label') : '';

			// Section wrapper (if the field belongs to a group the section doesn't get an ID)
			// TODO: Change to field_id (with global prefix)
			$section_id = (isset($field['group'])) ? false : 'yo-section-'.$field['id'];
			$output = self::wrap($label.$control, $section_id, $class);

			// Pass the field template as a Javascript variable
			if (isset($field_template) && !empty($field_template))
				wp_localize_script('yo-clone', $field['id'], preg_replace("/[\n\r\t]/", '', $field_template));

			return $output;
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Render an array of fields
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return string $output	The fields' HTML output
		 */
		static function create_many($fields, $data = array(), $normalize = false)
		{
			$output = '';
			foreach ($fields as &$field)
				$output .= self::create($field, (isset($data[$field['id']]) ? $data[$field['id']] : ''), $normalize);

			return $output;
		}


		/**
		 * Normalize parameters for a single field
		 *
		 * @param array $field
		 *
		 */
		public static function normalize($field)
		{
			$clone 	  = isset($field['clone']) ? $field['clone'] : false;
			$group 	  = isset($field['group']) ? $field['group'] : false;
			$multiple = (in_array($field['type'], array('checkbox_list', 'file', 'image', 'plupload_image')) || (isset($field['multiple']) && $field['multiple'] == true)) ? true : false;
			$std      = $multiple ? array() : '';
			$format   = ('date' === $field['type']) ? 'yy-mm-dd' : ('time' === $field['type'] ? 'hh:mm' : '');
			
			$field = wp_parse_args($field, array(
				'multiple' 	=> $multiple,
				'clone' 	=> $clone,
				// 'group'		=> $group,
				'std'      	=> $std,
				// 'desc'     	=> '',
				'format'   	=> $format
			));

			$field['field_name'] = $field['id'] . (($multiple || $clone) ? '[]' : '');

			// Execute a custom query to fill the options if it's not an array of values
			// Example: ?p=1& ...
			if (isset($field['options']) && ! is_array($field['options']))
			{
				$options = array();

				switch ($field['options'])
				{
					default:
					case 'categories':
						$categories = get_categories(array('hide_empty' => false));
						foreach ($categories as $category)
							$options[$category->cat_ID] = $category->name . ' (' .$category->category_count.')';
						break;
				}
				// $defaults = array(
				//    'orderby' => 'title',
				//    'order'=> 'ASC',
				//    'post_type' => 'post',
				//    'genre' => false,
				//    'post_status' => 'publish'
				// );

				// // Merge the passed parameters with the default values
				// $args = wp_parse_args($field['options'], $defaults);

				// // Get the posts
				// $posts = get_posts($args);

				// // This array will store the options
				// $options = array('' => '');

				// foreach ($posts as $post)
				// {
				// 	setup_postdata($post);
				// 	$options[$post->ID] = $post->post_title;
				// }



				// Set the options
				$field['options'] = $options;
				unset($options);
			}

			// Allow field class add/change default field values
			$field = self::apply_field_class_filters($field, 'normalize_field', $field);
			do_action('normalize_'.$field['type'].'_field', $field);

			// Execute actions defined in the fields
			$class = self::get_class_name($field['type']);
				if (method_exists($class, 'add_actions'))
					call_user_func(array($class, 'add_actions'));


			// $field = apply_filters("normalize_field", $field);

			// yo_debug($field, $field['id']);

			return $field;
		}


		/**
		 * Normalize parameters for an array of fields
		 *
		 * @param array $fields
		 *
		 */
		public static function normalize_many($fields)
		{
			foreach ($fields as &$field)
				$field = self::normalize($field);

			return $fields;
		}

		/**
		 * Callback function to add clone buttons on demand
		 * Hooks on the flight into the "yo_{$field_id}_html" filter before the closing div
		 *
		 * @param string $html
		 * @param array  $field
		 * @param mixed  $meta_data
		 *
		 * @return string $html
		 */
		public static function add_delete_clone_button($html, $field, $meta_data)
		{
			$id = $field['id'];
			$button  = '<a href="#" class="yo-button button-secondary yo-remove-clone" title="'.__('Delete Item', 'theme_admin').'" data-unclone="'.$field['id'].'">&ndash;</a>';

			return "{$html}{$button}";
		}


		/**
		 * Apply filters by field class, fallback to YO_Fields
		 *
		 * @param array  $field
		 * @param string $method_name
		 * @param mixed  $data
		 *
		 * @return mixed $data
		 */
		static function apply_field_class_filters($field, $method_name, $data)
		{
			$args	= array_slice(func_get_args(), 2);
			$args[]	= $field;

			// The field class
			$class = self::get_class_name($field['type']);
			
			// The caller
			$caller = self::get_caller_class();

			// Search the requested method in the field class
			if (method_exists($class, $method_name))
			{
				$data = call_user_func_array(array($class, $method_name), $args);
			}
			// Then look in the caller class
			elseif (method_exists($caller, $method_name))
			{
				$data = call_user_func_array(array($caller, $method_name), $args);
			}
			// Otherwise look in the current class
			elseif (method_exists(__CLASS__, $method_name))
			{
				$data = call_user_func_array(array(__CLASS__, $method_name), $args);
			}

			return $data;
		}


		/**
		 * Call field class method for actions, fallback to RW_Meta_Box method
		 * TODO: merge with apply_field_class_filters(), avoid repeating same code
		 *
		 * @param array  $field
		 * @param string $method_name
		 *
		 * @return mixed
		 */
		static function do_field_class_actions($field, $method_name)
		{
			$args   = array_slice( func_get_args(), 2 );
			$args[] = $field;

			$class = self::get_class_name( $field['type'] );
			$caller = self::get_caller_class();
			
			if (method_exists($class, $method_name))
			{
				call_user_func_array(array($class, $method_name), $args);
			}
			elseif (method_exists($caller, $method_name))
			{
				$value = call_user_func_array(array($caller, $method_name), $args);
			}
			elseif (method_exists(__CLASS__, $method_name))
			{
				call_user_func_array(array(__CLASS__, $method_name), $args);
			}
		}


		/**
		 * Get caller class name
		 *
		 * @return bool|string Caller class name OR false on failure
		 */
		static function get_caller_class()
		{
		    $traces = debug_backtrace();
		    return (isset($traces[2])) ? $traces[2]['class'] : null;
		}


		/**
		 * Get field class name
		 *
		 * @param string $type Field type
		 *
		 * @return bool|string Field class name OR false on failure
		 */
		static function get_class_name($type)
		{
			$type	= ucwords($type);
			$class	= "YO_{$type}_Field";
			return (class_exists($class)) ? $class : false;
		}


		/**
		 * Check if the field is cloneable
		 *
		 * @param  array $field
		 *
		 * @return bool False if not cloneable
		 */
		static function is_cloneable($field)
		{
			return (isset($field['clone'])) ? $field['clone'] : false;
		}


		/**
		 * Determine if the passed array is associative or numeric
		 *
		 * @return bool False if numeric
		 */
		static function is_associative($array)
		{
			return (is_numeric(key($array))) ? false : true;
		}


		/**
		 * Recursively check if the array is empty
		 *
		 * @return bool 
		 */
		public static function is_array_empty($array)
		{
		   $result = true;

		   if (is_array($array) && count($array) > 0)
		   {
		      foreach ($array as $value)
		         $result = $result && self::is_array_empty($value);
		   }
		   else
		   {
		      $result = empty($array);
		   }

		   return $result;
		}


		/**
		 * Wrap HTML content in a container tag
		 *
		 * @return string
		 */
		static function wrap($content, $id = false, $class = false)
		{
			$id = ($id) ? ' id="'.$id.'"' : '';
			$class = ($class) ? ' class="'.$class.'"' : '';
			return "<div{$id}{$class}>{$content}</div>";
		}


		/**
		 * Helper function to check the current page and load the assets when necessary.
		 * TODO: make this configurable
		 *
		 * @param  array $whitelist List of the allowed pages for the assets to be loaded
		 *
		 * @return bool False if the the current page is not in the $whitelist
		 */
		static function can_be_loaded($whitelist = array())
		{
			global $current_screen;

			$whitelist = array(	'edit-post',
								'post',
								'edit-portfolio',
								'portfolio',
								'edit-page',
								'edit',
								'page',
								'appearance_page_yo-theme-options',
								'settings_page_yo-shortcodes-editor',
								'portfolio_page_type-portfolio');

			$screen = get_current_screen();
			// echo 'PAGE-ID: '.$screen->id.' ';
			
			if (in_array($screen->id, $whitelist))
				return true;

			return false;
		}


		/**
		 * Helper function to check the current page and load the assets when necessary.
		 * TODO: make this configurable
		 *
		 * @param  array $whitelist List of the allowed pages for the assets to be loaded
		 *
		 * @return bool False if the the current page is not in the $whitelist
		 */
		static function can_load_assets($whitelist = array())
		{
			return self::can_be_loaded();
		}


		/**
		 * Adds a css class
		 * Mainly a copy of the core admin menu function
		 * As the core function is only meant to be used by core internally,
		 * We copy it here - in case core changes functionality or drops the function.
		 *
		 * @param string $add
		 * @param string $class | Class name - Default: empty
		 *
		 * @return string $class
		 */
		static function add_cssclass($add, $class = '')
		{
			$class .= empty( $class ) ? $add : " {$add}";
			return $class;
		}
	}
}
