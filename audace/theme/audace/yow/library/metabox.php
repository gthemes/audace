<?php
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/

// Prevent loading this file directly
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

// Metabox Generator Class
if ( ! class_exists('YO_Metabox'))
{
	class YO_Metabox
	{
		/**
		 * Meta box information
		 */
		var $metabox;

		/**
		 * Fields class object
		 */
		var $output;


		/**
		 *------------------------------------------------------------------------------------------------
		 * Create meta box with given data
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function __construct($metabox)
		{
			// Run script only in admin area
			// TODO: Add capabilities
			if ( ! is_admin())
				return;
			
			$this->output = new YO_Fields($metabox['fields']);
			unset($metabox['fields']);

			// Set the metabox in the class object
			$this->metabox = $metabox;
			

			// $this->metabox['fields'] = YO_Fields::normalize_many(&$this->metabox['fields']);

			// Add meta box
			foreach ($this->metabox['pages'] as $page)
				add_action( "add_meta_boxes_{$page}", array($this, 'add_metabox'));

			// Save post meta
			add_action('save_post', array($this, 'save_post'));
		}


		/**
		 * Add meta boxes for the selected post types
		 *
		 * @return void
		 */
		public function add_metabox()
		{
			foreach ($this->metabox['pages'] as $page)
			{
				add_meta_box(
					$this->metabox['id'],
					$this->metabox['title'],
					array($this, 'render_metabox'),
					$page,
					$this->metabox['context'],
					$this->metabox['priority']
				);

				// Add CSS classes to the meta box
				add_filter('postbox_classes_post_'.$this->metabox['id'], array(&$this, 'add_metabox_classes'));
			}
		}

		/**
		 * Add CSS classes for the selected metaboxes
		 *
		 * @return void
		 */
		public function add_metabox_classes($classes)
		{
			if ((isset($this->metabox['class'])))
				array_push($classes, $this->metabox['class']);

    		return $classes;
		}


		/**
		 * Callback function to show fields in meta box
		 *
		 * @return void
		 */
		public function render_metabox()
		{
			// Using the 'nonce' for security
			wp_nonce_field("yo-save-{$this->metabox['id']}", "nonce_{$this->metabox['id']}");

			global $post;

			// Check if the data has been saved already
			// $saved = self::has_been_saved($post->ID, &$this->metabox['fields']);
			$saved = self::has_been_saved($post->ID, $this->output->fields);

			// Allow users to add custom code before meta box content
			// 1st action applies to all meta boxes
			// 2nd action applies to only current meta box
			do_action('yo_before_metabox');
			do_action("yo_before_{$this->metabox['id']}");

			

			echo '<div class="yo-metabox yo-container">';
			// echo $this->output;

			// Set the data for each field
			// $data = self::get_recursive_data($post->ID, $saved, $this->metabox['fields']);
			$data = self::get_recursive_data($post->ID, $saved, $this->output->fields);

			

			// $data = array();
			// foreach ($this->metabox['fields'] as &$field)
			// {
			// 	// $field = YO_Fields::normalize($field);
			// 	$data[$field['id']] = YO_Fields::apply_field_class_filters($field, 'get_data', $post->ID, $saved);
			// 	$data = apply_filters("yo_{$field['type']}_data", $data, $field);

			// 	yo_debug($data[$field['id']], $field['id']);

			// 	// if ($field['type'] == 'group')
			// 	// {

			// 	// }
			// }
			
			// yo_debug($data, 'Data');


			// Output the fields
			// echo YO_Fields::create_many($this->output->fields, $data);
			echo $this->output->generate($data);

			echo '</div>';

			// Allow users to add custom code after meta box content
			// 1st action applies to all meta boxes
			// 2nd action applies to only current meta box
			do_action('yo_after_metabox');
			do_action("yo_after_{$this->metabox['id']}");
		}

		static function get_recursive_data($post_id, $saved, $fields)
		{			
			$data = array();
			foreach ($fields as &$field)
			{
				$data[$field['id']] = YO_Fields::apply_field_class_filters($field, 'get_data', $post_id, $saved);
				$data = apply_filters("yo_{$field['type']}_data", $data, $field);

				if (!empty($data[$field['id']]))
					if ($field['type'] === 'group')
						foreach ($field['fields'] as &$grouped_field)
							if ($grouped_field['type'] === 'group')
								$data[$field['id']] = array_merge($data[$field['id']], self::get_recursive_data($post_id, $saved, array($grouped_field)));
								// $data = array_merge($data, self::get_recursive_data($post_id, $saved, array($grouped_field)));
			}

			return $data;
		}

		/**
		 * Data retrieval
		 *
		 * @param mixed $meta
		 * @param int	$post_id
		 * @param array $field
		 * @param bool  $saved
		 *
		 * @return mixed
		 */
		static function get_data($post_id, $saved, $field)
		{		
			$data = get_post_meta($post_id, $field['id'], (!$field['multiple']));

			// Use $field['std'] only when the meta box hasn't been saved (i.e. the first time we run)
			if (isset($field['std']))
				$data = (! $saved && '' === $data || array() === $data) ? $field['std'] : $data;

			// Escape attributes for non-wysiwyg fields
			if ($field['type'] !== 'wysiwyg' && $field['type'] !== 'group')
				$data = is_array( $data ) ? array_map( 'esc_attr', $data ) : esc_attr( $data );

			// yo_debug($field['id']);

			// echo '<br>-----------------------<br>'.$field['id'];
			// echo '<pre>';
			// var_dump($data);
			// echo '</pre>';

			return $data;
		}


		/**************************************************
			SAVE META BOX
		**************************************************/

		/**
		 * Save data from meta box
		 *
		 * @param int $post_id Post ID
		 *
		 * @return int|void
		 */
		function save_post($post_id)
		{
			global $post_type;
			$post_type_object = get_post_type_object( $post_type );

			// Check whether:
			// - the post is autosaved
			// - the post is a revision
			// - current post type is supported
			// - user has proper capability
			if (
				( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				|| ( ! isset($_POST['post_ID']) || $post_id != $_POST['post_ID'])
				|| ( ! in_array($post_type, $this->metabox['pages']))
				|| ( ! current_user_can($post_type_object->cap->edit_post, $post_id))
				)
			{
				return $post_id;
			}

			// Verify nonce
			check_admin_referer( "yo-save-{$this->metabox['id']}", "nonce_{$this->metabox['id']}" );

			// Start the recursive save for nested fields
			self::recursive_save($post_id, $this->output->fields);

			// echo '<pre>';
			// print_r($_POST);
			// echo '</pre>';
		}


		static function recursive_save($post_id, $fields)
		{
			foreach ($fields as &$field)
			{
				$old  = get_post_meta($post_id, $field['id'], ! isset($field['multiple']));
				$new  = isset($_POST[$field['id']]) ? $_POST[$field['id']] : (isset($field['multiple']) ? array() : '');

				// Allow field class change the value
				$new = YO_Fields::apply_field_class_filters($field, 'value', $new, $old, $post_id);

				// Use filter to change field value
				// 1st filter applies to all fields with the same type
				// 2nd filter applies to current field only
				$new = apply_filters("yo_{$field['type']}_value", $new, $field, $old);
				$new = apply_filters("yo_{$field['id']}_value", $new, $field, $old);

				// Call defined method to save meta value, if there's no methods, call common one
				YO_Fields::do_field_class_actions($field, 'save', $new, $old, $post_id);

				if ($field['type'] === 'group')
					foreach ($field['fields'] as &$grouped_field)
						if ($grouped_field['type'] === 'group')
							self::recursive_save($post_id, array($grouped_field));
			}
		}


		/**
		 * Common functions for saving field
		 *
		 * @param mixed $new
		 * @param mixed $old
		 * @param int $post_id
		 * @param array $field
		 *
		 * @return void
		 */
		static function save($new, $old, $post_id, $field)
		{
			$name = $field['id'];

			delete_post_meta($post_id, $name);
			if ('' === $new || array() === $new)
				return;

			// echo '<pre>';
			// print_r($field);
			// echo '</pre>';

			if (isset($field['multiple']) && $field['multiple'] === true /*&& $field['type'] != 'select'*/)
			// if (isset($field['multiple']))
			{
				// echo $field['id'].': multiple field : '.$field['multiple'].'<br>';
				// print_r($new);
				foreach ($new as $add_new)
					add_post_meta($post_id, $name, $add_new, false);
			}
			else
			{
				update_post_meta($post_id, $name, $new);
			}

		}


		/**
		 * Check if meta box has been saved
		 * This helps saving empty value in meta fields (for text box, check box, etc.)
		 *
		 * @param int   $post_id
		 * @param array $fields
		 *
		 * @return bool
		 */
		static function has_been_saved($post_id, &$fields)
		{
			$saved = false;
			foreach ($fields as &$field)
			{
				if (get_post_meta($post_id, $field['id'], ! isset($field['multiple'])))
				{
					$saved = true;
					break;
				}
			}
			return $saved;
		}
	}
}