<?php

/*
 *------------------------------------------------------------------------------------------------
 * Custom Admin Login Logo
 *------------------------------------------------------------------------------------------------
 *
 */

if ( ! function_exists('yo_custom_login_logo'))
{
    function yo_custom_login_logo()
    {
        echo '<style type="text/css">
            h1 a { background-image:url('.get_template_directory_uri().'/assets/img/admin-login-logo.png) !important; background-size:auto !important; }
        </style>';
    }
}

add_action('login_head', 'yo_custom_login_logo');



/*
 *------------------------------------------------------------------------------------------------
 * Allow shortcodes in the 'text' widget
 *------------------------------------------------------------------------------------------------
 *
 */

add_filter('widget_text', 'do_shortcode');



/*
 *------------------------------------------------------------------------------------------------
 * Compare WP Versions
 *------------------------------------------------------------------------------------------------
 *
 * Compare wp_version with a specific version (useful for features check).
 * 
 */

if ( ! function_exists('is_wp_version')) 
{
    function is_wp_version($version, $operator = '>=')
    {
        global $wp_version;
        if (version_compare($wp_version, $v, $operator))
            return true;
    }
}



/*
 *------------------------------------------------------------------------------------------------
 * Add Custom Type to Feed
 *------------------------------------------------------------------------------------------------
 *
 * Add a Custom Post Type to the RSS feed.
 * 
 */

function add_cpt_to_feed($qv)
{
  if (isset($qv['feed']) && !isset($qv['post_type']))
    $qv['post_type'] = array('post');
  return $qv;
}



/*
 *------------------------------------------------------------------------------------------------
 * Body Classes
 *------------------------------------------------------------------------------------------------
 *
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed.
 *
 */

function yo_body_classes($classes)
{
    if (function_exists( 'is_multi_author') && ! is_multi_author())
        $classes[] = 'single-author';

    if (is_singular() && ! is_home() && ! is_page_template('showcase.php') && ! is_page_template('sidebar-page.php'))
        $classes[] = 'singular';

    return $classes;
}

add_filter('body_class', 'yo_body_classes');



/*
 *------------------------------------------------------------------------------------------------
 * Columnize Sidebar Widgets
 *------------------------------------------------------------------------------------------------
 *
 * Set the proper 'spanX' class depending on the number of widgets, assuming a 12 columns layout.
 * TODO: Make the CSS class configurable
 *
 */

function yo_columnize_sidebars($params)
{
    $sidebar_id = $params[0]['id'];
    $allowed_widgets = array('before-header', 'footer-widgets', 'before-loop', 'after-loop');

    if (in_array($sidebar_id, $allowed_widgets))
    {
        $total_widgets = wp_get_sidebars_widgets();
        $sidebar_widgets = count($total_widgets[$sidebar_id]);

        $params[0]['before_widget'] = str_replace('class="', 'class="grid_' . floor(12 / $sidebar_widgets) . ' ', $params[0]['before_widget']);
    }

    return $params;
}

add_filter('dynamic_sidebar_params', 'yo_columnize_sidebars');



/*
 *------------------------------------------------------------------------------------------------
 * Characters Counter for the Excerpt Box
 *------------------------------------------------------------------------------------------------
 *
 */

function excerpt_count_js(){
    echo '<script>jQuery(document).ready(function(){
    jQuery("#postexcerpt .handlediv").after("<div style=\"position:absolute;top:2px;right:30px;color:#666;\"><small>Excerpt length: </small><input type=\"text\" value=\"0\" maxlength=\"3\" size=\"3\" id=\"excerpt_counter\" readonly=\"\" style=\"background:#fff;\"> <small>character(s)</small></div>");
         jQuery("#excerpt_counter").val(jQuery("#excerpt").val().length);
         jQuery("#excerpt").keyup( function() {
         jQuery("#excerpt_counter").val(jQuery("#excerpt").val().length);
       });
    });</script>';
}
// add_action( 'admin_head-post.php', 'excerpt_count_js');
// add_action( 'admin_head-post-new.php', 'excerpt_count_js');



/*
 *------------------------------------------------------------------------------------------------
 * Change the automatic excerpt length
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_excerpt_length($length)
{
    return 50;
}
add_filter('excerpt_length', 'yo_excerpt_length', 999);


function yo_excerpt_more($more) {
    // global $post;
    // return '&hellip; <a class="read-more" href="'. get_permalink($post->ID) . '">...</a>';
    // return '<a class="read-more" href="'. get_permalink($post->ID) . '">&hellip;</a>';
    return ' &hellip;';
}
add_filter('excerpt_more', 'yo_excerpt_more');



function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function yo_has_excerpt()
{
    $excerpt = get_the_excerpt();


    // var_dump(strip_shortcodes($excerpt));

    // if (ctype_space($excerpt)) {
    //     echo 'only spaces!!';
    // }

    // $excerpt = strlen(trim(preg_replace('/\xc2\xa0/',' ', $excerpt)));
    // var_dump($excerpt);
    // return (trim($excerpt) == '') ? false : true;

    return (ctype_space($excerpt) || $excerpt == '' || empty($excerpt)) ? false : true;
    // return (strlen(trim(preg_replace('/\xc2\xa0/',' ', $excerpt))) == 0) ? false : true;
}



/*
 *------------------------------------------------------------------------------------------------
 * Refresh rewrite rules automatically on theme switch
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_rewrite_flush()
{
    flush_rewrite_rules();
}

// add_action('after_switch_theme', 'yo_rewrite_flush');
// register_activation_hook( __FILE__, 'yo_rewrite_flush' );


/*
 *------------------------------------------------------------------------------------------------
 * Add the custom sizes to the media library radio buttons
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_register_custom_thumbnails($form_fields, $post)
{
    if (!array_key_exists('image-size', $form_fields))
        return $form_fields;

    global $_wp_additional_image_sizes;
    foreach($_wp_additional_image_sizes as $size => $properties)
    {
        if ($size == 'post-thumbnail') continue;

        $label = ucwords(str_replace('-', ' ', $size));
        $cssID = "image-size-{$size}-{$post->ID}";

        $downsize = image_downsize($post->ID, $size);
        $enabled = $downsize[2];

        $html = '<input type="radio" ' . disabled($enabled, false, false) . 'name="attachments[' . $post->ID. '][image-size]" id="' . $cssID . '" value="' . $size .'">';
        $html .= '<label for="'. $cssID . '">' . $label . '</label>';
        if ($enabled) $html .= ' <label for="' . $cssID . '" class="help">(' . $downsize[1] . '&nbsp;&nbsp;' . $downsize[2] . ')</label>';
        $form_fields['image-size']['html'] .= '<div class="image-size-item">' . $html . '</div>';
    }

    return $form_fields;
}

add_filter('attachment_fields_to_edit', 'yo_register_custom_thumbnails', 100, 2);





// function yo_custom_tinymce_styles($init)
// {
//     $init['theme_advanced_buttons2_add_before'] = 'styleselect';
//     $init['theme_advanced_styles'] = 'Button=button mini,Centered text=big-centered';
//     return $init;
// }

// add_filter('tiny_mce_before_init', 'yo_custom_tinymce_styles');

/*
 *------------------------------------------------------------------------------------------------
 * Fix empty search query returning the wrong page
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_fix_search_result($query_vars) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}

add_filter('request', 'yo_fix_search_result');


/*
 *------------------------------------------------------------------------------------------------
 * User profiles custom contact methods
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_contactmethods( $contactmethods )
{
    $contactmethods['twitter'] = 'Twitter username';
    $contactmethods['facebook'] = 'Facebook URL';
    return $contactmethods;
}
// add_filter('user_contactmethods', 'yo_contactmethods');


/*
 *------------------------------------------------------------------------------------------------
 * Helpers for embedding CSS, Javascript & jQuery
 *------------------------------------------------------------------------------------------------
 *
 */

// Define the type of code
$yo_embed_types = array(
    'html' => '',
    'css' => '',
    'js' => '',
    'jquery' => '',
);

// Global array that stores the embedded code to load
$yo_embedded = array(
    'admin' => array(
        'head' => $yo_embed_types,
        'footer' => $yo_embed_types
    ),
    'frontend' => array(
        'head' => $yo_embed_types,
        'footer' => $yo_embed_types
    ),
);

// Add HTML code
function yo_html($html, $footer = false, $admin = true)
{   
    global $yo_embedded;
    $position = ($footer) ? 'footer' : 'head';
    $area = ($admin) ? 'admin' : 'frontend';
    $yo_embedded[$area][$position]['html'] .= $html/*."\n"*/;
}

// Add CSS code
function yo_css($css, $footer = false, $admin = true)
{
    global $yo_embedded;
    $position = ($footer) ? 'footer' : 'head';
    $area = ($admin) ? 'admin' : 'frontend';
    $yo_embedded[$area][$position]['css'] .= $css/*."\n"*/;
}

// Add Javascript code
function yo_js($js, $footer = false, $jquery = false, $admin = true)
{
    global $yo_embedded;
    $position = ($footer) ? 'footer' : 'head';
    $type = ($jquery) ? 'jquery' : 'js';
    $area = ($admin) ? 'admin' : 'frontend';
    $yo_embedded[$area][$position][$type] .= $js/*."\n"*/;
}

// Add jQuery code
function yo_jquery($js, $footer = true, $admin = true)
{
    yo_js($js, $footer, true, $admin);
}

// Output custom CSS and JS
function yo_embed_code($footer = false, $admin = true)
{
    global $yo_embedded;
    $position = ($footer) ? 'footer' : 'head';
    $area = ($admin) ? 'admin' : 'frontend';

    // HTML
    if ( ! empty($yo_embedded[$area][$position]['html']))
        echo yo_embed_type($yo_embedded[$area][$position]['html'], 'html');
        // echo $yo_embedded[$position]['html'];

    // CSS
    if ( ! empty($yo_embedded[$area][$position]['css']))
        echo yo_embed_type($yo_embedded[$area][$position]['css'], 'css');
        // echo '<style type="text/css">'.$yo_embedded[$position]['css'].'</style>';

    // Javascript
    if ( ! empty($yo_embedded[$area][$position]['js']))
        echo yo_embed_type($yo_embedded[$area][$position]['js'], 'js');
        // echo '<script type="text/javascript">'.$yo_embedded[$position]['js'].'</script>';

    // jQuery
    if ( ! empty($yo_embedded[$area][$position]['jquery']))
        echo yo_embed_type($yo_embedded[$area][$position]['jquery'], 'jquery');

        // echo '<script type="text/javascript">jQuery(document).ready(function($){
                // '.$yo_embedded[$position]['jquery'].'
            // });
        // </script>';
}

function yo_embed_type($content, $type)
{
    switch ($type)
    {
        case 'html':
            return $content;
            break;

        case 'css':
            return '<style type="text/css">'.$content.'</style>';
            break;

        case 'js':
            return '<script type="text/javascript" data-needed="true">'.$content.'</script>';
            break;

        case 'jquery':
            return '<script type="text/javascript" data-needed="true">jQuery(document).ready(function($){'.$content.'});</script>';
            break;
    }
}


// Admin embeds
// Output custom CSS and JS to the admin head
function yo_admin_head()
{
    yo_embed_code();
}
add_action('admin_head', 'yo_admin_head');

// Output custom CSS and JS to the admin footer
function yo_admin_footer()
{
    yo_embed_code(true);
}
add_action('admin_footer', 'yo_admin_footer', 100);

// Frontend embeds
// Output custom CSS and JS to the frontend head
function yo_frontend_head()
{
    yo_embed_code(false, false);
}
add_action('wp_head', 'yo_frontend_head');

// Output custom CSS and JS to the frontend footer
function yo_frontend_footer()
{
    yo_embed_code(true, false);
}
add_action('wp_footer', 'yo_frontend_footer', 100);


/*
 *------------------------------------------------------------------------------------------------
 * Extract Images from the Page / Post  content
 *------------------------------------------------------------------------------------------------
 *
 */

function yo_extract_images($content = false, $num = false) 
{
    // global $more;
    // $more = 1;
    $link = get_permalink();

    if ( $content === false )
        $content = get_the_content();

    $count = substr_count($content, '<img');
    $start = 0;

    for($i=1; $i<=$count; $i++)
    {
        $imgBeg = strpos($content, '<img', $start);
        $post = substr($content, $imgBeg);
        $imgEnd = strpos($post, '>');
        $postOutput = substr($post, 0, $imgEnd+1);
        $postOutput = preg_replace('/width="([0-9]*)" height="([0-9]*)"/', '', $postOutput);;
        $images[$i] = $postOutput;
        $start = $imgEnd+1;
    }
    
    if ($num)
    {
        if(stristr($images[$num], '<img'))
            return $images[$num];
    }
    else
    {
        return $images;
    }

    // $more = 0;
}



/*
 *------------------------------------------------------------------------------------------------
 * Protected posts title format
 *------------------------------------------------------------------------------------------------
 *
 * Removes the 'Protected' word from private posts
 *
 */
function yo_protected_title_format($title)
{
       return '%s';
}
add_filter('protected_title_format', 'yo_protected_title_format');



/*
 *------------------------------------------------------------------------------------------------
 * Add Custom Post Type Class to Menu
 *------------------------------------------------------------------------------------------------
 *
 * Adds the class to highlight the current page if it's a custom post type.
 *
 */

function current_type_nav_class($classes, $item)
{
    $post_type = strtolower(get_query_var('post_type'));
    // echo $post_type . ' / ' .$item->attr_title . ' ---------- ';
    // echo '<pre>';
    // print_r($item);
    // echo '<pre>';
    // echo '<br>-----------------<br>';
    $new_title = strtolower($item->title);

    if ($new_title == $post_type)
    {
        array_push($classes, 'current-menu-item');
    }

    return $classes;
}
add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2 );



/*
 *------------------------------------------------------------------------------------------------
 * Clean Menu
 *------------------------------------------------------------------------------------------------
 *
 * Remove the classes from the main nav
 *
 */

// function remove_nav_menu_classes($classes) {
//     return array(); 
// }
// add_filter('nav_menu_css_class','remove_nav_menu_classes');



// Filter wp_nav_menu() to add additional links and other output
// function theme_nav_menu_items($items) {
//     $homelink = '<li class="home"><a href="' . home_url( '/' ) . '">' . __('Home', 'theme') . '</a></li>';
//     $items = $homelink . $items;
//     return $items;
// }
// add_filter( 'wp_nav_menu_items', 'theme_nav_menu_items' );



/*
 *------------------------------------------------------------------------------------------------
 * Convert Array to JSON
 *------------------------------------------------------------------------------------------------
 *
 * Remove the classes from the main nav
 *
 */

// http://www.bin-co.com/php/scripts/array2json/

function array2json($arr)
{
    if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality.
    
    $parts = array();
    $is_list = false;

    //Find out if the given array is a numerical array
    $keys = array_keys($arr);
    $max_length = count($arr)-1;
    if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) { //See if the first key is 0 and last key is length - 1
        $is_list = true;
        for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position
            if($i != $keys[$i]) { //A key fails at position check.
                $is_list = false; //It is an associative array.
                break;
            }
        }
    }

    foreach($arr as $key=>$value) {
        if(is_array($value)) { //Custom handling for arrays
            if($is_list) $parts[] = array2json($value); /* :RECURSION: */
            else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */
        } else {
            $str = '';
            if(!$is_list) $str = '"' . $key . '":';

            //Custom handling for multiple data types
            if(is_numeric($value)) $str .= $value; //Numbers
            elseif($value === false) $str .= 'false'; //The booleans
            elseif($value === true) $str .= 'true';
            else $str .= '"' . addslashes($value) . '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Object?)

            $parts[] = $str;
        }
    }
    $json = implode(',',$parts);
    
    if($is_list) return '[' . $json . ']';//Return numerical JSON
    return '{' . $json . '}';//Return associative JSON
}



// Get all the images attached to the current post
function yo_get_attachments()
{
    global $post; // refers to the post or parent being displayed

    $args = array(
      'post_type' => 'attachment',  // only get "attachment" type posts
      'numberposts' => -1,
      'post_status' => null,
      'post_parent' => $post->ID,   // only get attachments for current post/page
      //'posts_per_page' => -1        // get all attachments
    );

    return get_posts($args);
}


function yo_get_images($size = 'thumbnail', $limit = '0', $offset = '0') {
    global $post;

    $images = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID') );

    if ($images) {

        $num_of_images = count($images);

        if ($offset > 0) : $start = $offset--; else : $start = 0; endif;
        if ($limit > 0) : $stop = $limit+$start; else : $stop = $num_of_images; endif;

        $i = 0;
        foreach ($images as $image)
        {
            if ($start <= $i and $i < $stop) 
            {
                $img_title = $image->post_title;   // title.
                $img_description = $image->post_content; // description.
                $img_caption = $image->post_excerpt; // caption.
                $img_url = wp_get_attachment_url($image->ID); // url of the full size image.
                $preview_array = image_downsize( $image->ID, $size );
                $img_preview = $preview_array[0]; // thumbnail or medium image to use for preview.

                ?>
                    <img src="<?php echo $img_preview; ?>" alt="<?php echo $img_caption; ?>" title="<?php echo $img_title; ?>">
                <?php

            }
            $i++;
        }

    }
}



/*
 *------------------------------------------------------------------------------------------------
 * Add Custom Taxonomy to the Menu
 *------------------------------------------------------------------------------------------------
 *
 */


// add_action('admin_head-nav-menus.php', 'filters_for_cpt_archives');               // hacks the output of CPT nav menu items displayed in Appearance -> Menus


// generate filter hooks for adding cpt archive checkboxes
// props to Kevin Langley https://github.com/klangley/cpt-archive-to-nav
function filters_for_cpt_archives()
{
    $post_type_args = array(
        'show_in_nav_menus' => true
    );

    $post_types = get_post_types($post_type_args, 'object');

    foreach ($post_types as $post_type)
    {
        // print_r($post_type);
        if ($post_type->has_archive)
        {
            // echo $post_type->name;
            // echo '<br>---------------------<br>';
            add_filter('nav_menu_items_' . $post_type->name, 'add_cpt_archive_checkbox', null, 3);
        }
        // echo '</pre>';
    }
}


// inject a new checkbox item in Apppearance -> Menus -> (your cpt) visible in the "view all" tab --- so you can manually add a custom nav menu item for your CPT archive pages
// props to Kevin Langley https://github.com/klangley/cpt-archive-to-nav
function add_cpt_archive_checkbox($posts, $args, $post_type)
{
    global $_nav_menu_placeholder, $wp_rewrite;
    $_nav_menu_placeholder = (0 > $_nav_menu_placeholder) ? intval($_nav_menu_placeholder) - 1 : -1;

    array_unshift($posts, (object) array(
        'ID' => 0,
        'object_id' => $_nav_menu_placeholder,
        'post_content' => '',
        'post_excerpt' => '',
        'post_title' => $post_type['args']->labels->name,
        'post_name' => $post_type['args']->name,
        'post_type' => 'nav_menu_item',
        'type' => 'custom',
        'url' => get_post_type_archive_link($args['post_type'])
    ));

    return $posts;
}








/*
 *------------------------------------------------------------------------------------------------
 * Change local language
 *------------------------------------------------------------------------------------------------
 *
 * Example: www.example.com/?l=it_IT
 *
 */

// CHANGE LOCAL LANGUAGE
// must be called before load_theme_textdomain()
function yo_theme_localized($locale) {
    if (isset($_GET['l'])) {
        return $_GET['l'];
    }
    return $locale;
}
add_filter('locale', 'yo_theme_localized');














/*
 *------------------------------------------------------------------------------------------------
 * Automatic Plugins Install
 *------------------------------------------------------------------------------------------------
 *
 * Example: 
 *
 *   $plugins = array(
 *       array('name' => 'jetpack', 'path' => 'http://downloads.wordpress.org/plugin/jetpack.1.3.zip', 'install' => 'jetpack/jetpack.php'),
 *       array('name' => 'buddypress', 'path' => 'http://downloads.wordpress.org/plugin/buddypress.1.5.5.zip', 'install' => 'buddypress/bp-loader.php'),
 *       array('name' => 'tumblr-importer', 'path' => 'http://downloads.wordpress.org/plugin/tumblr-importer.0.5.zip', 'install' => 'tumblr-importer/tumblr-importer.php')
 *   );
 *   
 *   mm_get_plugins($plugins);
 *
 * Credits: maiorano84, sorich87
 * Source: http://stackoverflow.com/questions/10353859/is-it-possible-to-programmatically-install-plugins-from-wordpress-theme
 * 
 */

function mm_get_plugins($plugins)
{
    $args = array(
        'path' => ABSPATH.'wp-content/plugins/',
        'preserve_zip' => false
    );

    foreach($plugins as $plugin)
    {
        mm_plugin_download($plugin['path'], $args['path'].$plugin['name'].'.zip');
        mm_plugin_unpack($args, $args['path'].$plugin['name'].'.zip');
        mm_plugin_activate($plugin['install']);
    }
}

function mm_plugin_download($url, $path) 
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);
    if(file_put_contents($path, $data))
        return true;
    else
        return false;
}

function mm_plugin_unpack($args, $target)
{
    if($zip = zip_open($target))
    {
        while($entry = zip_read($zip))
        {
            $is_file = substr(zip_entry_name($entry), -1) == '/' ? false : true;
            $file_path = $args['path'].zip_entry_name($entry);
            if($is_file)
            {
                if(zip_entry_open($zip,$entry,"r")) 
                {
                    $fstream = zip_entry_read($entry, zip_entry_filesize($entry));
                    file_put_contents($file_path, $fstream );
                    chmod($file_path, 0777);
                    //echo "save: ".$file_path."<br />";
                }
                zip_entry_close($entry);
            }
            else
            {
                if(zip_entry_name($entry))
                {
                    mkdir($file_path);
                    chmod($file_path, 0777);
                    //echo "create: ".$file_path."<br />";
                }
            }
        }
        zip_close($zip);
    }
    if($args['preserve_zip'] === false)
    {
        unlink($target);
    }
}

function mm_plugin_activate($installer)
{
    $current = get_option('active_plugins');
    $plugin = plugin_basename(trim($installer));

    if(!in_array($plugin, $current))
    {
        $current[] = $plugin;
        sort($current);
        do_action('activate_plugin', trim($plugin));
        update_option('active_plugins', $current);
        do_action('activate_'.trim($plugin));
        do_action('activated_plugin', trim($plugin));
        return true;
    }
    else
    {
        return false;
    }
}






/*
 *------------------------------------------------------------------------------------------------
 * Default Metaboxes
 *------------------------------------------------------------------------------------------------
 *
 * Set the default metaboxes for post edit pages
 *
 */
function change_default_hidden($hidden, $screen)
{
    if ('page' == $screen->id)
    {
        $hidden = array_flip($hidden);
        // unset($hidden['authordiv']); //show author box
        $hidden = array_flip($hidden);
        $hidden[] = 'revisionsdiv'; //hide page attributes
    }
    return $hidden;
}

// add_filter( 'default_hidden_meta_boxes', 'change_default_hidden', 10, 2 );






/*
 *------------------------------------------------------------------------------------------------
 * Dynamic Page Templates
 *------------------------------------------------------------------------------------------------
 *
 */
function yo_page_template($page_template)
{
    if ( is_page( 'my-custom-page-slug' ) ) {
        $page_template = dirname( __FILE__ ) . '/custom-page-template.php';
    }
    return $page_template;
}

// add_filter( 'page_template', 'yo_page_template' );








/*
 *------------------------------------------------------------------------------------------------
 * Cache MySQL Queries
 *------------------------------------------------------------------------------------------------
 *
 */

// if ( false === ( $special_query_results = get_transient( 'special_query_results' ) ) ) {
//     // It wasn't there, so regenerate the data and save the transient
//      $special_query_results = new WP_Query( 'cat=5&order=random&tag=tech&post_meta_key=thumbnail' );
//      set_transient( 'special_query_results', $special_query_results );
// }









// Display the value of a custom field in your post by adding 
// the shortcode [field name=name-of-your-custom-field]
// function field_func($atts) {
//    global $post;
//    $name = $atts['name'];
//    if (empty($name)) return;

//    return get_post_meta($post->ID, $name, true);
// }

// add_shortcode('field', 'field_func');



// Create a new page and set it as HOME.

// $default_pages = array('Home');
//     $existing_pages = get_pages();
//     $temp = array();

//     foreach ($existing_pages as $page) {
//       $temp[] = $page->post_title;
//     }

//     $pages_to_create = array_diff($default_pages, $temp);

//     foreach ($pages_to_create as $new_page_title) {
//       $add_default_pages = array(
//         'post_title' => $new_page_title,
//         'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consequat, orci ac laoreet cursus, dolor sem luctus lorem, eget consequat magna felis a magna. Aliquam scelerisque condimentum ante, eget facilisis tortor lobortis in. In interdum venenatis justo eget consequat. Morbi commodo rhoncus mi nec pharetra. Aliquam erat volutpat. Mauris non lorem eu dolor hendrerit dapibus. Mauris mollis nisl quis sapien posuere consectetur. Nullam in sapien at nisi ornare bibendum at ut lectus. Pellentesque ut magna mauris. Nam viverra suscipit ligula, sed accumsan enim placerat nec. Cras vitae metus vel dolor ultrices sagittis. Duis venenatis augue sed risus laoreet congue ac ac leo. Donec fermentum accumsan libero sit amet iaculis. Duis tristique dictum enim, ac fringilla risus bibendum in. Nunc ornare, quam sit amet ultricies gravida, tortor mi malesuada urna, quis commodo dui nibh in lacus. Nunc vel tortor mi. Pellentesque vel urna a arcu adipiscing imperdiet vitae sit amet neque. Integer eu lectus et nunc dictum sagittis. Curabitur commodo vulputate fringilla. Sed eleifend, arcu convallis adipiscing congue, dui turpis commodo magna, et vehicula sapien turpis sit amet nisi.',
//         'post_status' => 'publish',
//         'post_type' => 'page'
//       );

//       $result = wp_insert_post($add_default_pages);
//     }

//     $home = get_page_by_title('Home');
//     update_option('show_on_front', 'page');
//     update_option('page_on_front', $home->ID);

//     $home_menu_order = array(
//       'ID' => $home->ID,
//       'menu_order' => -1
//     );
//     wp_update_post($home_menu_order);





// Change permalinks structure
// if (get_option('permalink_structure') !== '/%postname%/') {
//       update_option('permalink_structure', '/%postname%/');
//     }

//     global $wp_rewrite;
//     $wp_rewrite->init();
//     $wp_rewrite->flush_rules();







// Modify SQL query
// http://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts

// function exclude_category( $query ) {
//     if ( $query->is_home ) {
//         $query->set( 'cat', '-1,-1347' );
//     }
// }
// add_action( 'pre_get_posts', 'exclude_category' );