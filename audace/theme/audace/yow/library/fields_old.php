<?php
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/

// Prevent direct access to this file
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

// Fields Generator Class
if ( ! class_exists('YO_Fields'))
{
	class YO_Fields
	{
		/**
		 * Fields information
		 */
		var $fields;

		/**
		 * Contains all field types of current meta box
		 */
		var $types;

		/**
		 *------------------------------------------------------------------------------------------------
		 * Create the form fields based on the parameters
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function __construct($fields = array())
		{
			// Assign meta box values to local variables and add it's missed values
			$this->fields = self::normalize($fields);
			unset($fields);
			
			// Execute this action after the $current_screen has been set by Wordpress
			// add_action('admin_notices', array($this, 'init'));

			// Enqueue common styles and scripts
			add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));

			// new dBug($this->fields);
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Enqueue CSS/Javascript files
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function admin_enqueue_scripts()
		{
			if ( ! self::can_load_assets())
				return;
			
			// Main CSS
			wp_enqueue_style('yo-fields', YO_CSS_URL.'style.css', array('thickbox'), YO_VER);

			// Main Javascript
			wp_enqueue_script('yo-cookie', YO_JS_URL.'cookie.js', array('jquery'), YO_VER);
			wp_enqueue_script('yow', YO_JS_URL.'admin.js', array('jquery'), YO_VER);

			// Load other scripts conditionally
			$has_clone = false;
			$has_toggle = false;
			$has_sortable = false;

			foreach ($this->fields as $field)
			{
				if (isset($field['clone']))
					$has_clone = true;

				if (isset($field['toggle']))
					$has_toggle = true;

				if (isset($field['sortable']))
					$has_sortable = true;

				// Enqueue scripts and styles for fields
				$class = self::get_class_name($field['type']);
				if (method_exists($class, 'admin_enqueue_scripts'))
					call_user_func(array($class, 'admin_enqueue_scripts'));

				// In case it's a group, also load the scripts for its fields
				if ($field['type'] == 'group')
				{
					foreach ($field['fields'] as &$grouped_field)
					{
						// Enqueue scripts and styles for fields
						$class = self::get_class_name($grouped_field['type']);
						if (method_exists($class, 'admin_enqueue_scripts'))
							call_user_func(array($class, 'admin_enqueue_scripts'));
					}
				}
			}

			if ($has_clone)
				wp_enqueue_script('yo-clone', YO_JS_URL.'clone.js', array('jquery'), YO_VER, true);

			if ($has_toggle)
				wp_enqueue_script('yo-toggles', YO_JS_URL.'toggles.js', array('jquery'), YO_VER, true);

			if ($has_sortable) {
				wp_enqueue_script('jquery-ui-core');
				wp_enqueue_script('jquery-ui-sortable');
			}
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Generate the Fields
		 *------------------------------------------------------------------------------------------------
		 *
		 * Loop through the fields, generate them one by one and set their value if present in $values.
		 *
		 */
		public function generate($values = array())
		{
			$output = '';
			
			// Hook before the fields
			do_action('yo_before_fields');

			foreach ($this->fields as $field)
			{
				// Add additional actions for fields
				// $class = self::get_class_name($field['type']);
				// if (method_exists($class, 'add_actions'))
				// 	call_user_func(array($class, 'add_actions'));

				// Hook before each field
				do_action('yo_before_field');

				// Variables used in the filters
				$type = $field['type'];
				$id = $field['id'];

				// Get the fields data, using filters allows to override the values externally
				// TODO: add htmlspecialchars
				$meta = self::apply_field_class_filters($field, 'meta', $values);
				$meta = apply_filters('yo_meta', $meta);
				$meta = apply_filters("yo_{$type}_meta", $meta, $field);
				$meta = apply_filters("yo_{$id}_meta", $meta);

				// Begin HTML filters allow to override the first part of the wrapper
				// 1st filter applies to all fields
				// 2nd filter applies to all fields with the same type
				// 3rd filter applies to current field only
				$begin = self::apply_field_class_filters($field, 'begin_html', '', $meta);
				$begin = apply_filters("yo_begin_html", $begin, $field, $meta);
				$begin = apply_filters("yo_{$type}_begin_html", $begin, $field, $meta);
				$begin = apply_filters("yo_{$id}_begin_html", $begin, $field, $meta);
				

				// Cloneable field
				if (self::is_cloneable($field))
				{
					// Initialize the values
					$meta = (array) $meta;
					$field_html = '';
					$group_counter = 0;

					// Loop through each clone
					foreach ($meta as $meta_data)
					{
						if ($field['type'] == 'group')
						{
							$field['counter'] = $group_counter;
							$group_counter++;
						}

						// Add the delete_clone button to the field if not disabled
						if ( ! isset($field['clone_button']) OR $field['clone_button'] === true)
							add_filter( "yo_{$id}_html", array($this, 'add_delete_clone_button'), 10, 3);

						// Wrap the clone in a div with class "yo-clone"
						$input_html = '<div class="yo-clone clearfix">';

						// Load the 'html' method in the field class and pass the data
						$input_html .= self::apply_field_class_filters($field, 'html', '', $meta_data);

						// Apply filter to field HTML
						// 1st filter applies to all fields with the same type
						// 2nd filter applies to current field only
						// $input_html = apply_filters("yo_{$type}_html", $input_html, $field, $meta_data);
						$input_html = apply_filters("yo_{$id}_html", $input_html, $field, $meta_data);

						$input_html .= '</div>';
						$field_html .= $input_html;
					}

					// Get one rendered field to be used as template, it'll be passed as a Javascript variable
					$field_template = $input_html;

					// Hide the field if the values are empty and the parameter 'empty' is set
					if (self::is_array_empty($meta) && isset($field['empty'])) $field_html = '';
				}
				// Non-cloneable field
				else
				{
					if ($field['type'] == 'group' && isset($meta[0]))
						$meta = $meta[0];

					// Call the 'html' method of each field class
					$field_html = self::apply_field_class_filters($field, 'html', '', $meta);

					// Apply filter to field HTML
					// 1st filter applies to all fields with the same type
					// 2nd filter applies to current field only
					$field_html = apply_filters("yo_{$type}_html", $field_html, $field, $meta);
					$field_html = apply_filters("yo_{$id}_html", $field_html, $field, $meta);
				}


				// Call separated methods for displaying each type of field
				$end = self::apply_field_class_filters($field, 'end_html', '', $meta);

				// Apply filter to field end HTML
				// 1st filter applies to all fields
				// 2nd filter applies to all fields with the same type
				// 3rd filter applies to current field only
				$end = apply_filters("yo_end_html", $end, $field, $meta);
				$end = apply_filters("yo_{$type}_end_html", $end, $field, $meta);
				$end = apply_filters("yo_{$id}_end_html", $end, $field, $meta);

				// Apply filter to field wrapper
				// This allow users to change whole HTML markup of the field wrapper (i.e. table row)
				// 1st filter applies to all fields with the same type
				// 2nd filter applies to current field only
				$html = apply_filters("yo_{$type}_wrapper_html", "{$begin}{$field_html}{$end}", $field, $meta);
				$html = apply_filters("yo_{$id}_wrapper_html", $html, $field, $meta);

				// The base class for each field, other classes can be appended
				$class = 'yo-field';

				// Add the yo-{$type} class for easier styling of fields
				$class = self::add_cssclass('yo-'.$field['type'], $class);

				// Add the field-id as a class to the field container, for easier styling
				$class = self::add_cssclass($field['id'], $class);

				// Add CSS classes if set
				if (isset($field['class']))
					$class = self::add_cssclass($field['class'], $class);

				// Ad the yo-sortable class to sortable items
				// if (isset($field['toggle']))
				// 	$class = self::add_cssclass('yo-toggle', $class);

				// Ad the yo-sortable class to sortable items
				if (isset($field['sortable']))
					$class = self::add_cssclass('yo-sortable', $class);

				// If the 'hidden' argument is set and TRUE, the div will be hidden
				if (isset($field['hidden']) && $field['hidden'])
					$class = self::add_cssclass('hidden', $class);
				
				// The field output
				$output .= "<div class='{$class}'>{$html}</div>";
				
				// If set, pass the field template as a Javascript variable
				if (isset($field_template))
				{
					$field_template = preg_replace("/[\n\r\t]/", '', $field_template); 
					wp_localize_script('yo-clone', $field['id'], $field_template);
				}

				// Allow to add custom code after meta box content
				// 1st action applies to all meta boxes
				// 2nd action applies to only current meta box
				// do_action( "yo_after_{$this->meta_box['id']}" );
				do_action('yo_after_field');
			}

			do_action('yo_after_fields');

			return $output;
		}


		/**
		 * Standard meta retrieval
		 *
		 * @param mixed $meta
		 * @param int	$post_id
		 * @param array $field
		 * @param bool  $saved
		 *
		 * @return mixed
		 */
		static function meta($values, $field)
		{
			$meta = (isset($values[$field['id']])) ? $values[$field['id']] : '';

			// Use $field['std'] only when the meta box hasn't been saved (i.e. the first time we run)
			if (isset($field['std']))
				$meta = ('' === $meta || array() === $meta) ? $field['std'] : $meta;

			// Escape attributes for non-wysiwyg fields
			if ('wysiwyg' !==  $field['type'] && $field['type'] !== 'group')
				$meta = is_array($meta) ? array_map('esc_attr', $meta) : esc_attr($meta);

			return $meta;
		}


		/**
		 * Show begin HTML markup for fields
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		public static function begin_html($html, $meta, $field)
		{
			if (empty($field['name']) && empty($field['desc']))
				return '<div class="yo-input">';

			$name = (isset($field['name'])) ? '<label for="'.$field['name'].'">'.$field['name'].'</label>' : '';
			$desc = ! empty($field['desc']) ? "<p class='yo-description'>{$field['desc']}</p>" : '';
			$heading = '';

			// Add a heading text above the field
			if ( ! empty($field['heading']))
			{
				if (is_array($field['heading']) && count($field['heading']) == 3)
				{
					$heading = $field['heading'][1].$field['heading'][0].$field['heading'][2];
				}
				else
				{
					$heading = '<h2 class="yo-heading">'.$field['heading'].'</h2>';
				}
			}

			$html = "{$heading}<div class='yo-label'>
						{$name}
						{$desc}
					</div>
					<div class='yo-input'>";

			return $html;
		}


		/**
		 * Show end HTML markup for fields
		 *
		 * @param string $html
		 * @param mixed $meta
		 * @param array $field
		 *
		 * @return string
		 */
		public static function end_html($html, $meta, $field)
		{
			$button = '';
			if (self::is_cloneable($field))
			{
				$max_clones = (isset($field['max'])) ? ' data-max-clones="'.$field['max'].'"' : '';
				$label = (isset($field['clone_label'])) ? $field['clone_label'] : __('Add New Item', 'theme_admin');
				$button = '<a href="#" class="yo-button button yo-add-clone" data-clone="'.$field['id'].'"'.$max_clones.'>' . $label . '</a>';		
			}

			// Closes the container
			$html = "{$button}</div>";

			return $html;
		}


		/**
		 * Callback function to add clone buttons on demand
		 * Hooks on the flight into the "yo_{$field_id}_html" filter before the closing div
		 *
		 * @param string $html
		 * @param array  $field
		 * @param mixed  $meta_data
		 *
		 * @return string $html
		 */
		public static function add_delete_clone_button($html, $field, $meta_data)
		{
			$id = $field['id'];
			$button  = '<a href="#" class="yo-button button-secondary yo-remove-clone" title="'.__('Delete Item', 'theme_admin').'" data-unclone="'.$field['id'].'">&ndash;</a>';

			return "{$html}{$button}";
		}


		/**************************************************
			HELPER FUNCTIONS
		**************************************************/

		/**
		 * Normalize parameters for meta box
		 *
		 * @param array $meta_box Meta box definition
		 *
		 * @return array $meta_box Normalized meta box
		 */
		public static function normalize($fields)
		{
			echo '<pre>';
			print_r($fields);
			echo '</pre>';
			// $group_counter = 0;

			// Set default values for fields
			foreach ($fields as &$field)
			{				
				$clone 	  = isset($field['clone']) ? $field['clone'] : false;
				$group 	  = isset($field['group']) ? $field['group'] : false;
				$multiple = in_array($field['type'], array(/*'group',*/ 'checkbox_list', 'file', 'image', 'plupload_image'));
				// $multiple = (isset($field['multiple'])) ? $field['multiple'] : $multiple;
				$std      = $multiple ? array() : '';
				$format   = ('date' === $field['type']) ? 'yy-mm-dd' : ('time' === $field['type'] ? 'hh:mm' : '');
				
				$field = wp_parse_args($field, array(
					'multiple' 	=> $multiple,
					'clone' 	=> $clone,
					/*'group'		=> $group,*/
					'std'      	=> $std,
					'desc'     	=> '',
					'format'   	=> $format
				));

				// yo_debug($field['id'].' /// '.$field['multiple'].'<br>');

				// Execute a custom query to fill the options if it's not an array of values
				if (isset($field['options']) && ! is_array($field['options']))
				{
					$defaults = array(
					   'orderby' => 'title',
					   'order'=> 'ASC',
					   'post_type' => 'post',
					   'genre' => false,
					   'post_status' => 'publish'
					);

					// Merge the passed parameters with the default values
					$args = wp_parse_args($field['options'], $defaults);

					// Get the posts
					$posts = get_posts($args);

					// This array will store the options
					$options = array('' => '');

					foreach ($posts as $post)
					{
						setup_postdata($post);
						$options[$post->ID] = $post->post_title;
					}

					// Set the options
					$field['options'] = $options;
					unset($options);
				}

				// Check if the field belongs to a group
				if (isset($field['group']) && $field['group'] != false)
				{
					$field['counter'] = 0;
				}
				else
				{
					$field['field_name'] = $field['id'] . ($field['multiple'] || $field['clone'] ? '[]' : '');
				}

				// Allow field class add/change default field values
				$field = self::apply_field_class_filters($field, 'normalize_field', $field);

				// Add actions defined in the fields
				$class = self::get_class_name($field['type']);
				if (method_exists($class, 'add_actions'))
					call_user_func(array($class, 'add_actions'));

				// if (isset($field['parent'])) echo '<br>---------<br>ID: '.$field['id'].'<br>Parent: ' . $field['parent'];

				// In case it's a group, also add the actions for its fields
				if ($field['type'] == 'group')
				{
					foreach ($field['fields'] as &$grouped_field)
					{
						$class = self::get_class_name($grouped_field['type']);
						if (method_exists($class, 'add_actions'))
							call_user_func(array($class, 'add_actions'));

						// if ($grouped_field['type'] == 'group')
							// $grouped_field['fields'] = self::normalize($grouped_field['fields']);
					}
				}
			}

			return $fields;
		}


		// Recursively check if the array is actually empty
		public static function is_array_empty($array)
		{
		   $result = true;

		   if (is_array($array) && count($array) > 0)
		   {
		      foreach ($array as $value)
		         $result = $result && self::is_array_empty($value);
		   }
		   else
		   {
		      $result = empty($array);
		   }

		   return $result;
		}


		/**
		 * Apply filters by field class, fallback to YO_Fields method
		 *
		 * @param array  $field
		 * @param string $method_name
		 * @param mixed  $value
		 *
		 * @return mixed $value
		 */
		static function apply_field_class_filters($field, $method_name, $value)
		{
			$args	= array_slice(func_get_args(), 2);
			$args[]	= $field;

			// The field class
			$class = self::get_class_name($field['type']);
			// The caller
			$caller = self::get_caller_class();

			// Search the requested method in the field class
			if (method_exists($class, $method_name))
			{
				$value = call_user_func_array(array($class, $method_name), $args);
			}
			// Then look in the caller class
			elseif (method_exists($caller, $method_name))
			{
				$value = call_user_func_array(array($caller, $method_name), $args);
			}
			// Otherwise look in the current class
			elseif (method_exists(__CLASS__, $method_name))
			{
				$value = call_user_func_array(array(__CLASS__, $method_name), $args);
			}

			return $value;
		}


		/**
		 * Call field class method for actions, fallback to RW_Meta_Box method
		 * TODO: merge with apply_field_class_filters(), avoid repeating same code
		 *
		 * @param array  $field
		 * @param string $method_name
		 *
		 * @return mixed
		 */
		static function do_field_class_actions($field, $method_name)
		{
			$args   = array_slice( func_get_args(), 2 );
			$args[] = $field;

			$class = self::get_class_name( $field['type'] );
			$caller = self::get_caller_class();
			
			if (method_exists($class, $method_name))
			{
				call_user_func_array(array($class, $method_name), $args);
			}
			elseif (method_exists($caller, $method_name))
			{
				$value = call_user_func_array(array($caller, $method_name), $args);
			}
			elseif (method_exists(__CLASS__, $method_name))
			{
				call_user_func_array(array(__CLASS__, $method_name), $args);
			}
		}


		static function get_caller_class()
		{
		    $traces = debug_backtrace();

		    if (isset($traces[2]))
		        return $traces[2]['class'];

		    return null;
		}


		/**
		 * Get field class name
		 *
		 * @param string $type Field type
		 *
		 * @return bool|string Field class name OR false on failure
		 */
		static function get_class_name($type)
		{
			$type	= ucwords($type);
			$class	= "YO_{$type}_Field";

			if ( class_exists($class))
				return $class;

			return false;
		}


		/**
		 * Format Ajax response
		 *
		 * @param string $message
		 * @param string $status
		 *
		 * @return void
		 */
		static function ajax_response($message, $status)
		{
			$response = array('what' => 'meta-box');
			$response['data'] = 'error' === $status ? new WP_Error('error', $message) : $message;
			$x = new WP_Ajax_Response($response);
			$x->send();
		}


		/**
		 * Adds a css class
		 * Mainly a copy of the core admin menu function
		 * As the core function is only meant to be used by core internally,
		 * We copy it here - in case core changes functionality or drops the function.
		 *
		 * @param string $add
		 * @param string $class | Class name - Default: empty
		 *
		 * @return string $class
		 */
		static function add_cssclass($add, $class = '')
		{
			$class .= empty( $class ) ? $add : " {$add}";
			return $class;
		}



		/**
		 * Helper function to check for multi/clone field IDs
		 *
		 * @param  array $field
		 *
		 * @return bool False if no cloneable
		 */
		static function is_cloneable($field)
		{
			return (isset($field['clone'])) ? $field['clone'] : false;
		}



		/**
		 * Helper function to check the current page and load the assets when necessary.
		 * TODO: make this configurable
		 *
		 * @param  array $whitelist List of the allowed pages for the assets to be loaded
		 *
		 * @return bool False if the the current page is not in the $whitelist
		 */
		static function can_be_loaded($whitelist = array())
		{
			global $current_screen;
			// new dBug($current_screen);

			$whitelist = array(	'edit-post',
								'post',
								'edit-portfolio',
								'portfolio',
								'edit-page',
								'edit',
								'page',
								'appearance_page_yo-theme-options',
								'settings_page_yo-shortcodes-editor',
								'portfolio_page_type-portfolio');

			$screen = get_current_screen();
			// echo 'PAGE-ID: '.$screen->id.' ';
			
			if (in_array($screen->id, $whitelist))
				return true;

			return false;
		}

		/**
		 * Helper function to check the current page and load the assets when necessary.
		 * TODO: make this configurable
		 *
		 * @param  array $whitelist List of the allowed pages for the assets to be loaded
		 *
		 * @return bool False if the the current page is not in the $whitelist
		 */
		static function can_load_assets($whitelist = array())
		{
			return self::can_be_loaded();
		}
	}
}
