<?php
/*
Title		: Yow Framework - Generator of Fields, Meta Boxes, Theme Options, Shortcodes
Description	: Yow is an extendable WordPress admin suite that helps generating form fields, meta boxes, theme options and shortcodes.
Version		: 1.0.0
Author		: Giordano Piazza
Author URI	: http://giordanopiazza.com
License		: GPLv2+
Credits		: Meta Box Script - http://www.deluxeblogtips.com/meta-box/
			  Slightly Modified Options Framework - https://github.com/sy4mil/Options-Framework
			  Thematic Options Panel - http://wptheming.com/2010/11/thematic-options-panel-v2/
		 	  Woo Themes - http://woothemes.com/
		 	  Option Tree - http://wordpress.org/extend/plugins/option-tree/
*/

// Prevent loading this file directly
if (!class_exists('WP')) { header('Status: 403 Forbidden'); header('HTTP/1.1 403 Forbidden'); exit; }

// Theme Options Generator Class
if ( ! class_exists('YO_Options'))
{
	class YO_Options
	{
		/**
		 * Options
		 */
		var $options = array();

		/**
		 * Fields information
		 */
		var $fields = array();

		/**
		 * Default values
		 */
		var $defaults = array();


		/**
		 * ------------------------------------------------------------------------------------------------
		 * Options Class Constructor
		 * ------------------------------------------------------------------------------------------------
		 *
		 */
		public function __construct($options)
		{
			// Check capabilities
			if ( ! is_admin() || ! current_user_can('edit_theme_options'))
				return;

			// Set the options in the class instance
			$this->options = $options;

			foreach ($this->options as &$option)
				array_push($this->fields, $option['fields']);

			// $this->fields = &$this->options['fields'];

			// Enqueue common styles and scripts
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));

			// Add the admin menu item
			add_action('admin_menu', array(&$this, 'register_admin_menu'));

			// Hook the ajax save method
			add_action('wp_ajax_yo_ajax_post_action', array(&$this, 'save'));			
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Enqueue CSS/Javascript files
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function admin_enqueue_scripts()
		{
			
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Register Admin Menu
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function register_admin_menu()
		{
    		add_theme_page(wp_get_theme(), 'Theme Options', 'edit_theme_options', 'yo-theme-options', array(&$this, 'render_theme_options'));
		}


		/**
		 *------------------------------------------------------------------------------------------------
		 * Render Theme Options Page
		 *------------------------------------------------------------------------------------------------
		 *
		 * @return void
		 */
		public function render_theme_options()
		{
			// Get all the stored theme options in one array for performance
	    	$options_data = get_option(THEME_OPTIONS);

	    	// Initialize the output variables
			$tabs_output = '';
			$fields_output = '';

			// Build the options
			foreach ($this->options as &$option)
			{
				// Add the tabs
				$tabs_output .= '<li class="'.$option['id'].'"><a href="#yo-option-'.$option['id'].'" title="'.$option['name'].'">'.$option['name'].'</a></li>';
			
				// Create the fields
				$fields = new YO_Fields($option['fields']);

	    		// This will store the fields values
				$values = array();

				// Set the current values if any, and build an array with the defaults
				foreach ($option['fields'] as $field)
				{
					// Get the current stored value if any
					$values[$field['id']] = YO_Fields::apply_field_class_filters($field, 'meta', $options_data);

					// Creating default values
					if ($field['type'] == 'multicheck')
					{
						if (is_array($field['std']))
						{
							foreach($field['std'] as $i=>$key)
								$this->defaults[$field['id']][$key] = true;
						}
						else
						{
							$this->defaults[$field['id']][$field['std']] = true;
						}
					}
					else
					{
						if (isset($field['std'])) $this->defaults[$field['id']] = $field['std'];
					}
				}
				// Wrap all the fields in a div
				$fields_output .= '<div id="yo-option-'.$option['id'].'">';
				// Generate the fields passing the $values
				$fields_output .= $fields->generate($values);
				// Close the div
				$fields_output .= '</div>';				
			}

			// Make sure all the scripts have been loaded
			do_action('admin_enqueue_scripts');


			// Set the variables for the template
			$theme_options_tabs = $tabs_output;
			$theme_options_fields = $fields_output;

			// Load the template file
			include_once(YO_DIR . 'options.php');
		}


		/**
		 * Standard meta retrieval
		 *
		 * @param mixed $data
		 * @param array $field
		 *
		 * @return mixed
		 */
		// public static function meta($data, $field)
		// {
		// 	// Search for the field value in the stored options
		// 	$meta = (isset($data[$field['id']])) ? $data[$field['id']] : '';

		// 	// Use $field['std'] only when the value hasn't been saved (i.e. the first time we run)
		// 	$meta = (isset($field['std']) && ('' === $meta || array() === $meta)) ? $field['std'] : $meta;

		// 	// Escape attributes for non-wysiwyg fields
		// 	if ('wysiwyg' !==  $field['type'])
		// 		$meta = is_array( $meta ) ? array_map( 'esc_attr', $meta ) : esc_attr( $meta );

		// 	return $meta;
		// }


		/**
		 *------------------------------------------------------------------------------------------------
		 * Ajax save options
		 *------------------------------------------------------------------------------------------------
		 *
		 *
		 */
		public function save() 
		{
			$nonce = $_POST['security'];
			
			if ( ! wp_verify_nonce($nonce, 'yo_ajax_nonce')) die('-1'); 
					
			// Get options array from db
			$all = get_option(THEME_OPTIONS);
			
			$save_type = $_POST['type'];

			switch ($save_type)
			{
				case 'upload':
					$clickedID = $_POST['data']; // Acts as the name
					$filename = $_FILES[$clickedID];
			       	$filename['name'] = preg_replace('/[^a-zA-Z0-9._\-]/', '', $filename['name']); 
					
					$override['test_form'] = false;
					$override['action'] = 'wp_handle_upload';    
					$uploaded_file = wp_handle_upload($filename,$override);
					
					$upload_tracking[] = $clickedID;
						
					//update $options array w/ image URL			  
					$upload_image = $all; //preserve current data
					$upload_image[$clickedID] = $uploaded_file['url'];
					
					update_option(THEME_OPTIONS, $upload_image);
							
					if(!empty($uploaded_file['error'])) {echo 'Upload Error: ' . $uploaded_file['error']; }	
					else { /*yo_debug($upload_image);*/ echo $uploaded_file['url']; } // Is the Response
					break;

				case 'image_reset':
					$id = $_POST['data']; // Acts as the name
					$delete_image = $all; //preserve rest of data
					$delete_image[$id] = ''; //update array key with empty value	 
					update_option(THEME_OPTIONS, $delete_image ) ;
					break;

				case 'backup_options':
					$backup = $all;
					$backup['backup_log'] = date('r');	
					update_option(BACKUPS, $backup ) ;
					die('1'); 
					break;

				case 'restore_options':
					$options_data = get_option(BACKUPS);	
					update_option(THEME_OPTIONS, $options_data);
					die('1'); 
					break;

				case 'import_options':
					$options_data = $_POST['data'];
					$options_data = unserialize(base64_decode($options_data)); // 100% safe - ignore theme check nag
					update_option(THEME_OPTIONS, $options_data);
					die('1'); 
					break;

				case 'save':
					wp_parse_str(stripslashes($_POST['data']), $options_data);
					unset($options_data['security']);
					// unset($options_data['of_save']);
					update_option(THEME_OPTIONS, $options_data);
					// $options_data = json_encode($options_data);
					// die($options_data);
					die('1');
					break;

				case 'reset':
					update_option(THEME_OPTIONS, $this->defaults);
		        	die('1'); //options reset
					break;

				default:
					die();
					break;
			}

		  	die();
		}
	}
}