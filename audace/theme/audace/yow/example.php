<?php

/*
 *------------------------------------------------------------------------------------------------
 * Meta Box Definitions
 *------------------------------------------------------------------------------------------------
 *
 * All the meta boxes are defined here.
 * 
 */

// global $ustom_metabox_fields;

$prefix = 'yo_';

$my_fields = array();

$my_fields[] = array(
        //----------------------------------------------------------------------------------------
        // TEXT
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Text field',
            'id'        => $prefix.'text',
            'desc'      => 'Text Description',
            'type'      => 'text',
            'std'       => '',  
            // 'clone'     => true,
            // 'sortable'  => true
        ),
        //----------------------------------------------------------------------------------------
        // TEXTAREA
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Textarea',
            'id'        => $prefix.'textarea',
            'desc'      => 'Textarea Description',
            'type'      => 'textarea',
            'std'       => '',
            'heading'   => 'Hello this is a metabox'
        ),
        //----------------------------------------------------------------------------------------
        // SELECT BOX (Dropdown)
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Selectbox (Dropdown)',
            'id'        => $prefix.'selectbox',
            'desc'      => 'Selectbox description',
            'type'      => 'image',
            'mod'       => 'chosen',
            // 'class'     => 'yo-chosen',
            // Array of 'key' => 'value' pairs for select box
            'options'   => array(
                'it' => 'Italy',
                'es' => 'Spain'),
            // Default value, can be string (single value) or array (for both single and multiple values)
            'std'       => array( 'it' ),
        ),
        //----------------------------------------------------------------------------------------
        // MULTIPLE SELECT BOX (Dropdown)
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Selectbox (Dropdown)',
        //     'id'        => $prefix.'multiple_selectbox',
        //     'desc'      => 'Selectbox description',
        //     'type'      => 'select',
        //     'multiple'  => true,
        //     // Array of 'key' => 'value' pairs for select box
        //     'options'   => array(
        //         'it' => 'Italy',
        //         'es' => 'Spain'),
        //     // Default value, can be string (single value) or array (for both single and multiple values)
        //     'std'       => array( 'it' ),
        // ),
        //----------------------------------------------------------------------------------------
        // CHECKBOX
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Checkbox',    // File type: checkbox
            'id'        => $prefix.'checkbox',
            'desc'      => 'Checkbox description',
            'type'      => 'checkbox',
            // Value can be 0 or 1
            'std'       => 1
        ),
        //----------------------------------------------------------------------------------------
        // RADIO
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Radio',
            'id'        => $prefix.'radio',
            'desc'      => 'Radio description',
            'type'      => 'radio',
            // Array of 'key' => 'value' pairs for select box
            'options'   => array(
                'it' => 'Italy',
                'es' => 'Spain'),
            // Value can be 0 or 1
            'std'       => 'it'
        ),
        //----------------------------------------------------------------------------------------
        // HIDDEN
        //----------------------------------------------------------------------------------------
        // array(
        //     'id'        => $prefix.'hidden',
        //     'type'      => 'hidden',
        //     // Hidden field must have predefined value
        //     'std'       => 'This is a hidden value'
        // ),
        //----------------------------------------------------------------------------------------
        // PASSWORD
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Password',
            'id'        => $prefix.'password',
            'type'      => 'password'
        ),
        //----------------------------------------------------------------------------------------
        // DATE
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Date',
            'id'        => $prefix.'date',
            'desc'      => 'Date Description',
            'type'      => 'date',
        ),
        //----------------------------------------------------------------------------------------
        // TOGGLE CHECKBOX
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Toggle Checkbox',
            'id'        => $prefix.'toggle',
            'type'      => 'checkbox',
            'desc'      => 'Toggle description',
            'toggle'    => "#yo-section-{$prefix}toggable_content",
            'heading'   => 'Default Heading Text',
            // 'class'     => 'yo-toggle'
            // Value can be 0 or 1
            // 'std'       => 1
        ),
        //----------------------------------------------------------------------------------------
        // TOGGABLE TEXT
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Toggable text',
            'id'        => $prefix.'toggable_content',
            'desc'      => 'Text Description',
            'type'      => 'text',
            'std'       => ''
        ),
        //----------------------------------------------------------------------------------------
        // MEDIA UPLOAD
        //----------------------------------------------------------------------------------------
        array(
            'name'  => 'Media',
            'id'    => $prefix.'media',
            'desc'  => 'Media upload description',
            'type'  => 'media',
            'mod'   => 'min'
        ),
);

$my_fields[] = array(
        //----------------------------------------------------------------------------------------
        // GOOGLE FONTS
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Google Fonts',
            'id'        => $prefix.'google_fonts',
            'desc'      => 'Google Fonts Description',
            'type'      => 'google_fonts',
            'preview'   => true,
        ),
        //----------------------------------------------------------------------------------------
        // GROUP
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Group',
            'id'        => $prefix.'group',
            'desc'      => 'Group description',
            'type'      => 'group',
            'class'     => 'test-group',
            'clone'     => true,
            // 'sortable'  => true,
            // 'empty'     => true,
            'fields'    => array(
                // array(
                //     // 'name'  => 'Group Media',
                //     'id'    => 'media_item',
                //     // 'desc'  => 'Group media upload description',
                //     'type'  => 'media'
                // ),
                //----------------------------------------------------------------------------------------
                // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
                //----------------------------------------------------------------------------------------
                // array(
                //     'name'  => 'Plupload',
                //     'id'    => 'plupload',
                //     'desc'  => 'Drag and drop uploader description.',
                //     'type'  => 'plupload',
                //     // 'max_file_uploads' => 4,
                // ),
                array(
                    'name'      => 'Title',
                    'id'        => 'title',
                    // 'desc'      => 'Group Text Description 1',
                    'type'      => 'text',
                ),
                array(
                    'name'      => 'Caption',
                    'id'        => 'caption',
                    // 'desc'      => 'Group Text Description 2',
                    'type'      => 'text',
                ),
                array(
                    'name'      => 'Caption Type',
                    'id'        => 'caption_type',
                    // 'desc'      => 'Selectbox description',
                    'type'      => 'select',
                    // Array of 'key' => 'value' pairs for select box
                    'std'       => 'small_caption',
                    'options'   => array(
                        'none'           => 'No caption',
                        'small-caption'  => 'Small bottom caption',
                        'medium-caption' => 'Bottom caption full width',
                        'big-caption'    => 'Large centered caption'
                        ),
                ),           
            )
        ),
        //----------------------------------------------------------------------------------------
        // NESTED GROUPS
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Nested Groups',
            'id'        => $prefix.'nested_groups',
            'desc'      => 'Nested Groups description',
            'type'      => 'group',
            // 'clone'     => true,
            // 'sortable'  => true,
            // 'empty'     => true,
            'fields'    => array(
                // array(
                //     // 'name'  => 'Group Media',
                //     'id'    => 'media_item',
                //     // 'desc'  => 'Group media upload description',
                //     'type'  => 'media'
                // ),
                array(
                    'name'      => 'Title',
                    'id'        => 'title',
                    // 'desc'      => 'Group Text Description 1',
                    'type'      => 'text',
                ),
                array(
                    'name'      => 'Nested Group',
                    'id'        => 'nested_group',
                    'type'      => 'group',
                    // 'multiple'  => true,
                    'clone'     => true,
                    // 'sortable'  => true,
                    'empty'     => true,
                    'fields'    => array(
                        array(
                            'name'      => 'Field 1',
                            'id'        => 'field1',
                            // 'desc'      => 'Group Text Description 1',
                            'type'      => 'text',
                        ),
                        array(
                            'name'      => 'Field 2',
                            'id'        => 'field2',
                            // 'desc'      => 'Group Text Description 1',
                            'type'      => 'text',
                        ),
                        // array(
                        //     'name'      => 'Nested Group',
                        //     'id'        => 'nested_group23',
                        //     'type'      => 'group',
                        //     // 'multiple'  => true,
                        //     'clone'     => true,
                        //     // 'sortable'  => true,
                        //     // 'empty'     => true,
                        //     'fields'    => array(
                        //         array(
                        //             'name'      => 'Field A',
                        //             'id'        => 'f1',
                        //             // 'desc'      => 'Group Text Description 1',
                        //             'type'      => 'text',
                        //         )
                        //     )
                        // ),
                        // array(
                        //     'name'      => 'Nested Group',
                        //     'id'        => 'nested_groupBB',
                        //     'type'      => 'group',
                        //     // 'multiple'  => true,
                        //     'clone'     => true,
                        //     // 'sortable'  => true,
                        //     // 'empty'     => true,
                        //     'fields'    => array(
                        //         array(
                        //             'name'      => 'Field A2',
                        //             'id'        => 'f1',
                        //             // 'desc'      => 'Group Text Description 1',
                        //             'type'      => 'text',
                        //         )
                        //     )
                        // ),
                    )
                ),
            )
        ),
        //----------------------------------------------------------------------------------------
        // SLIDER
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Slider',
        //     'id'        => $prefix.'slider',
        //     'type'      => 'slider',
        //     'min'   => '0',  
        //     'max'   => '100',  
        //     'step'  => '5'
        // ),
        //----------------------------------------------------------------------------------------
        // PANELS
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Panels',
        //     'id'        => $prefix.'panels',
        //     'desc'      => 'Panels description',
        //     'type'      => 'panel',
        //     // 'multiple'  => true
        // ),
        //----------------------------------------------------------------------------------------
        // ATTACHMENTS
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Attachments',
        //     'id'        => $prefix.'attachments',
        //     'desc'      => 'Choose the images or videos you want to show.',
        //     // 'label'     => __('Choose Image', 'theme_admin'),
        //     'type'      => 'attachments',
        //     'std'       => '',
        // ),
        //----------------------------------------------------------------------------------------
        // WYSIWYG/RICH TEXT EDITOR
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'  => 'Wysiwyg',
        //     'id'    => $prefix.'wysiwyg',
        //     'type'  => 'wysiwyg',
        //     'std'   => '',
        //     'desc'  => 'Editor description'
        // ),
        //----------------------------------------------------------------------------------------
        // FILE UPLOAD
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'  => 'File',
        //     'id'    => $prefix.'file',
        //     'desc'  => 'File upload description',
        //     'type'  => 'file'
        // ),
        //----------------------------------------------------------------------------------------
        // IMAGE UPLOAD
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'  => 'Image',
        //     'id'    => $prefix.'image',
        //     'desc'  => 'Screenshots of problems, warnings, etc.',
        //     'type'  => 'image'
        // ),
);

$my_fields[] = array(
        //----------------------------------------------------------------------------------------
        // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
        //----------------------------------------------------------------------------------------
        array(
            'name'  => 'Plupload',
            'id'    => $prefix.'plupload',
            'desc'  => 'Drag and drop uploader description.',
            'type'  => 'plupload',
            // 'max_file_uploads' => 4,
        ),
        //----------------------------------------------------------------------------------------
        // GROUP
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Group',
        //     'id'        => $prefix.'dragdrop_group',
        //     'desc'      => 'Group description',
        //     'type'      => 'group',
        //     // 'class'     => 'test-group',
        //     // 'clone'     => true,
        //     // 'sortable'  => true,
        //     // 'empty'     => true,
        //     'fields'    => array(
        //         //----------------------------------------------------------------------------------------
        //         // DROPPED ITEMS
        //         //----------------------------------------------------------------------------------------
        //         array(
        //             'name'      => 'Group',
        //             'id'        => $prefix.'dragdrop_group',
        //             'desc'      => 'Group description',
        //             'type'      => 'group',
        //             'export_template' => true,
        //             'empty'     => true,
        //             'fields'    => array(
        //                 array(
        //                     'id'    => 'drop-target',
        //                     'type'  => 'hidden',
        //                     'std'   => ''
        //                 ),
        //                 array(
        //                     'id'    => 'title',
        //                     'type'  => 'text',
        //                 )
        //             )
        //         ),
        //         //----------------------------------------------------------------------------------------
        //         // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
        //         //----------------------------------------------------------------------------------------
        //         array(
        //             'name'   => 'Plupload',
        //             'id'     => 'plupload2',
        //             'desc'   => 'Drag and drop uploader description.',
        //             'type'   => 'plupload',
        //             'target' => $prefix.'dragdrop_group'
        //             // 'max_file_uploads' => 4,
        //         ),
        //     )
        // ),
        //----------------------------------------------------------------------------------------
        // GROUP
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Plupload Group',
            'id'        => $prefix.'plupload_group',
            'desc'      => 'Group description',
            'type'      => 'group',
            // 'class'     => 'test-group',
            'clone'     => true,
            'clone_button' => false,
            'sortable'  => true,
            'empty'     => true,
            'plupload'  => true,
            'class'     => 'yo-custom-thumbs yo-plupload',
            'fields'    => array(
                array(
                    'name'  => 'Title',
                    'id'    => 'title',
                    'type'  => 'text',
                    'class' => 'yo-tooltip-item',
                ),
                array(
                    'name'  => 'Description',
                    'id'    => 'description',
                    'type'  => 'text',
                    'class' => 'yo-tooltip-item',
                ),
                array(
                    'name'  => 'Category',
                    'id'    => 'category',
                    'type'  => 'select',
                    'class' => 'yo-tooltip-item',
                    'options' => array(
                        '1' => 'Option 1',
                        '2' => 'Option 2',
                    )
                ),
                array(
                    'id'        => 'media',
                    'type'      => 'image',
                    'bar'       => true,
                    'class'     => 'yo-receiver',
                    'receiver'  => true
                )
            )
        ),
        //----------------------------------------------------------------------------------------
        // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'  => 'Plupload2',
        //     'id'    => $prefix.'plupload2',
        //     'desc'  => 'Drag and drop uploader description.',
        //     'type'  => 'plupload',
        //     // 'max_file_uploads' => 4,
        // ),

        //----------------------------------------------------------------------------------------
        // COLOR
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Color',
            'id'        => "{$prefix}color",
            'type'      => 'color'
        ),
        //----------------------------------------------------------------------------------------
        // CHECKBOX (List)
        //----------------------------------------------------------------------------------------
        array(
            'name'      => 'Checkbox List',
            'id'        => $prefix.'checbox_list',
            'type'      => 'checkbox_list',
            // Options of checkboxes, in format 'key' => 'value'
            'options'   => array(
                'reading'   => 'Books',
                'sport'     => 'Gym, Boxing'
            ),
            'desc'      => 'What do you do in free time?'
        ),
        //----------------------------------------------------------------------------------------
        // TIME
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Time',
        //     'id'        => $prefix.'time',
        //     'type'      => 'time',
        //     // Time format, default hh:mm. Optional. @link See: http://goo.gl/hXHWz
        //     'format'    => 'hh:mm:ss'
        // ),
        //----------------------------------------------------------------------------------------
        // DATETIME
        //----------------------------------------------------------------------------------------
        // array(
        //     'name'      => 'Datetime',
        //     'id'        => $prefix.'datetime',
        //     'type'      => 'datetime',
        //     // Time format, default hh:mm. Optional. @link See: http://goo.gl/hXHWz
        //     'format'    => 'hh:mm:ss'
        // )
    );



// $test_fields = array();

// $test_fields[] = array(
//         //----------------------------------------------------------------------------------------
//         // SLIDER
//         //----------------------------------------------------------------------------------------
//         // array(
//         //     'name'      => 'Slider',
//         //     'id'        => $prefix.'slider',
//         //     'type'      => 'slider',
//         //     'min'   => '0',  
//         //     'max'   => '100',  
//         //     'step'  => '5'
//         // ),
//         //----------------------------------------------------------------------------------------
//         // CHECKBOX (List)
//         //----------------------------------------------------------------------------------------
//         // array(
//         //     'name'      => 'Checkbox List',
//         //     'id'        => $prefix.'checbox_list',
//         //     'type'      => 'checkbox_list',
//         //     // Options of checkboxes, in format 'key' => 'value'
//         //     'options'   => array(
//         //         'reading'   => 'Books',
//         //         'sport'     => 'Gym, Boxing'
//         //     ),
//         //     'desc'      => 'What do you do in free time?',
//         //     'multiple'  => true
//         // ),
//         //----------------------------------------------------------------------------------------
//         // TEXT
//         //----------------------------------------------------------------------------------------
//         array(
//             'name'      => 'Text field',
//             'id'        => $prefix.'text',
//             'desc'      => 'Text Description',
//             'type'      => 'text',
//             'std'       => '',
//             'clone'     => true
//         ),
//         //----------------------------------------------------------------------------------------
//         // MEDIA UPLOAD
//         //----------------------------------------------------------------------------------------
//         array(
//             'name'  => 'Media',
//             'id'    => $prefix.'media',
//             'desc'  => 'Media upload description',
//             'type'  => 'media',
//             'mod'   => 'min'
//         ),
//         //----------------------------------------------------------------------------------------
//         // GROUP
//         //----------------------------------------------------------------------------------------
//         array(
//             'name'      => 'Group',
//             'id'        => $prefix.'group',
//             'desc'      => 'Group description',
//             'type'      => 'group',
//             'class'     => 'test-group',
//             'clone'     => true,
//             'sortable'  => true,         
//             'fields'    => array(
//                 array(
//                     'name'      => 'Group Text field 1',
//                     'id'        => 'text1a',
//                     // 'desc'      => 'Group Text Description 1',
//                     'type'      => 'text',
//                 ),
//                 array(
//                     'name'  => 'Group Media',
//                     'id'    => 'media',
//                     // 'desc'  => 'Group media upload description',
//                     'type'  => 'media',
//                     'class' => 'test-media',
//                 ),
//                 array(
//                     'name'      => 'Group Text field 2',
//                     'id'        => 'text2a',
//                     // 'desc'      => 'Group Text Description 2',
//                     'type'      => 'text',
//                 )
//             )
//         )
// );





// global $metaboxes;

// 
$metaboxes = array();

// Project details 1
$metaboxes[] = array(
    'id' => 'project-details1',
    'title' => 'Project Details 1',
    'pages' => array('post', 'portfolio'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'fields' => $my_fields[0]
);

// Project details 2
$metaboxes[] = array(
    'id' => 'project-details2',
    'title' => 'Project Details 2',
    'pages' => array('post', 'portfolio'),
    'context' => 'advanced',
    'priority' => 'high',
    'fields' => $my_fields[1]
);

// Project details 3
$metaboxes[] = array(
    'id' => 'project-details3',
    'title' => 'Project Details 3',
    'pages' => array('post', 'portfolio'),
    'context' => 'normal',
    'priority' => 'high',
    'fields' => $my_fields[2]
);


yo_register_metabox($metaboxes);





// $fields = new YO_Fields($my_fields);

// $values = array('id_field' => 'xxx', 'id2' => 'asdasd');

// echo $fields->generate($values);



// $theme_options[] = array(   "name"  => "General Settings",
//                             'id'    =>  '',
//                             "type"  => "heading",
//                             'icon'  => 'none');

// // $theme_options[] = array(   "name" => "Hello there!",
// //                             "desc" => "",
// //                             "id" => "introduction",
// //                             "std" => "<h3 style=\"margin: 0 0 10px;\">Welcome to the Theme Options.</h3>
// //                             Here you can configure the theme options and change the website's look and feel. On the left you'll find the settings tabs to navigate through the various panels. To visualize all the settings in one page, click on the little 'layout' icon above the settings tabs.",
// //                             "icon" => true,
// //                             "type" => "info");

// // $theme_options[] = array(   "name" => "Logo",
// //                             "desc" => "This is the logo at the original size, leave it empty if you want to use the default title text.",
// //                             "id" => "logo",
// //                             "std" => "",
// //                             "mod" => "min",
// //                             "type" => "media");

// $theme_options[] = array(   "name" => "Custom CSS",
//                             "desc" => "Paste here the CSS code you wish to include in all pages. You don't need to add any style tags.",
//                             "id" => "of_custom_css",
//                             "std" => "",
//                             "type" => "text"); 


//
$theme_options = array();

// Theme Options
$theme_options[] = array(
    'name'   => 'General Settings',
    'id'     => 'generalsettings',
    'icon'   => 'none',
    'fields' => $my_fields[0]
);

$theme_options[] = array(
    'name'   => 'Advanced Settings',
    'id'     => 'advancedsettings',
    'icon'   => 'none',
    'fields' => $my_fields[1]
);

$theme_options[] = array(
    'name'   => 'Extra Settings',
    'id'     => 'extrasettings',
    'icon'   => 'none',
    'fields' => $my_fields[2]
);

// TODO: Add config array with menu label, position etc...

yo_register_options($theme_options);



// unset($my_fields);
// unset($metaboxes);
// unset($theme_options);
