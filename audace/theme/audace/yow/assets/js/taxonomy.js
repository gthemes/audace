jQuery( document ).ready( function($)
{
	$( '.yo-taxonomy-tree input:checkbox' ).change( function()
	{
		var $this = $( this ),
			$childList = $this.parent().siblings( '.yo-taxonomy-tree' );
		if ( $this.is( ':checked' ) )
			$childList.removeClass( 'hidden' );
		else
		{
			$('input', $childList).removeAttr('checked');
			$childList.addClass( 'hidden' );
		}
	} );
	
	$( '.yo-taxonomy-tree select' ).change( function()
	{
		var $this = $( this ),
			$childList = $this.parent().find( 'div.yo-taxonomy-tree' ),
			$value = $this.val();
		$childList.removeClass('active').addClass('disabled').find('select').each(function(){
			$(this).val($('options:first', this).val()).attr("disabled", "disabled")
		});
		$childList.filter('#yo-taxonomy-' + $value).removeClass('disabled').addClass('active').children('select').removeAttr('disabled');
		
	} );
} );