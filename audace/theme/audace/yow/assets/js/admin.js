/**
 * Yow Framework
 *
 */

jQuery.noConflict();


jQuery(document).ready(function($){


	// Hides warning if Javascript is enabled
	$('#js-warning').hide();


	/* Tabs
	======================================================================== */

	// Show object and hide siblings
	function switch_tabs(obj) {
		$('.yo-tabs a').removeClass("active");
		var $current_tab = $(obj.attr('href'));
		$current_tab.siblings('div').hide();
		$current_tab.fadeIn(300);
	    obj.addClass("active");
	}

	$('.yo-tabs a').click(function(){
		$('.yo-tabs li').removeClass('current');
		$(this).parent().addClass('current');
		var clicked_group = $(this).attr('href');
		$.cookie('yo_current_opt', clicked_group, { expires: 7, path: '/' });
        switch_tabs($(this));
        return false;
    });
		
	// Display last viewed tab when loading the page
	if ($.cookie) {
		var last_viewed_tab = $.cookie("yo_current_opt");
		if (last_viewed_tab) {
			$('.yo-tabs a[href='+last_viewed_tab+']').trigger('click');
		} else {
			$('.yo-tabs li:first a').trigger('click');
		}
	}

	// Expand all the options together
	var flip = 0;				
	$('.yo-expand').click(function(){
		if(flip == 0){
			flip = 1;
			
			$('.yo-container .yo-nav').hide();
			$('.yo-container .yo-sections').css({width:'100%'});	
			$('.yo-container [id*="yo-option-"]').addClass('yo-expanded').show();			
			$(this).removeClass('expand');
			$(this).addClass('close');
			$(this).text('Close');	
		} else {
			flip = 0;
			
			$('.yo-container .yo-nav').show();
			$('.yo-container .yo-sections').css({width:''});
			$('.yo-container [id*="yo-option-"]').removeClass('yo-expanded');			
			$('.yo-tabs .current a').trigger('click');
			$(this).removeClass('close');
			$(this).addClass('expand');
			$(this).text('Expand');	
		}
	});



    /* Messages
	======================================================================== */

	// Fade out the save message in the Theme Options
	$('.fade').delay(2000).fadeOut(1000);



    /* Checkbox show/hide toggle
	======================================================================== */

	var first_run = true;
	$("input[data-toggle]").each(function() 
	{
		var targets = $(this).attr('data-toggle');
		// yo_show_targets($(this));

		// Checkbox
		if($(this).attr('type') == 'checkbox')
		{
			if ($(this).is(':checked')) {
				$(targets).fadeIn(400);
			} else {
				$(targets).hide();
			}

			$(this).click(function() {
				$($(this).attr('data-toggle')).fadeToggle(400);
	  		});
		}
		// Radio
		else
		{
			// yo_toggle_radio($(this));

			$(this).click(function() {
				var targets = $($(this).attr('data-toggle'));
				$('input[type="radio"]', $(this).parents('.yo-field')).each(function(){
					$($(this).attr('data-toggle')).hide();
				});

				if (first_run == true) {
					targets.show();
				} else {
					targets.fadeIn(400);
				}
	  		});	
		}
	});

	$('input[type="radio"]:checked').click();
	first_run = false;


	/* Enable 'Toggler' for cool toggles 
	======================================================================== */

	// if ($().toggler()) 
	// {
	// 	$('.yo-toggle input').toggler({checkboxCls:'slide-checkbox', checkedCls:'slide-checkbox-checked'});
	// }




	/* Sortable clones
	======================================================================== */

	if (jQuery().sortable) {
		$('.yo-sortable > .yo-control').sortable({
	        // containment: 'parent',
	        // connectWith: '#another-sortable-item', // Drag items across sortable groups
	        items: '.yo-clone',
	        // cursor: 'move',
	        // placeholder: 'yo-placeholder',
	        // forcePlaceholderSize: true,
	        // axis: 'y',
	        revert: true,
	        tolerance: 'pointer',
	        // helper: 'clone',
				// helper: function(e, ui) {
			//     console.log(ui);
			// },
	        // scroll:false,
	        // scrollSensitivity: 150,
	        // scrollSpeed: -20,
	        // delay: 2000,
	  //       create:function() {
	  //       	// Fix for the page "jump" on drag.
	  //           // var list=this;
	  //           // resize=function(){
	  //           //     jQuery(list).css("height","auto");
	  //           //     jQuery(list).height(jQuery(list).height());
	  //           // };
	  //           // jQuery(list).height(jQuery(list).height());
	  //           // jQuery(list).find('img').load(resize).error(resize);
	  //       },
	  //       start: function (e, ui) {        // new lines to
	  //       	// $(ui.item).css('margin-bottom', '50px');
		 //      // $(ui.placeholder).slideUp(200); // remove popping
		 //    },                             // effect on start
		 //    change: function (e, ui) {
		 //    	// $(ui.placeholder).slideUp(0).slideDown(200);
		 //    },
		 //    receive: function(e, ui) {
			// 	// $(ui.item).attr("id");
			// },
			// update: function(e, ui) {
			// 	// console.log('ok');
			// },
	        // handle: '.attachment-handle-icon',
	        // start: function(e, ui){
	        //     jQuery(e.target).hide(300);
	        // },
	        // change: function (e,ui){
	        //     jQuery(e.target).hide().show(300);
	        // },
	        // start: function (e,ui){        // new lines to
	        //   // jQuery(ui.placeholder).slideUp(); // remove popping
	        // },                             // effect on start
	        // change: function (e,ui){
	        //   jQuery(ui.placeholder).hide().slideDown();
	        // },
	        // create:function(){
	        //     jQuery(this).height(jQuery(this).height());
	        // },
	        stop: function (e, ui) {
	            // $('.yo-input > .yo-clone').each(function (i, id) {
	                // jQuery(this).find('input.attachment_order').val(i + 1);
	            // });
	        }
	    });


		// $('.yo-sortable > .yo-control').sortable();
		// $(".yo-sortable > .yo-control").disableSelection();
	}



	/* Update Message popup position
	======================================================================== */

	var $popup_save  = $('#yo-popup-save'),
		$popup_reset = $('#yo-popup-reset'),
		$popup_fail	 = $('#yo-popup-fail');

	$.fn.center = function () {
		this.animate({"top":( $(window).height() - this.height() - 200 ) / 2+$(window).scrollTop() + "px"},100);
		this.css("left", 250 );
		return this;
	}			
	$popup_save.center();
	$popup_reset.center();
	$popup_fail.center();
			
	$(window).scroll(function() {
		if ($popup_save.is(":visible") ||
			$popup_reset.is(":visible") ||
			$popup_fail.is(":visible"))
		{
			$popup_save.center();
			$popup_reset.center();
			$popup_fail.center();
		}
	});



	/* Masked Inputs
	======================================================================== */

	// Images as radio buttons
	$('.yo-radio-img').click(function(){
		$(this).parent().parent().find('.yo-radio-img').removeClass('yo-radio-img-selected');
		$(this).addClass('yo-radio-img-selected');
		$('input', $(this).parent()).attr('checked', true);
	});



	/* AJAX Upload
	======================================================================== */

	//delays until AjaxUpload is finished loading
	//fixes bug in Safari and Mac Chrome
	// if (typeof AjaxUpload != 'function') { 
	// 		return ++counter < 6 && window.setTimeout(init, counter * 500);
	// }

	// Upload
	function yo_image_upload() {
		$('.image_upload_button').each(function(){
				
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');	
					
			var nonce = $('#security').val();
					
			new AjaxUpload(clickedID, {
				action: ajaxurl,
				name: clickedID, // File upload name
				data: { // Additional data to send
					action: 'yo_ajax_post_action',
					type: 'upload',
					security: nonce,
					data: clickedID },
				autoSubmit: true, // Submit file after selection
				responseType: false,
				onChange: function(file, extension){},
				onSubmit: function(file, extension){
					clickedObject.text('Uploading'); // change button text, when user selects file	
					this.disable(); // If you want to allow uploading only 1 file at time, you can disable upload button
					interval = window.setInterval(function(){
						var text = clickedObject.text();
						if (text.length < 13){	clickedObject.text(text + '.'); }
						else { clickedObject.text('Uploading'); } 
						}, 200);
				},
				onComplete: function(file, response) {
					window.clearInterval(interval);
					clickedObject.text('Upload Image');	
					this.enable(); // enable upload button
						
			
					// If nonce fails
					if(response==-1){
						var fail_popup = $('#yo-popup-fail');
						fail_popup.fadeIn();
						window.setTimeout(function(){
						fail_popup.fadeOut();                        
						}, 2000);
					}				
							
					// If there was an error
					else if(response.search('Upload Error') > -1){
						var buildReturn = '<span class="upload-error">' + response + '</span>';
						$(".upload-error").remove();
						clickedObject.parent().after(buildReturn);
						
						}
					else{
						var buildReturn = '<img class="hide yo-image-thumb" id="image_'+clickedID+'" src="'+response+'" alt="" />';

						$(".upload-error").remove();
						$("#image_" + clickedID).remove();	
						clickedObject.parent().after(buildReturn);
						$('img#image_'+clickedID).fadeIn();
						clickedObject.next('span').fadeIn();
						clickedObject.parent().prev('input').val(response);
					}
				}
			});
		});
	}
	
	yo_image_upload();



	/* AJAX Remove Image (clear option value)
	======================================================================== */

	$('.image-reset-button').on('click', function(){
	
		var clickedObject = $(this);
		var clickedID = $(this).attr('id');
		var theID = $(this).attr('title');	
				
		var nonce = $('#security').val();
	
		var data = {
			action: 'yo_ajax_post_action',
			type: 'image_reset',
			security: nonce,
			data: theID
		};
					
		$.post(ajaxurl, data, function(response) {
						
			//check nonce
			if(response==-1){ //failed
							
				var fail_popup = $('#yo-popup-fail');
				fail_popup.fadeIn();
				window.setTimeout(function(){
					fail_popup.fadeOut();                        
				}, 2000);
			}
						
			else {
						
				var image_to_remove = $('#image_' + theID);
				var button_to_hide = $('#reset_' + theID);
				image_to_remove.fadeOut(500,function(){ $(this).remove(); });
				button_to_hide.fadeOut();
				clickedObject.parent().prev('input').val('');
			}
						
						
		});
					
	}); 

	// Style Select
	(function ($) {
	styleSelect = {
		init: function () {
		$('.select_wrapper').each(function () {
			$(this).prepend('<span>' + $(this).find('.select option:selected').text() + '</span>');
		});
		$('.select').live('change', function () {
			$(this).prev('span').replaceWith('<span>' + $(this).find('option:selected').text() + '</span>');
		});
		$('.select').bind($.browser.msie ? 'click' : 'change', function(event) {
			$(this).prev('span').replaceWith('<span>' + $(this).find('option:selected').text() + '</span>');
		}); 
		}
	};
	$(document).ready(function () {
		styleSelect.init()
	})
	})(jQuery);
	
	
	
	// Update slide title upon typing		
	// function update_slider_title(e) {
	// 	var element = e.target;
	// 	if ( this.timer ) {
	// 		clearTimeout( element.timer );
	// 	}
	// 	this.timer = setTimeout( function() {
	// 		$(element).parent().prev().find('strong').text( element.value );
	// 	}, 100);
	// 	return true;
	// }
	


	/* AJAX Backup & Restore
	======================================================================== */

	// Backup button
	$('#yo-backup-button').on('click', function(){
	
		var answer = confirm("Click OK to backup your current saved options.")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
		
			var data = {
				action: 'yo_ajax_post_action',
				type: 'backup_options',
				security: nonce
			};
						
			$.post(ajaxurl, data, function(response) {
							
				//check nonce
				if(response==-1){ //failed
								
					var fail_popup = $('#yo-popup-fail');
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}
							
				else {
							
					var success_popup = $('#yo-popup-save');
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}
			});
		}
		
		return false;				
	}); 
	
	// Restore button
	$('#yo-restore-button').on('click', function(){
	
		var answer = confirm("'Warning: All of your current options will be replaced with the data from your last backup! Proceed?")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
		
			var data = {
				action: 'yo_ajax_post_action',
				type: 'restore_options',
				security: nonce
			};
						
			$.post(ajaxurl, data, function(response) {
			
				//check nonce
				if(response==-1){ //failed
								
					var fail_popup = $('#of-popup-fail');
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}
							
				else {
							
					var success_popup = $('#of-popup-save');
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}	
						
			});
	
		}
	
	return false;
					
	});
	


	/* AJAX Transfer Options (Import/Export)
	======================================================================== */

	// Import button
	$('#yo-import-button').on('click', function(){
	
		var answer = confirm("Click OK to import options.")
		
		if (answer){
	
			var clickedObject = $(this);
			var clickedID = $(this).attr('id');
					
			var nonce = $('#security').val();
			
			var import_data = $('#export_data').val();
		
			var data = {
				action: 'yo_ajax_post_action',
				type: 'import_options',
				security: nonce,
				data: import_data
			};
						
			$.post(ajaxurl, data, function(response) {
				var fail_popup = $('#yo-popup-fail');
				var success_popup = $('#yo-popup-save');
				
				//check nonce
				if(response==-1){ //failed
					fail_popup.fadeIn();
					window.setTimeout(function(){
						fail_popup.fadeOut();                        
					}, 2000);
				}		
				else 
				{
					success_popup.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				}
			});
		}

		return false;
	});
	


	/* AJAX Save Options
	======================================================================== */

	$('.yo-save').on('click',function() {

		var nonce = $('#security').val();
					
		$('.ajax-loading-img').fadeIn();
		
		//get serialized data from all our option fields			
		var serializedReturn = $('#yo-form :input[name][name!="security"][name!="yo-reset"]').serialize();

		var data = {
			type: 'save',
			action: 'yo_ajax_post_action',
			security: nonce,
			data: serializedReturn
		};
					
		$.post(ajaxurl, data, function(response) {
			var success = $('#yo-popup-save');
			var fail = $('#yo-popup-fail');
			var loading = $('.ajax-loading-img');
			loading.fadeOut();  
			
			// $(success).children('div').html(response);
			// success.fadeIn();

			if (response == 1) {
				success.fadeIn();
			} else {
				fail.fadeIn();
			}
						
			window.setTimeout(function(){
				success.fadeOut(); 
				fail.fadeOut();				
			}, 2000);
		});
		
		$popup_save.center();
		$popup_reset.center();
		$popup_fail.center();	
		return false; 
	});   
	

	
	/* AJAX Reset Options
	======================================================================== */
	
	$('#yo-reset').click(function() {
		
		//confirm reset
		var answer = confirm("Click OK to reset. All settings will be lost and replaced with default settings!");
		
		//ajax reset
		if (answer) {
			
			var nonce = $('#security').val();
						
			$('.ajax-reset-loading-img').fadeIn();
							
			var data = {
				type: 'reset',
				action: 'yo_ajax_post_action',
				security: nonce,
			};
						
			$.post(ajaxurl, data, function(response) {
				var success = $('#yo-popup-reset');
				var fail = $('#yo-popup-fail');
				var loading = $('.ajax-reset-loading-img');
				loading.fadeOut();  
							
				if (response==1)
				{
					success.fadeIn();
					window.setTimeout(function(){
						location.reload();                        
					}, 1000);
				} 
				else 
				{ 
					fail.fadeIn();
					window.setTimeout(function(){
						fail.fadeOut();				
					}, 2000);
				}
			});
		}

		return false;
	});


	// Sortable Items - Portfolio Custom Type
	if (jQuery().sortable) {
		// Sortable Items - Portfolio Custom Type
		var portfolio_list = $('#portfolio-list');
	 
		portfolio_list.sortable({
			update: function(event, ui) {
				$('#loading-animation').show(); // Show the animate loading gif while waiting
	 
				opts = {
					url: ajaxurl, // ajaxurl is defined by WordPress and points to /wp-admin/admin-ajax.php
					type: 'POST',
					async: true,
					cache: false,
					dataType: 'json',
					data:{
						action: 'sort_portfolio', // Tell WordPress how to handle this ajax request
						order: portfolio_list.sortable('toArray').toString() // Passes ID's of list items in	1,3,2 format
					},
					success: function(response) {
						$('#loading-animation').hide(); // Hide the loading animation
						return; 
					},
					error: function(xhr, textStatus, e) {  // This can be expanded to provide more information
						alert('There was an error saving the updates');
						$('#loading-animation').hide(); // Hide the loading animation
						return; 
					}
				};
				$.ajax(opts);
			}
		});	
	}


	/* Move the excerpt box below the content editor
	======================================================================== */

	// $($("#postexcerpt").parent()).prepend($("#postexcerpt")); // After the editor
	// $('div[id^="yo-post-format-"]').each(function(){
	// 	$(this).insertBefore('#wp-content-wrap');
	// 	// $(this).parent()).before($(this));
	// });

	// $("#postcustom").before($("#postexcerpt")); // Before the custom fields



	/* Post Formats
	======================================================================== */

	function yo_post_formats_metaboxes(id) {
		$('div[id^="yo-post-format-"]').hide();	
		if (typeof id === 'undefined' || id === 0) {
			id = $('input[name=post_format]:checked').val();
		}
		$('#yo-post-format-'+id).show();
	}
	 
	$('.post-format').change(function (e) {
		yo_post_formats_metaboxes(e.target.value);
	});
	 
	yo_post_formats_metaboxes();
	

	/* Template meta boxes
	======================================================================== */
	
	var yo_template_metaboxes_first_run = true;

	$('#page_template').change(function (e) {
		if (e.target.value == 'template-blog.php') {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-blog-template').show();
			} else {
				$('#yo-blog-template').fadeIn();
			}
		} else {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-blog-template').hide();
			} else {
				$('#yo-blog-template').fadeOut();
			}
		}

		if (e.target.value == 'template-custom.php') {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-custom-template').show();
			} else {
				$('#yo-custom-template').fadeIn();
			}
		} else {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-custom-template').hide();
			} else {
				$('#yo-custom-template').fadeOut();
			}
		}

		if (e.target.value == 'default') {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-post-layout').show();
			} else {
				$('#yo-post-layout').fadeIn();
			}
		} else {
			if (yo_template_metaboxes_first_run == true) {
				yo_template_metaboxes_first_run = false;
				$('#yo-post-layout').hide();
			} else {
				$('#yo-post-layout').fadeOut();
			}
		}
	});

	$('#page_template').trigger('change');


}); //end doc ready