jQuery(document).ready(function($)
{
	//
	// Recursively find an available ID
	//
	// TODO: Also substitute IDs for input, label for, etc...
	function set_new_id(id, clone)
	{
		if (id == null) id = 0;

		$('input[name], select[name]', clone).each(function() {
			this.name = this.name.replace(/\[(\d+)\]/, function(str, p1){
                return '[' + id + ']';
            });
		});

		$('input[name], select[name]', clone.siblings('.yo-clone')).each(function()
		{
			if ($('input[name="'+this.name+'"], select[name="'+this.name+'"]', clone).length > 0)
			{
				id++;
				set_new_id(id, clone);
				return false;
			}
		});
	}

	function get_field_template(id)
	{
		var template = window[id];
		return (template) ? template : false;
	}

	//
	// Add a clone
	//
	$(".yo-container").delegate("[data-clone]", "click", function()
	{
		// Set the new ID
		var id = $(this).siblings('.yo-clone').length;

		// Stop everything if we reached the maximum allowed items
		if ($(this).attr('data-max-clones') != null && id >= $(this).attr('data-max-clones'))
			return false;

		// Get the template variable
		// var template = window[$(this).attr('data-clone')];
		var template = get_field_template($(this).attr('data-clone'));
		$(this).before(template);

		// Set the cloned object
		var $clone = $(this).prev();
		$clone.hide().slideToggle(300);

		set_new_id(id, $clone);

		// Clean the values
		$clone.find(':input').val('');
		$('.yo-uploaded-image', $clone).remove();
		$('.yo-remove-media', $clone).hide();
		
		// Fix color picker
		if ( 'function' === typeof yo_update_color_picker )
			yo_update_color_picker();
		
		// Fix date picker
		if ( 'function' === typeof yo_update_date_picker )
			yo_update_date_picker();
			
		// Fix time picker
		if ( 'function' === typeof yo_update_time_picker )
			yo_update_time_picker();
		
		// Fix datetime picker
		if ( 'function' === typeof yo_update_datetime_picker )
			yo_update_datetime_picker();

		setTimeout(function () {
			$('.yo-clone', '.yo-container').trigger('mouseout');
		}, 500);

		return false;
	});

	//
	// Remove a clone
	//
	$(".yo-container").delegate("[data-unclone]", "click", function()
	{
		var $parent = $(this).parent();
		$parent.slideToggle(300, function(){ $(this).remove(); });
		return false;
	});

	//
	// Remove clone button fade in/out
	//
	$(".yo-container").delegate(".yo-clone", "mouseover", function(){
      $('[data-unclone], [data-collapse]', $(this)).stop().fadeTo(300, 1);
    });

	$(".yo-container").delegate(".yo-clone", "mouseout", function(){
      $('[data-unclone], [data-collapse]', $(this)).stop().fadeTo(300, 0.2);
    });

	$('.yo-clone', $(".yo-container")).trigger('mouseout');


	//
	// Collapse/Expand group
	//
	$(".yo-container").delegate("[data-collapse]", "click", function()
	{
		var $parent = $(this).parent();

		var toggled = $(this).data('toggled');
	    $(this).data('toggled', !toggled);

	    if (!toggled) {
			$parent.addClass('group-collapsed');
	    }
	    else {
			$parent.removeClass('group-collapsed');
	    }

		return false;
	});


});