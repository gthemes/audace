/**
 * Update color picker element
 * Used for static & dynamic added elements (when clone)
 */
function yo_update_color_picker()
{
	var $ = jQuery;
	$('.yo-color-picker').each(function()
	{
		var $this = $(this),
			$input = $this.siblings('input.yo-color');

		// Make sure the value is displayed
		// if ( ! $input.val())
		// 	$input.val('#');

		$this.farbtastic($input);
	});
}

jQuery(document).ready(function($)
{
	$('.yo-color').focus(function()
	{
		$(this).siblings('.yo-color-picker').show();
		return false;
	}).blur(function() {
		$(this).siblings('.yo-color-picker').hide();
		return false;
	});

	yo_update_color_picker();
});