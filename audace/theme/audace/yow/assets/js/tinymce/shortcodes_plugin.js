/*
 * Shortcode Editor
 * v1.0
*/
(function() {

	/**
	 * Get String between a Prefix String and a Suffix String
	 * Usage  var you = 'hello you guys'.between('hello ',' guys');
	 * you = 'you';
	 */
	String.prototype.between = function(prefix, suffix) {
	  s = this;
	  var i = s.indexOf(prefix);
	  if (i >= 0) {
	    s = s.substring(i + prefix.length);
	  }
	  else {
	    return '';
	  }
	  if (suffix) {
	    i = s.indexOf(suffix);
	    if (i >= 0) {
	      s = s.substring(0, i);
	    }
	    else {
	      return '';
	    }
	  }
	  return s;
	}

	var yo_registered_shortcodes = [];

    tinymce.create('tinymce.plugins.yoShortcodes', {
        init: function (ed, url)
		{
			var yo = this;
			yo.url = url;

			//replace shortcode before editor content set
			// ed.onBeforeSetContent.add(function(ed, o) {
				// o.content = yo.visualShortcodes(o.content);
			// });
			
			//replace shortcode as its inserted into editor (which uses the exec command)
			// ed.onExecCommand.add(function(ed, cmd) {
			    // if (cmd ==='mceInsertContent'){
					// tinyMCE.activeEditor.setContent( yo.visualShortcodes(tinyMCE.activeEditor.getContent()) );
				// }
			// });

			//replace the image back to shortcode on save
			// ed.onPostProcess.add(function(ed, o) {
				// console.log('converting to standardShortcodes');
				// if (o.get)
					// o.content = yo.standardShortcodes(o.content);
			// });

			ed.addCommand("yoEditor", function (a, params)
			{
				var shortcode = params.identifier;
					// active = params.active;

				// Load thickbox
				tb_show("Insert Shortcode", yo_shortcodes.editor_page + "&post_id=&shortcode=" + shortcode + /*"&width=" + 650+ "&height=" + 550 +*/ "&TB_iframe=1");

				// Wait for the iframe to load
				jQuery('#TB_iframeContent').load(function()
				{
					// Resize the popup
					yo.tb_resize();
					jQuery(window).resize(function() { yo.tb_resize(); });

					jQuery("#TB_iframeContent").contents().find('#yo-insert-shortcode').click(function()
					{
						var shortcode_template = jQuery("#TB_iframeContent").contents().find('#yo-shortcode-template').text();
			    		
			    		// Clone groups
			    		// Fill in the gaps eg {{param}}
			    		// TODO: Change 'input, textarea...' with the serialized form data
			    		jQuery("#TB_iframeContent").contents().find('[data-clone]').each(function()
			    		{
			    			// console.log('----------------');

			    			// Get the clone group id and the template to use for the clones
			    			var clone_group = jQuery(this).attr('data-clone'),
			    				clone_template = shortcode_template.between('{{clone:'+clone_group+'}}','{{/clone}}'),
			    				form_fields = 'input, textarea, select, radio, checkbox',
			    				output = '';

			    			if (clone_template)
			    			{
			    				// Get the clones
			    				jQuery("#TB_iframeContent").contents().find('#yo-section-'+clone_group+' .yo-clone').each(function()
			    				{
			    					var clone = clone_template;

				    				// Get the fields of each clone
				    				jQuery(form_fields, jQuery(this)).each(function(){
				    					var input = jQuery(this),
				    						id = input.attr('id').replace(clone_group+'_', ''),
						    				re = new RegExp("{{"+id+"}}","g");
						    			clone = clone.replace(re, input.val());	
						    			// console.log('----');
						    			// console.log('ID: ' + id);
						    			// console.log('Clone: ' + clone);
						    			// console.log('Val: ' + input.val());
				    				});

				    				output += clone;
			    				});
			    			}

			    			// console.log('Output');
			    			// console.log(output);
			    			// console.log('Clone template');
			    			// console.log(clone_template);
			    			// console.log('Shortcode template');
			    			// console.log(shortcode_template);
			    			// Remove the loop tags from the shortcode
			    			shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);
			    			// console.log('-');
			    			// console.log(shortcode_template);

			    		});// clone group
						
			    		// Single fields
			    		// Fill in the gaps eg {{param}}
			    		// TODO: Change 'input, textarea...' with the serialized form data
						jQuery("#TB_iframeContent").contents().find('.yo-control input, .yo-control textarea, .yo-control select').each(function() {
			    			var input = jQuery(this),
			    				id = input.attr('id'),
			    				re = new RegExp("{{"+id+"}}","g");
			    			shortcode_template = shortcode_template.replace(re, input.val());
			    		});

						// Remove any {{/clone}}
			    		// shortcode_template = shortcode_template.replace('{{clone:'+clone_group+'}}'+clone_template+'{{/clone}}', output);

						if(window.tinyMCE)
						{
							window.tinyMCE.activeEditor.execCommand("mceInsertContent", false, shortcode_template);
	                		tb_remove();
	                	}
					});
				});
			});
		},

        createControl: function (btn, e)
		{
			if (btn == "yo_sc_button")
			{
				var a = this;
				
				var btn = e.createSplitButton('yo_sc_button', {
                    title: "Insert Shortcode",
					image: yo_shortcodes.assets +"img/shortcodes.png",
					icons: false,
					onclick: function() { }
                });

                btn.onRenderMenu.add(function (c, b)
				{
					a.addWithPopup(b, "Accordion", "yo_accordion");
					a.addWithPopup(b, "Button", "yo_button");
					a.addWithPopup(b, "Columns", "yo_columns");
					a.addImmediate(b, "Contact Form", "yo_contact");
					a.addWithPopup(b, "Hero Text", "yo_hero");
					a.addImmediate(b, "Horizontal Rule", "yo_hr");
					a.addWithPopup(b, "Lists", "yo_list");
					a.addWithPopup(b, "Maps", "yo_maps");
					a.addWithPopup(b, "Messages", "yo_message");
					a.addWithPopup(b, "Quote", "yo_quote");
					a.addWithPopup(b, "Slider", "yo_slider");
					a.addWithPopup(b, "Tabs", "yo_tabs");
					a.addWithPopup(b, "Video", "yo_embed");		

					// a.addImmediate(b, "Test", "icitspot");
					// a.addImmediate(b, "Test2", 'icitspots id="test"');			
				});
                
                return btn;
			}
			
			return null;
		},

		addWithPopup: function (ed, title, shortcode) {
			// yo_registered_shortcodes.push(shortcode);

			ed.add({
				title: title,
				onclick: function() {
					window.tinyMCE.activeEditor.execCommand("yoEditor", false, {
						title: title,
						identifier: shortcode
					});
				}
			});
		},

		addImmediate: function(ed, title, shortcode){
			// yo_registered_shortcodes.push(shortcode);
			ed.add({
				title: title,
				onclick: function() {
					window.tinyMCE.activeEditor.execCommand("mceInsertContent", false, "["+shortcode+"]");
				}
			});
		},

		visualShortcodes: function(co) {
			// var shortcodes_count = yo_registered_shortcodes.length;
			for (var i = 0; i < shortcodes_count; i++)
			{
				var re = new RegExp('\\['+yo_registered_shortcodes[i]+'([^\\]]*)\\]', 'g');

			 	co = co.replace(re, function(a,b){
					return '<img src="/wordpress3/wp-content/themes/audace/yow/assets/img/t.gif" class="wpSpot mceItem '+yo_registered_shortcodes[i]+'" title="'+yo_registered_shortcodes[i]+tinymce.DOM.encode(b)+'" />';
				});	   
			}

			return co;
		},

		standardShortcodes: function(co) {
			function getAttr(s, n) {
				n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
				return n ? tinymce.DOM.decode(n[1]) : '';
			};

			return co.replace(/(?:<p[^>]*>)*(<img[^>]+>)(?:<\/p>)*/g, function(a,im) {
				var cls = getAttr(im, 'class');

				if ( cls.indexOf('wpSpot') != -1 )
					return '<p>['+tinymce.trim(getAttr(im, 'title'))+']</p>';

				return a;
			});
		},

		tb_resize: function()
    	{
 			var	iframeCont = jQuery('#TB_iframeContent'),
				tbWindow = jQuery('#TB_window'),
				popup = jQuery('#TB_iframeContent').contents().find('#yo-shortcodes');

			tbWindow.css({
                height: popup.outerHeight(),
                width: popup.outerWidth(),
                marginLeft: -(popup.outerWidth()/2),
            });

			iframeCont.css({
				paddingTop: 0,
				paddingLeft: 0,
				paddingRight: 0,
				height: (tbWindow.outerHeight()),
				overflow: 'auto', // !!!
				width: popup.outerWidth()
			});
    	},

        getInfo: function() {
            return {
                longname: "Yo Shortcodes",
                author: 'Giordano Piazza',
                authorurl: 'http://giordanopiazza.com',
                version: "1.0"
            };
        }
    });
    tinymce.PluginManager.add('yo_sc_button', tinymce.plugins.yoShortcodes);
})();


