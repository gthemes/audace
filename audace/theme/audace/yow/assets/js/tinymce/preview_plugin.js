
(function() {
	tinymce.create('tinymce.plugins.yoPreview', {

		init : function(ed, url) {
			var t = this;

			t.url = url;
			t._createButtons();

			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('...');
			ed.addCommand('yoPreview', function() {
				var el = ed.selection.getNode(), post_id, vp = tinymce.DOM.getViewPort(),
					H = vp.h - 80, W = ( 640 < vp.w ) ? 640 : vp.w;

				if ( el.nodeName != 'IMG' ) return;
				if ( ed.dom.getAttrib(el, 'class').indexOf('yo-preview') == -1 )	return;

				post_id = tinymce.DOM.get('post_ID').value;
				tb_show('', tinymce.documentBaseURL + 'media-upload.php?post_id='+post_id+'&tab=gallery&TB_iframe=true&width='+W+'&height='+H);

				tinymce.DOM.setStyle( ['TB_overlay','TB_window','TB_load'], 'z-index', '999999' );
			});

			ed.onMouseDown.add(function(ed, e) {
				if ( e.target.nodeName == 'IMG' && ed.dom.hasClass(e.target, 'yo-preview') )
					ed.plugins.wordpress._showButtons(e.target, 'yo_preview_btns');
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = t._do_gallery(o.content);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = t._get_gallery(o.content);
			});

			// ed.addButton('preview', {title : 'Preview', cmd : 'yoPreview' });
		},

		_do_gallery : function(co) {
			return co.replace(/\[minkia([^\]]*)\]/g, function(a,b){
				// console.log('tinymce.baseURL: '+tinymce.baseURL);
				console.log('t.url: '+co);

				return '<img src="'+tinymce.baseURL+'/plugins/wpgallery/img/t.gif" class="yo-preview mceItem wpGallery" title="shortcode'+tinymce.DOM.encode(b)+'" />';
			});
		},

		_get_gallery : function(co) {

			function getAttr(s, n) {
				n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
				return n ? tinymce.DOM.decode(n[1]) : '';
			};

			return co.replace(/(?:<p[^>]*>)*(<img[^>]+>)(?:<\/p>)*/g, function(a,im) {
				var cls = getAttr(im, 'class');

				if ( cls.indexOf('yo-preview') != -1 )
					return '<p>['+tinymce.trim(getAttr(im, 'title'))+']</p>';

				return a;
			});
		},

		_createButtons : function() {
			var t = this, ed = tinyMCE.activeEditor, DOM = tinymce.DOM, editButton, dellButton;

			// DOM.remove('yo_preview_btns');
			DOM.remove('yo_preview_btns');

			DOM.add(document.body, 'div', {
				id : 'yo_preview_btns',
				style : 'display:none;'
			});

			editButton = DOM.add('yo_preview_btns', 'img', {
				src : t.url+'/img/edit.png',
				id : 'yo_preview_edit',
				width : '24',
				height : '24',
				title : ed.getLang('wordpress.editgallery')
			});

			tinymce.dom.Event.add(editButton, 'mousedown', function(e) {
				var ed = tinyMCE.activeEditor;
				ed.windowManager.bookmark = ed.selection.getBookmark('simple');
				ed.execCommand("yoPreview");
			});

			dellButton = DOM.add('yo_preview_btns', 'img', {
				src : t.url+'/img/delete.png',
				id : 'yo_preview_delete',
				width : '24',
				height : '24',
				title : ed.getLang('wordpress.delgallery')
			});

			tinymce.dom.Event.add(dellButton, 'mousedown', function(e) {
				var ed = tinyMCE.activeEditor, el = ed.selection.getNode();

				if ( el.nodeName == 'IMG' && ed.dom.hasClass(el, 'yo-preview') ) {
					ed.dom.remove(el);

					ed.execCommand('mceRepaint');
					return false;
				}
			});
		},

		getInfo : function() {
			return {
				longname : 'Preview Settings',
				author : 'Giordano Piazza',
				authorurl : 'http://giordanopiazza.com',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('yo_preview_button', tinymce.plugins.yoPreview);
})();
