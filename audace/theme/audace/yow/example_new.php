<?php

/*
 *------------------------------------------------------------------------------------------------
 * Meta Box Definitions
 *------------------------------------------------------------------------------------------------
 *
 * All the meta boxes are defined here.
 * 
 */

// global $ustom_metabox_fields;

$my_fields = array();

$my_fields[] = array(
    array(
        'name'   => 'Field',
        'id'     => 'standard_field',
        'type'   => 'text',
    ),
    array(
    'name'      => 'Standard Group',
    'id'        => 'standard_group',
    'type'      => 'group',
    // 'clone'     => true,
    'fields'    => array(
            array(
                'name'   => 'Title',
                'id'     => 'title',
                'type'   => 'text',
            ),
        )
    ),
    array(
        'name'      => 'Group',
        'id'        => 'grouped_fields',
        'type'      => 'group',
        'fields'    => array(
            array(
                'name'   => 'Simple Field',
                'id'     => 'simple_field',
                'type'   => 'text',
            ),
            array(
                'name'   => 'Nested Group',
                'id'     => 'nested_group',
                'type'   => 'group',
                'clone' => true,
                'fields' => array(
                    array(
                        'name'   => 'Nested Group Field 1',
                        'id'     => 'nested1',
                        'type'   => 'text',
                    ),
                    array(
                        'name'   => 'Nested Group Field 2',
                        'id'     => 'nested2',
                        'type'   => 'select',
                        'options' => array(
                                'one' => 'One',
                                'two' => 'Two',
                            )
                    ),
                    // array(
                    //     'name'   => 'Nested Group 2',
                    //     'id'     => 'nested_group2',
                    //     'type'   => 'group',
                    //     'fields' => array(
                    //         array(
                    //             'name'   => 'N1',
                    //             'id'     => 'n1',
                    //             'type'   => 'text',
                    //         ),
                    //         array(
                    //             'name'   => 'N2',
                    //             'id'     => 'n2',
                    //             'type'   => 'text',
                    //         ),
                    //     )
                    // ),
                )
            ),
        )
    ),
    array(
        'name'      => 'Cloneable Group',
        'id'        => 'cloneable_group',
        'type'      => 'group',
        'clone'     => true,
        'fields'    => array(
                array(
                    'name'   => 'Cloneable title',
                    'id'     => 'title',
                    'type'   => 'text',
                ),
                // array(
                //     'name'   => 'Cloneable description',
                //     'id'     => 'description',
                //     'type'   => 'text',
                // ),
            )
    ),
    array(
        'name'   => 'Single Cloneable Field',
        'id'     => 'cloneable_field',
        'type'   => 'text',
        'clone'  => true
    ),
);




// global $metaboxes;

// 
$metaboxes = array();

// Project details 1
$metaboxes[] = array(
    'id' => 'project-details1',
    'title' => 'Project Details 1',
    'pages' => array('post'), // 'post', 'page', 'link', or 'custom_post_type'
    'context' => 'normal', // 'normal', 'advanced', or 'side'
    'priority' => 'high', // 'high', 'core', 'default' or 'low'
    'fields' => $my_fields[0]
);

yo_register_metabox($metaboxes);






//
$theme_options = array();

// Theme Options
$theme_options[] = array(
    'name'   => 'General Settings',
    'id'     => 'generalsettings',
    'icon'   => 'none',
    'fields' => $my_fields[0]
);

// TODO: Add config array with menu label, position etc...

yo_register_options($theme_options);


