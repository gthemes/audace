	<?php if (post_password_required()) : ?>
	<div class="comments caption">
		<p class="nopassword"><?php _e('This post is password protected. Enter the password to view any comments.', 'theme_admin'); ?></p>
	</div><!-- #comments -->
	<?php return; endif; ?>

	<?php if (have_comments()) : ?>
	<div class="caption comments-container clearfix top">
		<h2 id="comments-title">
			<?php
				printf(_n('One comment on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', get_comments_number(), 'theme_admin'),
					number_format_i18n(get_comments_number()), '<span>' . get_the_title() . '</span>');
			?>
		</h2>

		<?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through ?>
		<nav id="comment-nav-above">
			<h1 class="assistive-text"><?php _e('Comment navigation', 'theme_admin'); ?></h1>
			<div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'theme_admin')); ?></div>
			<div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'theme_admin')); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<ul class="comments">
			<?php wp_list_comments(array('callback' => 'yo_comment', 'type' => 'comment')); ?>
		</ol>

		<?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through ?>
		<nav id="comment-nav-below">
			<h1 class="assistive-text"><?php _e('Comment navigation', 'theme_admin'); ?></h1>
			<div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'theme_admin')); ?></div>
			<div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'theme_admin')); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>
	</div><!-- #comments -->
	<?php elseif ( ! comments_open() && ! is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
	<?php endif; ?>

	<?php if (comments_open()) : ?>
	<div class="caption comments-container"><?php comment_form(array('comment_notes_after' => ' ')); ?></div>
	<?php endif; ?>