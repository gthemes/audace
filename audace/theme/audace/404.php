<?php get_header(); ?>

<section id="content" role="main" class="clearfix row top">

    <header class="grid_12 base">
        <h1 class="page-title"><?php _e("Oooops!", 'theme_admin'); ?></h1>
    </header>

	<div class="hentry grid_12">
		<div class="caption base"><p><?php _e("We can't find the page you're looking for, please try the options below.", 'theme_admin'); ?></p></div>
		<div class="caption">
			<div class="row-fluid">
				<div class="span4">
					<?php get_search_form();  ?>
				</div>
				<div class="span4">
					<?php the_widget('YO_Better_Recent_Posts', array('title' => __('Recent posts', 'theme_admin'), 'cat' => '', 'number' => '5', 'offset' => '', 'thumbnail_size' => 80, 'thumbnail_width' => 80, 'thumbnail_height' => 80, 'thumbnail' => true, 'posttype' => 'post'), array('widget_id' => '404')); ?>
				</div>

				<div class="span4">

					
					<?php the_widget('WP_Widget_Categories', array('title' => __('Categories', 'theme_admin'))); ?>

					<!-- <div class="widget">
						<h2 class="widgettitle"><?php _e('Categories', 'theme_admin'); ?></h2>
						<?php
						$categories = get_categories(array('hide_empty' => false));
						foreach ($categories as $category)
							$options[$category->cat_ID] = $category->name . ' (' .$category->category_count.')';
						?>
					</div> -->
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_footer(); ?>