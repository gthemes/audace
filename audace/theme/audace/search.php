<?php get_header();

$page_layout = get_field('search_layout', 'option');
if (!$page_layout) $page_layout = '1sidebar';

$page_title = __('Search results for: ', 'theme_admin') . get_search_query();

include(locate_template('loop.php'));

get_footer();

?>