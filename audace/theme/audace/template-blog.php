<?php
/*
Template Name: Blog
*/

get_header();

global $wp_query;
$current_page = $wp_query->get_queried_object();

if (isset($current_page->post_content) && !empty($current_page->post_content))
{
	$page_content = $current_page->post_content;
	$page_content = do_shortcode($page_content);
}

// echo '<pre>';
// print_r($current_page);
// echo '</pre>';

global $query_string, $paged;

if (get_query_var('paged'))
{
	$paged = get_query_var('paged');
}
elseif (get_query_var('page'))
{
	$paged = get_query_var('page');
}
else
{
	$paged = 1;
}

$args = array(
	'post_type' => 'post',
	// 'paged' => get_query_var('paged'),
	'paged' => $paged,
	'orderby' => 'date',
	'order' => 'DESC',
	'cat' => ''
);

$page_title = get_the_title();
$page_layout = get_field('yo_blog_layout');
$categories = get_field('yo_blog_categories');
$excluded_categories = get_field('yo_blog_excluded_categories');
$posts_per_page = get_field('yo_blog_posts_per_page');
$infinite_scroll = get_field('yo_infinite_scroll');


if (is_front_page())
{
	// $args['offset'] = (get_query_var('page')) ? (get_query_var('page')-1)*$posts_per_page : 0;
	$args['offset'] = (get_query_var('paged')) ? (get_query_var('paged'))*$posts_per_page : 0;

	// echo 'page: '.get_query_var('page').'<br>';
	// echo 'posts_per_page: '.$posts_per_page.'<br>';
	// echo 'offset: '.$args['offset'].'<br>';
}

// echo 'page: '.get_query_var('page').'<br>';
// echo 'paged: '.get_query_var('paged').'<br>';
// echo 'paged: '.$paged.'<br>';


if (!empty($categories)) $args['cat'] .= implode(',', $categories);
if (!empty($excluded_categories)) 
	foreach ($excluded_categories as &$excluded)
		$args['cat'] .= ',-'.$excluded;
if (!empty($posts_per_page)) $args['posts_per_page'] = $posts_per_page;

query_posts($args);

include(locate_template('loop.php'));

wp_reset_query();

get_footer();

?>