<?php 

if (is_category() && !is_singular() && !isset($page_title))
    $page_title = get_the_category_by_ID(get_query_var('cat'));

if (is_singular())
{
    $page_layout = get_field('yo_post_layout');
    $page_layout = (!empty($page_layout) && $page_layout != NULL) ? $page_layout : '1sidebar';
    $blog_container = '';
    if (!isset($page_title)) $page_title = get_the_title();
}
else
{
    $page_layout = (isset($page_layout) && !empty($page_layout) && $page_layout != NULL) ? $page_layout : '3col';
    $blog_container = 'blog-grid';
    if (!isset($page_title)) $page_title = __('Blog', 'theme_admin');
}

switch ($page_layout)
{
    case 'slides':
        // $content_class = '';
        // $content_class = 'grid_12';
        $entry_class = 'box fullwidth-slide';
        // $entry_class = 'box grid_3';
        $image_size = 'full-hd';
        // $blog_container = 'blog-fluid-grid fluid';
        break;

    case 'fullwidth':
        $content_class = 'grid_12';
        $entry_class = 'box grid_12';
        $image_size = 'full';
        $blog_container = '';
        break;

    case '2col':
        $content_class = 'grid_12';
        $entry_class = 'box grid_6';
        // $image_size = '2-columns';
        $image_size = 'standard';
        break;

    default:
    case '3col':
        $content_class = 'grid_12';
        $entry_class = 'box grid_4';
        // $image_size = '3-columns';
        $image_size = 'standard';
        break;

    case '4col':
        $content_class = 'grid_12';
        $entry_class = 'box grid_3';
        // $image_size = '4-columns';
        $image_size = 'standard';
        break;

    case '2col-1sidebar':
        $content_class = 'grid_8';
        $entry_class = 'box grid_4';
        $sidebar_class = 'sidebar grid_4';
        // $image_size = '2-columns';
        $image_size = 'standard';
        break;

    case '2col-2sidebars':
        $content_class = 'grid_6';
        $entry_class = 'box grid_3';
        $sidebar_class = 'sidebar grid_3';
        // $image_size = '2-columns';
        $image_size = 'standard';
        break;

    case '3col-1sidebar':
        $content_class = 'grid_9';
        $entry_class = 'box grid_3';
        $sidebar_class = 'sidebar grid_3';
        // $image_size = '2-columns';
        $image_size = 'standard';
        break;

    case '4col-1sidebar':
        $content_class = 'grid_8';
        $entry_class = 'box grid_2';
        $sidebar_class = 'sidebar grid_4';
        // $image_size = '2-columns';
        $image_size = 'standard';
        break;

    case '1sidebar':
        $content_class = 'grid_8';
        $entry_class = 'box grid_8';
        $sidebar_class = 'sidebar grid_4';
        $image_size = 'standard';
        $blog_container = '';
        break;

    case '2sidebars':
        $content_class = 'grid_6';
        $entry_class = 'box grid_6';
        $sidebar_class = 'sidebar grid_3';
        $image_size = 'standard';
        $blog_container = '';
        break;
}

// Widget areas
$before_loop = get_field('yo_before_loop');
$after_loop = get_field('yo_after_loop');

?>


<?php if (isset($yo_half_page_background) && $yo_half_page_background == true): ?>
    <div class="half-slider"></div>
<?php endif; ?>


<?php if (isset($page_title) && !empty($page_title) && $page_layout != 'slides'): ?>
<?php $show_title = get_field('yo_show_title_above'); if ($show_title == true OR $show_title == NULL): ?>
<header class="row top">
    <div class="grid_12">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
</header>
<?php endif; ?>
<?php endif; ?>

<?php if ($before_loop == NULL || $before_loop == true) : ?>
<?php if (!is_singular() && function_exists('dynamic_sidebar') && is_active_sidebar('before-loop') && !is_page_template('template-clean.php') && !is_page_template('template-custom.php')) : ?>
<div id="before-loop" class="row top clearfix">
    <div class="grid_12">
        <div class="row horizontal-widgets">
            <?php dynamic_sidebar('before-loop'); ?>
        </div>
    </div>
</div>
<?php endif; endif; ?>


<?php 
//
// Slides layout
//
if ($page_layout == 'slides'): 

    include(locate_template('loop-slides.php'));
//
// Other layouts
//
else : ?>

    <!-- Posts -->
    <section id="content" role="main" class="clearfix <?php echo $blog_container; ?> row top">

        <?php if (isset($page_content)) : ?>
            <div id="page-content" class="grid_12">
                <?php echo $page_content; ?>
            </div>
        <?php endif; ?>

        <!-- Content -->
        <div class="<?php echo $content_class; ?>">
            <div class="content-wrapper">
                <div class="row">
                    <?php include(locate_template('loop-items.php')); ?>
                </div>
            </div>
        </div>

        <?php if ($page_layout == '1sidebar' || $page_layout == '2sidebars' || $page_layout == '2col-1sidebar' || $page_layout == '2col-2sidebars' || $page_layout == '3col-1sidebar' || $page_layout == '4col-1sidebar') : ?>
            <!-- Sidebar 1 -->
            <aside class="<?php echo $sidebar_class; ?>">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar1')) : ?><?php endif; ?>
            </aside>
        <?php endif; ?>

        <?php if ($page_layout == '2sidebars' || $page_layout == '2col-2sidebars') : ?>
            <!-- Sidebar 2 -->
            <aside class="<?php echo $sidebar_class; ?>">
                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar2')) : ?><?php endif; ?>
            </aside>
        <?php endif; ?>

    </section>    

<?php endif; // End 'Slides' layout check ?>


<?php if ($after_loop == NULL || $after_loop == true) : ?>
<?php if (!is_singular() && function_exists('dynamic_sidebar') && is_active_sidebar('after-loop')  && !is_page_template('template-clean.php') && !is_page_template('template-custom.php')) : ?>
<div id="after-loop" class="row clearfix">
    <div class="grid_12">
        <div class="row horizontal-widgets">
            <?php dynamic_sidebar('after-loop'); ?>
        </div>
    </div>
</div>
<?php endif; endif; ?>


<?php 

// Pages

global $wp_query;

$big = 999999999; // need an unlikely integer

$pages = paginate_links( array(
    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages
)); ?>

<?php if (!empty($pages)): ?>
    <?php $pages_class = ''; if (isset($infinite_scroll) && $infinite_scroll == true) : $pages_class = 'hidden infinite-scroll'; ?>
        <?php if ($page_layout != 'slides'): ?>
            <div class="row base">
                <div class="grid_12">
                    <a id="load-more" href="#" class="button full"><?php _e('Load more...', 'theme_admin'); ?></a>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <!-- Next/Previous Page -->
    <div id="pages-nav" class="row base <?php echo $pages_class; ?>">
        <div class="grid_12">
        <?php echo $pages; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (empty($pages) || (!isset($infinite_scroll) || $infinite_scroll == false)): ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.slide-next', $('.fullwidth-slides .fullwidth-slide:last-child')).remove();
        });
    </script>
<?php endif; ?>